<?php
/********************* DEFINE MAIN PATHS ********************/
define('PIX_funcPATH', get_template_directory() . '/library/functions/');
$pixhealth_adminPath = get_template_directory() . '/library/admin/';
$pixhealth_funcPath = get_template_directory() . '/library/functions/';
$pixhealth_incPath = get_template_directory() . '/library/includes/';
if(class_exists('WP_Session'))
    $pixhealth_customize_live_preview = WP_Session::get_instance();
include_once( ABSPATH . 'wp-admin/includes/plugin.php' );
global $pixhealth_options;
$pixhealth_options = isset($_POST['options']) ? $_POST['options'] : get_option('pixhealth_general_settings');

/********************* DEFINE MAIN PATHS ********************/
require_once ($pixhealth_incPath . 'menu_walker.php');
require_once ($pixhealth_funcPath . 'helper.php');
require_once ($pixhealth_incPath . 'portfolio_walker.php');
require_once ($pixhealth_funcPath . 'post-types.php');
require_once ($pixhealth_funcPath . 'widgets.php');
require_once ($pixhealth_funcPath . '/shortcodes/shortcode.php');
require_once ($pixhealth_adminPath . 'custom-fields.php');
require_once ($pixhealth_adminPath . 'scripts.php');
require_once ($pixhealth_adminPath . 'admin-panel/admin-panel.php');
require_once ($pixhealth_adminPath . 'admin-panel/class-tgm-plugin-activation.php');
require_once ($pixhealth_funcPath . 'functions.php');
require_once ($pixhealth_funcPath . 'filters.php');
require_once ($pixhealth_funcPath . 'common.php');

require_once (get_template_directory() . '/pixinit/loadscripts.php');
require_once (get_template_directory() . '/pixinit/fonts.php');
require_once (get_template_directory() . '/pixinit/setup.php');
require_once (get_template_directory() . '/pixinit/tgm_plugin.php');
require_once (get_template_directory() . '/pixinit/customize.php');
require_once (get_template_directory() . '/pixinit/woo.php');


//##################### Add custome  admin column ##############################
add_filter( 'manage_edit-shop_order_columns', 'shop_order_columns' );
function shop_order_columns($columns){
	add_thickbox();
    $new_columns = (is_array($columns)) ? $columns : array();

    //edit this for you column(s)
    $new_columns['prescription'] = 'prescription';
    //stop editing


    return $new_columns;
}

add_action( 'manage_shop_order_posts_custom_column', 'shop_order_posts_custom_column', 2 );
function shop_order_posts_custom_column($column){
    global $post, $the_order , $wpdb;
    $upload_dir = wp_upload_dir();
    if ( empty( $the_order ) || $the_order->id != $post->ID ) {
        $the_order = wc_get_order( $post->ID );
       
    }
     $order_id=$the_order->id;
     $key='prescription';
     $prescription_id=woocommerce_get_order_item_meta( $order_id, $key);
     
     
    if ( $column == 'prescription' ) {  
    	$post_id = $wpdb->get_results("SELECT  option_value FROM $wpdb->options WHERE option_id = '$prescription_id'");
      ?>
        <a class="thickbox" href="<?php echo $upload_dir['baseurl'];?>/<?php echo $post_id[0]->option_value; ?>"><?php echo (isset($prescription_id) ? $prescription_id : ''); ?></a>

      <?php
    }
    //stop editing
}
?>