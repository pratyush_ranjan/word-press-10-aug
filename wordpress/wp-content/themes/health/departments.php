<?php 
/*** 
Template Name: Departments
The template for displaying all pages.	
***/ 

$pixhealth_options = get_option('pixhealth_general_settings');
$pixhealth_exclude = (class_exists( 'RW_Meta_Box' ) && function_exists( 'pixtheme_services_register' )) ? rwmb_meta( 'page_departments_not' , 'type=taxonomy&taxonomy=services_category') : array();
$pixhealth_deps = array();
if(!empty($pixhealth_exclude))
foreach ($pixhealth_exclude as $pixhealth_dep) {
	array_push($pixhealth_deps, $pixhealth_dep->term_id);
}

?>

<?php get_header();?>

<main class="main-content">

      <div class="container">
            
          
                     <?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
        
							<?php the_content(); ?>
                      
                    <?php endwhile; ?>
                 
        
        
        <div class="text-center">
				
                         
					<?php	
						$args = array( 'taxonomy' => 'services_category', 'hide_empty' => '0', 'exclude' => $pixhealth_deps);							
						$pixhealth_categories = get_categories ($args);								
						if( $pixhealth_categories ):
							foreach($pixhealth_categories as $pixhealth_cat) :
								$pixhealth_t_id = $pixhealth_cat->term_id;
								$pixhealth_cat_meta = get_option("services_category_$pixhealth_t_id");
								$pixhealth_link = !isset($pixhealth_cat_meta['pix_serv_url']) || $pixhealth_cat_meta['pix_serv_url'] == '' ? get_term_link( $pixhealth_cat ) : $pixhealth_cat_meta['pix_serv_url'];
					?>										
								<div class="departments-item ">
									<span class="icon-round bg-color_second helper"><i class="icon <?php echo esc_attr($pixhealth_cat_meta['pix_icon']) ?>"></i></span>
									<h3 class="ui-title-inner"><?php echo wp_kses_post($pixhealth_cat->name) ?></h3>
									<p class="ui-text"><?php echo pixhealth_limit_words($pixhealth_cat->description, 20) ?></p>
									<a class="btn btn_small" href="<?php echo esc_url($pixhealth_link) ?>"><?php _e("READ MORE", "PixHealth") ?></a>
								</div>
					<?php							
							 endforeach;
						endif; ?>
							
					
       </div>              
		
        
        </div></main>
 
<?php get_footer();?>
