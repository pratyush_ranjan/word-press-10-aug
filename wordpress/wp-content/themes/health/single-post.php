 <?php /* Template Name: Single Post */ 

$pixhealth_custom =  get_post_custom($post->ID);
$pixhealth_layout = isset ($pixhealth_custom['pixhealth_page_layout']) ? $pixhealth_custom['pixhealth_page_layout'][0] : '2';
$pixhealth_sidebar = isset ($pixhealth_custom['pixhealth_selected_sidebar'][0]) ? $pixhealth_custom['pixhealth_selected_sidebar'][0] : 'global-sidebar-1';
$pixhealth_options = get_option('pixhealth_general_settings');
?>
<?php get_header();?>

<div class="container">
    <div class="row">
      
		<?php if ($pixhealth_layout == '3'): require_once(get_template_directory() .'/template-parts/sidebar.php'); endif?>
        
      	<div class="col-xs-12 col-sm-12 <?php if ($pixhealth_layout == '1'):?> col-md-12 col-lg-12 <?php else: ?> col-md-9 col-lg-9 <?php endif?>">
        	<main class="main-content">
            
        	<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
            	
                <article id="post-<?php esc_attr(the_ID());?>" class="post">
    				<div class="entry-main">
                    	<ul class="info-post">
						<?php if($pixhealth_options['pixhealth_blog_show_date']): ?>
                            <li class="date color_primary"><?php echo get_the_time('j'); ?></li>
                            <li class="month"><?php echo get_the_time('M'); ?></li>
                        <?php endif?>
                        <?php if( 'open' == $post->comment_status && pixhealth_get_option('pixhealth_blog_show_comments',1)) : ?>
                            <li class="comments li-last">
                                <i class="icon icon-bubbles color_second"></i>
                                <?php comments_popup_link( __( '0', 'PixHealth' ), __( '1', 'PixHealth' ), __( '%', 'PixHealth' ), "comments-link"); ?>
                            </li>
                        <?php endif; ?>
                        </ul>
                        
                        <?php			
							$pixhealth_format  = get_post_format();
							$pixhealth_format = !in_array($pixhealth_format, array("quote", "gallery")) ? 'standared' : $pixhealth_format;								
							get_template_part('template-parts/post-format-single/blog', $pixhealth_format);
						?>
                        <h2 class="entry-title"><?php the_title() ?></h2>
                        <ul class="entry-meta unstyled clearfix">
                            <li>
                                <i class="icon color_second icon-user"></i><?php the_author_posts_link(); ?>
                            </li>
                            <?php 
                            if(pixhealth_get_option('pixhealth_blog_show_category',1)){
                                $pixhealth_categories = get_the_category($post->ID);
                                if($pixhealth_categories){
                                    $pixhealth_output = '<li class="cat-meta-a" ><i class="icon color_second icon-folder"></i>';						
                                    foreach($pixhealth_categories as $pixhealth_category) {
                                        $pixhealth_output .= '<a href="'.esc_url(get_category_link( $pixhealth_category->term_id )).'">'.wp_kses_post($pixhealth_category->cat_name).'</a> ';
                                    }
                                    $pixhealth_output .= '</li>';
                                    echo wp_kses_post($pixhealth_output);
                                }
                            }
                            ?>
                        </ul>
                        
                        <div class="entry-content ui-text"><?php the_content() ?></div>
                        <?php wp_link_pages();?>
                        
                        <?php if ( 'post' == get_post_type() ) : // Hide category and tag text for pages on Search                        
							if(pixhealth_get_option('pixhealth_blog_show_tag', 1)){	
								$pixhealth_posttags = get_the_tags($post->ID);
								if ($pixhealth_posttags) {
									$pixhealth_output = '<div class="footer-panel">
                        							<i class="icon-tag color_primary"></i>';
									foreach($pixhealth_posttags as $pixhealth_tag) {
										$pixhealth_output .= '<a href="'.esc_url(get_tag_link( $pixhealth_tag->term_id )).'" >'.wp_kses_post($pixhealth_tag->name).'</a>';
									}
									$pixhealth_output .= '</div>';
									echo wp_kses_post($pixhealth_output);
								}
							}
						endif; // End if 'post' == get_post_type()?>
                                               
                        <div class="social-links clearfix">
							<?php if(pixhealth_get_option('pixhealth_blog_share', 1)) echo do_shortcode('[share]'); ?>                        
                        </div>
                        
                   	</div>
                </article>
                
                <?php if(pixhealth_get_option('pixhealth_blog_show_author', 1)) : ?>
					<?php 
                        $pixhealth_get_avatar = get_avatar(get_the_author_meta('ID'), 85);
						preg_match("/src=['\"](.*?)['\"]/i", $pixhealth_get_avatar, $pixhealth_matches);
						$pixhealth_src = !empty($pixhealth_matches[1]) ? $pixhealth_matches[1] : '';
                    ?>
                    
                <div class="about-autor bg-color_second clearfix bounceIn" data-animation="bounceIn">
                    <img class="about-autor__foto" src="<?php echo esc_url($pixhealth_src) ?>" height="130" width="135" alt="Avatar">
                    <div class="about-autor__inner">
                        <span class="about-autor__name"><?php the_author_posts_link(); ?></span>
                        <span class="about-autor__category"><?php the_author_meta( 'nickname' ); ?></span>
                        <div class="about-autor__text ui-text"><?php the_author_meta( 'description' ); ?></div>
                    </div>
                </div>
                <?php endif; ?>
                
                        
    
			<?php if(pixhealth_get_option('pixhealth_blog_show_comments', 1)) : ?>
                <section class="section-comment">
                    <?php comments_template(); ?>				
                </section>
                <section class="comment-reply-form">          
                  <?php $test = false; if ($test) {comment_form(); } ?>
                </section>
            <?php endif; ?>
            
        <?php endwhile; ?>
        
			</main>
      	</div>
    	
		<?php if ($pixhealth_layout == '2'): require_once(get_template_directory() .'/template-parts/sidebar.php'); endif?>
        
	</div>
</div>
<?php get_footer();?>
