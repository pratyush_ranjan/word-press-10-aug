<?php 
/*** The template for displaying archive pages ***/

$pixhealth_custom =  get_post_custom($post->ID);
$pixhealth_layout = isset ($pixhealth_custom['pixhealth_page_layout']) ? $pixhealth_custom['pixhealth_page_layout'][0] : '2';
$pixhealth_sidebar = isset ($pixhealth_custom['pixhealth_selected_sidebar'][0]) ? $pixhealth_custom['pixhealth_selected_sidebar'][0] : 'global-sidebar-1';
$pixhealth_options = get_option('pixhealth_general_settings');
?>

 <?php get_header();?>
 

<main class="section" id="main">
  <div class="container">
    <div class="row"> 
    
     <?php if ($pixhealth_layout == '3'): require_once(get_template_directory() .'/template-parts/sidebar.php'); endif?>
      
       <div class="col-xs-12 col-sm-7 col-md-9  <?php if ($pixhealth_layout == '3'):?>  col2-left  <?php endif?>   <?php if ($pixhealth_layout == '2'):?>  col2-right  <?php endif?>">  
        <section role="main" class="main-content">
        
   
            <?php
				if ( have_posts() ) 
					the_post();
				rewind_posts();       
				get_template_part( 'loop', 'archive' );
            ?>
    
        
        </section></div>
        
      <?php if ($pixhealth_layout == '2'): require_once(get_template_directory() .'/template-parts/sidebar.php'); endif?>      
    
    </div>
    </div>
    </main>
   

<?php get_footer(); ?>