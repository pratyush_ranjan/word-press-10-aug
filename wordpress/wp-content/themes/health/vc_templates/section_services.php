<?php
global $post;
$out = $cnt = '';

extract(shortcode_atts(array(
	'image_icon'=>'',
	'count'=>'0',
	'serv'=>'',
	//'perrow'=>'',
	//'container'=>'',
	//'showcont'=>'',
	'buttext'=>'',
	'css_animation' => '',				
), $atts));

if( $serv == '' ):
	$out .= '<p>'.__('No services selected. To fix this, please login to your WP Admin area and set the services you want to show by editing this shortcode and setting one or more services in the multi checkbox field "Services".', 'PixHealth');
else: 

$out = $css_animation != '' ? '<div class="animated" data-animation="' . esc_attr($css_animation) . '">' : '<div>';
$out .= $image_icon == 'icon' ? '<div class="list-services">' : '<div class="row">';
	
	$services = explode( ",", $serv );	
	$args = array(
				'post_type' => 'services', 
				'orderby' => 'menu_order',
				'post__in' => $services,			 
				'order' => 'ASC'
			);
	if( is_numeric($count) )
		$args['showposts'] = $count;
	else
		$args['posts_per_page'] = -1;
		
	$wp_query = new WP_Query( $args );
									
	if ($wp_query->have_posts()):
		while ($wp_query->have_posts()) :
			$wp_query->the_post();		
			$cats = wp_get_object_terms($post->ID, 'services_category');											   
						
			if ( get_post_meta( get_the_ID(), 'service_icon', true ) !== '' )
			 
			$thumbnail = get_the_post_thumbnail($post->ID, 'pixhealth-services-thumb');
			/*
			$t_id = $cat->term_id;
			$cat_meta = get_option("services_category_$t_id");
			*/
if($image_icon == 'icon'){						
$out .= '
		<div class="col-md-4 list-services__item">';
			if ( get_post_meta( get_the_ID(), 'service_icon', true ) !== '' ){
$out .= '
			<span class="icon-round bg-color_second helper"><i class="icon '.esc_attr(get_post_meta( get_the_ID(), 'service_icon', true )).'"></i></span>
		';
			}
$out .= '            
            <div class="list-services__inner border_btm">
				<h3 class="list-services__title">'.wp_kses_post(get_the_title()).'</h3>
				<p class="ui-text">'.pixhealth_limit_words(get_the_excerpt(), 20).'</p>
            </div>
        </div>';
}
else{		
$out .= '		
		<div class="col-sm-4 text-center">
			<div class="services__item">
				<div class="service__figure">
					<div class="hover__figure"> '.wp_kses_post($thumbnail).' </div>';
					if ( get_post_meta( get_the_ID(), 'service_icon', true ) !== '' ){
$out .= '
					<span class="icon-round icon-round_small helper"><i class="icon '.esc_attr(get_post_meta( get_the_ID(), 'service_icon', true )).'"></i></span> 
		';
					}
$out .= '
				</div>
				<h3 class="ui-title-inner">'.wp_kses_post(get_the_title()).'</h3>
				<p class="ui-text">'.pixhealth_limit_words(get_the_excerpt(), 20).'</p>
				<a class="btn btn_small" href="'.esc_url(get_permalink(get_the_ID())).'">'.wp_kses_post($buttext).'</a> 
			</div>
		</div>
		';
}
		 endwhile;
	endif;
	 
$out .= '
	</div>
	';
		
$out .= '</div>';
endif;	
echo $out;