<?php
$out = '';

extract(shortcode_atts(array(
	"title"=>'',
	"title_strong"=>'',
	"title_color"=>'',
	"usedecor" => '',
	"align" => '',
	"color" => '',
	"css_animation" => '',
), $atts));

$fullcontent = ($content == "") ? "" : '<div class="ui-subtitle-block">'.do_shortcode($content).'</div>';
$decor = $usedecor ? '<i class="decor-brand"></i>' : ''; 

$strong_t = ($title_strong == '') ? '' : '<strong class="font-weight_600">'.wp_kses_post($title_strong).'</strong>';
$color_t = ($title_color == '') ? '' : '<span class="font-weight-norm color_primary">'.wp_kses_post($title_color).'</span>';

$out = $css_animation != '' ? '<div class="animated" data-animation="' . esc_attr($css_animation) . '">' : '<div>';
$out .= '
		<div class="text-center '.esc_attr($align).' '.esc_attr($color).'">
			<h1 class="ui-title-block">'.wp_kses_post($title).$strong_t.$color_t.'</h1>
			'.$fullcontent.'
			'.wp_kses_post($decor).'
		</div>
		'; 
$out .= '</div>';

echo $out;