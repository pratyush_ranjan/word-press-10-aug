<?php
$out = $decor = '';

extract(shortcode_atts(array(
	"title"=>'',
	"usedecor"=>'',
	"css_animation" => '',
), $atts));

$decor = $usedecor ? '<i class="decor-brand decor-brand_footer"></i>' : '';
$out = $css_animation != '' ? '<div class="animated" data-animation="' . esc_attr($css_animation) . '">' : '<div>';
$out .= '
		<section class="footer__section">
			<h2 class="footer__title">'.wp_kses_post($title).'</h2>
			'.wp_kses_post($decor).'
			<div class="footer__content">'.do_shortcode($content).'</div>
		</section>
	';
$out .= '</div>'; 

echo $out;