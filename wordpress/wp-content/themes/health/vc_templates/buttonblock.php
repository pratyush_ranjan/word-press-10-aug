<?php
/**
 * Shortcode attributes
 * @var $atts
 * @var $type
 * @var $icon_pixelegant
 * @var $icon_pixflaticon
 * @var $icon_pixicomoon
 * @var $icon_pixfontawesome
 * @var $icon_pixsimple
 * @var $icon_fontawesome
 * @var $icon_openiconic
 * @var $icon_typicons
 * @var $icon_entypo
 * @var $icon_linecons
 * @var $title
 * @var $link
 * @var $icon
 * @var $color
 * Shortcode class
 * @var $this WPBakeryShortCode_Buttonblock
 */
 
$atts = vc_map_get_attributes( $this->getShortcode(), $atts );
extract( $atts );
$out = '';

$icon = $icon != '' ? $icon : ( isset( ${"icon_" . $type} ) ? ${"icon_" . $type} : '' );


$out .='

	<a href="'.esc_url($link).'" target="_blank" class="ellipseBtn bigBtn '.esc_attr($color).'">
		<span>
			<i class="fa '.esc_attr($icon).'"></i>
			'.wp_kses_post($title).'
		</span>
	</a>
	
  ';
 

echo $out;