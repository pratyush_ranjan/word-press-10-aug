<?php
/**
 * Shortcode attributes
 * @var $atts
 * @var $type
 * @var $icon_pixelegant
 * @var $icon_pixflaticon
 * @var $icon_pixicomoon
 * @var $icon_pixfontawesome
 * @var $icon_pixsimple
 * @var $icon_fontawesome
 * @var $icon_openiconic
 * @var $icon_typicons
 * @var $icon_entypo
 * @var $icon_linecons
 * @var $icon
 * @var $css_animation
 * Shortcode class
 * @var $this WPBakeryShortCode_Buttonblock
 */
 
$atts = vc_map_get_attributes( $this->getShortcode(), $atts );
extract( $atts );
$out = '';

$icon = $icon != '' ? $icon : ( isset( ${"icon_" . $type} ) ? ${"icon_" . $type} : '' );

$out = $css_animation != '' ? '<div class="animated" data-animation="' . esc_attr($css_animation) . '">' : '<div>';
$out .= '

		<ul  class="list-contacts unstyled ">
			<li>
				<i class="icon '.esc_attr($icon).'"></i>
				'.do_shortcode($content).'
			</li>
		</ul>
		
	';
$out .= '</div>'; 

echo $out;