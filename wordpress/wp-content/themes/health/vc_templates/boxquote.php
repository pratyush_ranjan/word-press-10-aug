<?php
$out = $image = '';

extract(shortcode_atts(array(
	"name" => '',
	"position" => '',
	"image" => $image,
	"css_animation" => '',
), $atts));	

$img = wp_get_attachment_image_src( $image, 'large' );
	
$img_output = $img[0];

$out = $css_animation != '' ? '<div class="animated" data-animation="' . esc_attr($css_animation) . '">' : '<div>';
$out .= '
		<div class="post entry-review">
			<i class="quotes fa fa-quote-left bg-color_primary"></i>
			<blockquote>
			  '.do_shortcode($content).'
			</blockquote>
			<img class="entry-review__avatar" src="'.esc_url($img_output).'" height="79" width="78" alt="Avatar">
			<span class="entry-review__name">'.wp_kses_post($name).'</span>
			<span class="entry-review__categories">'.wp_kses_post($position).'</span>
		</div>		
  ';
$out .= '</div>';
	
echo $out;