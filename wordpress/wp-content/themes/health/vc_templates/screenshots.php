<?php
$out = '';

extract(shortcode_atts(array(
	'images'=>'',				
), $atts));

$images = explode( ',', $images );

$out = '
		<div>
			<ul class="bxslider slider_gallery" data-max-slides="7" data-min-slides="3" data-width-slides="400" data-margin-slides="0" data-auto-slides="true" data-move-slides="1" data-infinite-slides="true" data-pager="false">			
    			
		';
foreach( $images as $image ){
	if ( $image > 0 ) {
		$img = wp_get_attachment_image_src( $image, array(257,453) );
		$img_output = $img[0];
		$image_meta = pixhealth_wp_get_attachment($image);
		$image_alt = $image_meta['alt'] == '' ? $image_meta['title'] : $image_meta['alt'];
	} else {
		$img_output = '<img src="' . vc_asset_url( 'vc/no_image.png' ) . '" />';
	}
	$out .=	'
				<li class="slide">
					<a href="'.esc_url($img_output).'" class="prettyPhoto">
						<span class="slide_bg"></span>
						<img src="'.esc_url($img_output).'" height="350" width="400" alt="'.esc_attr($image_alt).'">
					</a>
				</li>				
			';
}
$out .=	'				
			</ul>
		</div>
	';	
echo $out;