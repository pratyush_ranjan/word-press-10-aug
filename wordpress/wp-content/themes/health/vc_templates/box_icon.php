<?php
/**
 * Shortcode attributes
 * @var $atts
 * @var $type
 * @var $icon_pixelegant
 * @var $icon_pixflaticon
 * @var $icon_pixicomoon
 * @var $icon_pixfontawesome
 * @var $icon_pixsimple
 * @var $icon_fontawesome
 * @var $icon_openiconic
 * @var $icon_typicons
 * @var $icon_entypo
 * @var $icon_linecons
 * @var $title
 * @var $icon
 * @var $position
 * @var $css_animation
 * Shortcode class
 * @var $this WPBakeryShortCode_Box_Icon
 */
 
$atts = vc_map_get_attributes( $this->getShortcode(), $atts );
extract( $atts );
$out = $decor = '';

$icon = $icon != '' ? $icon : ( isset( ${"icon_" . $type} ) ? ${"icon_" . $type} : '' );

$out = $css_animation != '' ? '<div class="animated" data-animation="' . esc_attr($css_animation) . '">' : '<div>';
$out .= '
		<div class="list-services list-services_vert '.esc_attr($position).'">
			<div class="list-services__item">
				<span class="icon-round icon-round_grey helper"><i class="icon fa '.esc_attr($icon).'"></i></span>
				<div class="list-services__inner">
					<h3 class="list-services__title">'.wp_kses_post($title).'</h3>
					<p class="ui-text">'.do_shortcode($content).'</p>
				</div>
			</div>
		</div>
	';
$out .= '</div>'; 

echo $out;