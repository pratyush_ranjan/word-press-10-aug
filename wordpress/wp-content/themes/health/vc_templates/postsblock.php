<?php
global $post;
$out = $cat = $date ='';
$pixhealth_options = get_option('pixhealth_general_settings');

extract(shortcode_atts(array(
	'count'=>3,
	'read_more'=>"READ MORE",
	'max_slides' => '',
	'min_slides' => '',
	'width_slides' => '',
	'margin_slides' => '',
	'disable_carousel' => '',				
), $atts));

$max_slides = $max_slides == '' ? 2 : $max_slides;
$min_slides = $min_slides == '' ? 1 : $min_slides;
$width_slides = $width_slides == '' ? 370 : $width_slides;
$margin_slides = $margin_slides == '' ? 30 : $margin_slides;
$disable_carousel = $disable_carousel != '0' ? 'bxslider' : 'carousel-disable';
	   
$out = ' 
		<div class="row">';

$args = array( 
			'showposts' => $count
		);
if( is_numeric($count) )
	$args['showposts'] = $count;
else
	$args['posts_per_page'] = -1;
	
$wp_query = new WP_Query( $args );
			 					
	if ($wp_query->have_posts()): 
		while ($wp_query->have_posts()) : 							
			$wp_query->the_post();
			$custom = get_post_custom($post->ID);
			if(!empty($pixhealth_options['pixhealth_blog_show_category']) && $pixhealth_options['pixhealth_blog_show_category'] == 1){
				$categories = get_the_category($post->ID);
				if($categories){
					$cat = '';//'<span class="meta-i">'.__('In: ', 'PixHealth');						
					foreach($categories as $category) {
						$cat .= '<a href="'.esc_url(get_category_link( $category->term_id )).'" class="category color_primary">'.wp_kses_post($category->cat_name).'</a> ';
					}
					//$cat .= '</span>';						
				}
			}
			if( 'open' == $post->comment_status && $pixhealth_options['pixhealth_blog_show_comments']){
				$comments = '
							<li class="comments li-last">
								<a href="'.esc_url(get_comments_link( $post->ID )).'">
									<i class="icon icon-bubbles color_second"></i>
									'.wp_kses_post(get_comments_number()).'
								</a>
							</li>
							';     
      		}
			if($pixhealth_options['pixhealth_blog_show_date']){
				$date = '
						<li class="date color_primary">'.wp_kses_post(get_the_time('j')).'</li>
						<li class="month">'.wp_kses_post(get_the_time('M')).'</li>
						';                    
			}
			$thumbnail = get_the_post_thumbnail( $post->ID ) != '' ? get_the_post_thumbnail( $post->ID, 'pixhealth-post-thumb' ) : '<img src="'.esc_url(get_template_directory_uri()).'/images/noimage.jpg">';
						
$out .= '

				<div class="col-sm-4">
					<article class="article-short">
						<ul class="info-post">
							'.wp_kses_post($date).'
							'.wp_kses_post($comments).'
						</ul>
						<a href="'.esc_url(get_the_permalink()).'">'.wp_kses_post($thumbnail).'</a>
						'.wp_kses_post($cat).'
						<a href="javascript:void(0);" class="autor">'.get_the_author_link().'</a>
						<h3 class="title">'.wp_kses_post(get_the_title()).'</h3>
						<p class="ui-text">'.get_the_excerpt().'</p>
						<a class="btn btn_small" href="'.esc_url(get_the_permalink()).'">'.wp_kses_post($read_more).'</a>
					</article>
				</div>
				
            	';	
	
		 endwhile;
		 endif;
	 
$out .= '            
            	<!--end-->
			</div>
 
	';
	
echo $out;