<?php
$out = $image = '';

extract(shortcode_atts(array(
	"title_strong" => '',
	"title" => '',
	"image" => $image,
	"link" => '',
	"btntext" => '',
	"icon" => '',
	"css_animation" => '',
), $atts));	

$img = wp_get_attachment_image_src( $image, 'large' );

$final_title_strong = $title_strong != '' ? '<h2 class="ui-title-block color_white font-weight_700">'.$title_strong.'</h2>' : '';
$final_title = $title != '' ? '<div class="subtitle_mod-1">'.$title.'</div>' : '';
$final_btntext = $btntext != '' ? '<a class="btn btn_transparent prettyPhoto" href="'.esc_url($link).'">'.$btntext.'</a>' : '';
	
$img_output = $img[0];

$out = $css_animation != '' ? '<div class="animated" data-animation="' . esc_attr($css_animation) . '">' : '<div>';
$out .= '

		<div class="row">
			<div class="col-md-6 color_white">
				'.wp_kses_post($final_title_strong).'
				'.wp_kses_post($final_title).'
				<p class="ui-text color_white">'.do_shortcode($content).'</p>
				'.wp_kses_post($final_btntext).'
			</div>
			<div class="col-md-5 col-md-offset-1"> 
				<a class="link_on-youtube prettyPhoto" href="'.esc_url($link).'" >
					<img src="'.esc_url($img_output).'" alt="Link on video">
				</a>
			</div>
		</div>

  ';
$out .= '</div>';
	
echo $out;