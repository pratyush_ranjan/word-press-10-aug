<?php
$out = '';

extract(shortcode_atts(array(
	"title" => '',
	"title_strong" => '',
	"link" => '',
	"btntext" => '',
	"title_info" => '',
	"css_animation" => '',
), $atts));	

$final_title = $title != '' ? '<p class="block-hourse__text"><i class="icon icon-note"></i>'.wp_kses_post($title).'</p>' : '';
$final_title_strong = $title_strong != '' ? '<p class="block-hourse__title">'.wp_kses_post($title_strong).'</p>' : '';
$final_btntext = $btntext != '' ? '<a class="btn btn_transparent" href="'.esc_url($link).'">'.wp_kses_post($btntext).'</a>' : '';
$final_title_info = $title_info != '' ? '<div class="block-hourse__title-table"><i class="icon icon-clock"></i>'.wp_kses_post($title_info).'</div>' : '';

$out = $css_animation != '' ? '<div class="animated" data-animation="' . esc_attr($css_animation) . '">' : '<div>';

$out .= '
		<div class="block-hourse">
			<div class="row">
				<div class="col-md-6">
					<div class="block-hourse__inner block-hourse__inner_first">
						'.wp_kses_post($final_title.$final_title_strong.$final_btntext).' 
					</div>
				</div>
				<div class="col-md-6">
					<div class="block-hourse__inner block-hourse__inner_second">
						'.wp_kses_post($final_title_info).'
						'.do_shortcode($content).'
					</div>
				</div>
			</div>
		</div>	
  ';

$out .= '</div>'; // css_animation <div>

echo $out;