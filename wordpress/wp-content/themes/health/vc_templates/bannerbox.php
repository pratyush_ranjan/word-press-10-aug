<?php
$out =  '';
 extract(shortcode_atts(array(
		"title" => '',		
		"link" => '',
		"btntext" => '',
		"css_animation" => '',
	), $atts));	

$out = $css_animation != '' ? '<div class="animated" data-animation="' . esc_attr($css_animation) . '">' : '<div>';		
$out .= '	


	<div class="banner mod-1"><div class="container">
		<div class="row">
			<div class="col-md-7 col-md-offset-1">
				<div class="banner__wrap pull-left">
					<p class="banner__title">'.wp_kses_post($title).'</p>
					<p class="banner__text">'.do_shortcode($content).'</p>
				</div>
			</div>
			<div class="col-md-3 col-md-offset-1"> <a class="btn btn pull-right" href="'.esc_url($link).'">'.wp_kses_post($btntext).' <span class="btn-plus"></span></a> </div>
		</div>
	</div></div>
	';
$out .= '</div>';

echo $out;