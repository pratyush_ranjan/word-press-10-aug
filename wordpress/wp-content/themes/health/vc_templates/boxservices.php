<?php
/**
 * Shortcode attributes
 * @var $atts
 * @var $type
 * @var $icon_pixelegant
 * @var $icon_pixflaticon
 * @var $icon_pixicomoon
 * @var $icon_pixfontawesome
 * @var $icon_pixsimple
 * @var $icon_fontawesome
 * @var $icon_openiconic
 * @var $icon_typicons
 * @var $icon_entypo
 * @var $icon_linecons
 * @var $title
 * @var $icon
 * @var $btntext
 * @var $link
 * @var $usedecor
 * @var $css_animation
 * Shortcode class
 * @var $this WPBakeryShortCode_Boxservices
 */
 
$atts = vc_map_get_attributes( $this->getShortcode(), $atts );
extract( $atts );
$out = $decor = $fbutton = '';

$icon = $icon != '' ? $icon : ( isset( ${"icon_" . $type} ) ? ${"icon_" . $type} : '' );

$fbutton = $link != '' ? '<a class="btn btn_small" href="'.esc_url($link).'">'.$btntext.'</a>' : '';
$decor = $usedecor ? '<i class="decor-brand"></i>' : '';
$out = $css_animation != '' ? '<div class="animated" data-animation="' . esc_attr($css_animation) . '">' : '<div>';
$out .= '
		<section class="advantages__inner text-center"> 
			<i class="icon '.esc_attr($icon).' color_second"></i>
			<h2 class="ui-title-inner">'.wp_kses_post($title).'</h2>
			'.wp_kses_post($decor).'
			<p class="ui-text text-center">'.do_shortcode($content).'</p>
			'.wp_kses_post($fbutton).' 
		</section>
	';
$out .= '</div>'; 

echo $out;