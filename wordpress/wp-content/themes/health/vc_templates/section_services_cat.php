<?php
global $post;
$out = $cnt = '';

extract(shortcode_atts(array(
	'title'=>'',
	'count'=>'0',
	'cats'=>'',
	//'perrow'=>'',
	//'container'=>'',
	//'showcont'=>'',
	'buttext'=>'',
	'css_animation' => '',				
), $atts));

if( $cats == '' ):
	$out .= '<p>'.__('No departments selected. To fix this, please login to your WP Admin area and set the departments you want to show by editing this shortcode and setting one or more departments in the multi checkbox field "Departments".', 'PixHealth');
else: 

$out = $css_animation != '' ? '<div class="animated" data-animation="' . esc_attr($css_animation) . '">' : '<div>';
$out .= '					
	<div class="row">
		<div class="col-xs-12 text-center">	
	';		
		
	$args = array( 'taxonomy' => 'services_category', 'hide_empty' => '0', 'include' => $cats, 'number' => $count);							
	$categories = get_categories ($args);								
	if( $categories ):
		foreach($categories as $cat) :
			$t_id = $cat->term_id;
			$cat_meta = get_option("services_category_$t_id");
			$link = !isset($cat_meta['pix_serv_url']) || $cat_meta['pix_serv_url'] == '' ? get_term_link( $cat ) : $cat_meta['pix_serv_url'];
						
$out .= '					
			<div class="departments-item">
				<span class="icon-round bg-color_second helper"><i class="icon '.esc_attr($cat_meta['pix_icon']).'"></i></span>
				<h3 class="ui-title-inner">'.wp_kses_post($cat->name).'</h3>
				<p class="ui-text">'.pixhealth_limit_words($cat->description, 20).'</p>
				<a class="btn btn_small" href="'.esc_url($link).'">'.wp_kses_post($buttext).'</a>
			</div>
		';
		
		 endforeach;
	endif;
	 
$out .= '            
    	</div><!-- end col -->
	</div>
	';
		
	if (isset($showcont) && $showcont != ''): 
		while ($wp_query->have_posts()) : 							
			$wp_query->the_post(); 
	$out .= '<div class="portfolio-modal modal fade" id="myModal'.esc_attr($post->ID).'"  >
				<div class="modal-content"> </div>
			</div>';
		endwhile;
	endif;
		
$out .= '</div>';
endif;	
echo $out;