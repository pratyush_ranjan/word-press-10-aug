<?php
global $post;
$out = '';

extract(shortcode_atts(array(
	'form_id'=>'',
	'css_animation' => '',				
), $atts));

$out = $css_animation != '' ? '<div class="animated" data-animation="' . esc_attr($css_animation) . '">' : '<div>';
$out .= do_shortcode('[contact-form-7 id="'.esc_attr($form_id).'"]');
/*	
	$args = array(
				'post_type' => 'wpcf7_contact_form',
				'p' => $form_id,
			);
		
	$wp_query = new WP_Query( $args );
									
	if ($wp_query->have_posts()):
		while ($wp_query->have_posts()) : 							
				$wp_query->the_post();
				print_r($post);
		 endwhile;
	endif;
*/
$out .= '</div>';

echo $out;