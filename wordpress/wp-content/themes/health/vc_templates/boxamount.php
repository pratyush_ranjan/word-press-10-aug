<?php
/**
 * Shortcode attributes
 * @var $atts
 * @var $type
 * @var $icon_pixelegant
 * @var $icon_pixflaticon
 * @var $icon_pixicomoon
 * @var $icon_pixfontawesome
 * @var $icon_pixsimple
 * @var $icon_fontawesome
 * @var $icon_openiconic
 * @var $icon_typicons
 * @var $icon_entypo
 * @var $icon_linecons
 * @var $title
 * @var $icon
 * @var $amount
 * Shortcode class
 * @var $this WPBakeryShortCode_Boxamount
 */
 
$atts = vc_map_get_attributes( $this->getShortcode(), $atts );
extract( $atts );
$out = '';

$icon = $icon != '' ? $icon : ( isset( ${"icon_" . $type} ) ? ${"icon_" . $type} : '' );

$out='
		<div class="list-progress">
			<span class="icon-round icon-round_small bg-color_second helper"><i class="icon fa '.esc_attr($icon).'"></i> </span>
			<div class="info"> 	
				<span class="chart" data-percent="'.esc_attr($amount).'"> 
					<span class="percent">'.wp_kses_post($amount).'</span> 
					<canvas height="0" width="0"></canvas>
				</span>
				<span class="label-chart">'.wp_kses_post($title).'</span>
			</div>
		</div>
	'; 

echo $out;