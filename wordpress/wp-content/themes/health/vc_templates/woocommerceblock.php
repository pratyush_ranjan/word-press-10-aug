<?php
global $post;
$out = $css_class = '';

extract(shortcode_atts(array(
	'title'=>'',
	'count'=>'',
	'cats'=>'',
	'carousel'=>'',
	'controls' => '',
	'min_slides' => '',
	'css_animation' => '',				
), $atts));

$min_slides = $min_slides == '' ? 3 : $min_slides;

if( $cats == '' ):
	$out .= '<p>'.__('No categories selected. To fix this, please login to your WP Admin area and set the categories you want to show by editing this shortcode and setting one or more categories in the multi checkbox field "Categories".', 'PixHealth');
else: 

$out = $title != '' ? '' : '';
$out .= $css_animation != '' ? '<div class="animated" data-animation="' . esc_attr($css_animation) . '">' : '';
$out .= '	
			
			<div class="'.esc_attr($carousel).'-carousel white-control apps-slider staticBox owl-theme '.esc_attr($controls).'" data-min-slides="'.esc_attr($min_slides).'">
			';	
		

	$product_cat_to_query = get_objects_in_term( explode( ",", $cats ), 'product_cat');
	$args = array(
				'post_type' => 'product', 
				'orderby' => 'menu_order',	
				'post__in' => $product_cat_to_query,		 
				'order' => 'ASC',
			);
	if( is_numeric($count) )
		$args['showposts'] = $count;
	else
		$args['posts_per_page'] = -1;
$wp_query = new WP_Query( $args );
				 					
	if ($wp_query->have_posts()): 
		while ($wp_query->have_posts()) : 							
						$wp_query->the_post();
						global $product;
						$custom = get_post_custom($post->ID);					
						$icon = class_exists( 'RW_Meta_Box' ) ? rwmb_meta( 'product_icon') : '';
						$bgcolor = class_exists( 'RW_Meta_Box' ) ? rwmb_meta( 'product_icon_bgcolor') : '';
		
						$cats = wp_get_object_terms($post->ID, 'product_cat');											   
												
						if ($cats){
							$cat_slugs = '';
							$cat_names = '';
							foreach( $cats as $cat ){
								$cat_slugs .= $cat->slug . " ";
								$cat_names .= $cat->name . ", ";
							}
							$cat_names = substr($cat_names, 0, -2);
						}
						
						$link = get_the_permalink($product->id); 
						$thumbnail = get_the_post_thumbnail($post->ID, 'shop_catalog', array('class' => 'cover'));
						
						$attach_ids = $product->get_gallery_attachment_ids();
						$attachment_count = count( $product->get_gallery_attachment_ids() );
						if($attachment_count > 0){
							$image_link = wp_get_attachment_url( $attach_ids[0] ); 
							$default_attr = array(
								'class'	=> "slider_img",
								'alt'   => get_the_title($product->id),
							);
							$image = wp_get_attachment_image( $attach_ids[0], 'shop_catalog', false, $default_attr);
							
						}
						
$out .= '
            		<div class="'.esc_attr($carousel).'-item">
						<div class="app-item">
							<div class="preview-item center-block">
								<div class="preview-item_header">
									'.wp_kses_post($thumbnail).'
								</div>
								<div class="preview-item_body">';
if ( !empty($icon) && !empty($bgcolor) ) {
	$out .= '
									<span class="colorIcon smallIcon" style="background-color:'.esc_attr($bgcolor).'">
										<i class="fa '.esc_attr($icon).' whiteTxtColor"></i>
									</span>
			';
}
$out .= '
									<h4 class="preview-item_title">
										<a href="'.esc_url($link).'" class="ralewaySemiBold blackTxtColor">'.wp_kses_post(get_the_title($product->id)).'</a>
									</h4>';
if ( get_option( 'woocommerce_enable_review_rating' ) !== 'no' ) {
	$out .= '  						<div class="rating">';
		if ( $rating_html = $product->get_rating_html() ) { 
			$out .= wp_kses_post($rating_html); 
		}
    $out .= '  						</div>';
}
if ( $price_html = $product->get_price_html() ) {
	$out .= '
									<div class="preview-item_pricebox clear"> 
										<span class="preview-item_price robotoCondensed blackTxtColor">'. wp_kses_post($price_html).'</span> 
									</div>
			';
}

				
$out .= '      						<div class="app-product-item_body_btnbox"> ';

$out .= apply_filters( 'woocommerce_loop_add_to_cart_link',
	sprintf( '<a href="%s" rel="nofollow" data-product_id="%s" data-product_sku="%s" data-quantity="%s" class="ellipseLink addToCartBtn robotoMedium text-uppercase %s product_type_%s"><i class="fa fa-shopping-cart customColor"></i>%s</a>',
		esc_url( $product->add_to_cart_url() ),
		esc_attr( $product->id ),
		esc_attr( $product->get_sku() ),
		esc_attr( isset( $quantity ) ? $quantity : 1 ),
		$product->is_purchasable() && $product->is_in_stock() ? 'add_to_cart_button' : '',
		esc_attr( $product->product_type ),
		esc_html( $product->add_to_cart_text() )
	),
$product );
 
$out .= '
									</div>
								</div>
							</div>
						</div>
					</div>						
        ';
	
		endwhile;
	endif;
 
$out .= '            
			</div>
	';

$out .= $css_animation != '' ? '</div>' : '';
endif;	
echo $out;