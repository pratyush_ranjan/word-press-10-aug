<?php
global $post;
$out = $cnt = '';

extract(shortcode_atts(array(
	//'title'=>'',
	'count'=>'',
	'cats'=>'',
	'perrow'=>'',
	'container'=>'',
	'showcont'=>'',
	'buttext'=>'',
	'css_animation' => '',				
), $atts));

if( $cats == '' ):
	$out .= '<p>'.__('No categories selected. To fix this, please login to your WP Admin area and set the categories you want to show by editing this shortcode and setting one or more categories in the multi checkbox field "Categories".', 'PixHealth');
else: 

$out = $css_animation != '' ? '<div class="animated" data-animation="' . esc_attr($css_animation) . '">' : '<div>';
$out .= '	
	<div class="slider_team slider_team_filter">					 
	<div class="container">
		<div class="row">	
	 		<div class="col-lg-8 col-lg-offset-2">
				<ul id="filter" class="clearfix">     			
					<li><a href="#" class="current" data-filter="*">'. __('HealthCare' , 'PixHealth').'</a></li>';																		
						$PixHealth_Walker = new PixHealth_Portfolio_Walker();
						$args = array( 'taxonomy' => 'portfolio_category', 'hide_empty' => '1', 'include' => $cats, 'title_li'=> '', 'walker' => $PixHealth_Walker, 'show_count' => '0', 'echo' => 0);
						$categories = wp_list_categories ($args);
$out .= '
						'.$categories.'
				</ul>					
			</div>
		</div>
			
		<div class="row">
			<div class="col-sm-12">
				<div class="isotope-frame">
					<div class="isotope-filter isotope">
	';
				
	$portfolio_posts_to_query = get_objects_in_term( explode( ",", $cats ), 'portfolio_category');
	
	$args = array(
				'post_type' => 'portfolio', 
				'orderby' => 'menu_order',
				'post__in' => $portfolio_posts_to_query,			 
				'order' => 'ASC'
			);
	if( is_numeric($count) )
		$args['showposts'] = $count;
	else
		$args['posts_per_page'] = -1;
	
	$wp_query = new WP_Query( $args );
									
	if ($wp_query->have_posts()):
		while ($wp_query->have_posts()) : 							
						$wp_query->the_post();
						//$cnt++;
						//$custom = get_post_custom($post->ID);
						//$pixhealth_format = $custom['post_types_select'][0];						
						
						$cats = wp_get_object_terms($post->ID, 'portfolio_category');											   
												
						if ($cats){
							$cat_slugs = '';
							foreach( $cats as $cat ){
								$cat_slugs .= $cat->slug . " ";
							}
						}
						
						$position = (class_exists( 'RW_Meta_Box' ) && function_exists( 'pixtheme_portfolio_register' )) && rwmb_meta( 'portfolio_position') != '' ? '<span class="category">'.rwmb_meta( 'portfolio_position').'</span>' : '';
						//$phone = rwmb_meta( 'portfolio_phone') != '' ? '' : '';
						//$email = rwmb_meta( 'portfolio_email') != '' ? '' : '';
						$facebook = (class_exists( 'RW_Meta_Box' ) && function_exists( 'pixtheme_portfolio_register' )) && rwmb_meta( 'portfolio_facebook') != '' ? '<li><a target="_blank" href="'.esc_url(rwmb_meta( 'portfolio_facebook')).'"><i class="social_icons fa fa-facebook-square"></i></a></li>'."\n" : '';
						$twitter = (class_exists( 'RW_Meta_Box' ) && function_exists( 'pixtheme_portfolio_register' )) && rwmb_meta( 'portfolio_twitter') != '' ? '<li><a target="_blank" href="'.esc_url(rwmb_meta( 'portfolio_twitter')).'"><i class="social_icons fa fa-twitter-square"></i></a></li>'."\n" : '';
						$googleplus = (class_exists( 'RW_Meta_Box' ) && function_exists( 'pixtheme_portfolio_register' )) && rwmb_meta( 'portfolio_googleplus') != '' ? '<li><a target="_blank" href="'.esc_url(rwmb_meta( 'portfolio_googleplus')).'"><i class="social_icons fa fa-google-plus-square"></i></a></li>'."\n" : '';
						$linkedin = (class_exists( 'RW_Meta_Box' ) && function_exists( 'pixtheme_portfolio_register' )) && rwmb_meta( 'portfolio_linkedin') != '' ? '<li><a target="_blank" href="'.esc_url(rwmb_meta( 'portfolio_linkedin')).'"><i class="social_icons fa fa-linkedin-square"></i></a></li>'."\n" : ''; 
						
						$thumbnail = get_the_post_thumbnail($post->ID, 'pixhealth-portfolio-thumb', array('class' => 'cover'));
						
$out .= '
						<div class="isotope-item '.esc_attr($cat_slugs).'">
							<div class="slide">
								'.wp_kses_post($thumbnail).'
								<span class="name">'.wp_kses_post(get_the_title()).'</span>
								'.wp_kses_post($position).'
								<a href="'.esc_url(get_permalink(get_the_ID())).'" class="btn btn_small">'.wp_kses_post($buttext).'</a>
								';
								if($facebook || $twitter || $googleplus || $linkedin){
$out .= '
								<ul class="social-links social-links_right">
									'.wp_kses_post($facebook.$twitter.$googleplus.$linkedin).'
								</ul>';
								}
$out .= '
							</div>
						</div>
					';	
	
		 endwhile;
	endif;
	 
$out .= '            
					</div><!-- end isotope-filter -->
				</div><!-- end isotope-frame -->
			</div><!-- end col -->
		</div>	
	';
		
	if ($showcont != '' && $wp_query->have_posts()): 
		while ($wp_query->have_posts()) : 							
			$wp_query->the_post(); 
	$out .= '<div class="portfolio-modal modal fade" id="myModal'.esc_attr($post->ID).'"  >
				<div class="modal-content"> </div>
			</div>';
		endwhile;
	endif;
		
$out .= '
	</div>
	</div> 
	';
$out .= '</div>';
endif;	
echo $out;