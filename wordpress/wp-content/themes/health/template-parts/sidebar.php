<?php /*** This html display sidebar for other templates. ***/ ?>

<div class="col-xs-12 col-sm-12 col-md-3">
  <aside class="sidebar">
    <?php if ( is_active_sidebar( $pixhealth_sidebar ) ) : dynamic_sidebar($pixhealth_sidebar);   endif;?>
  </aside>
</div>

