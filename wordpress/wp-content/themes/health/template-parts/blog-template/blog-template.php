<?php
/**
 * This template is for displaying part of blog.
 *
 * @package Pix-Theme
 * @since 1.0
 */
$pixhealth_format  = get_post_format();
$pixhealth_options = get_option('pixhealth_general_settings');
$pixhealth_format = !in_array($pixhealth_format, array("quote", "gallery", "video")) ? "standared" : $pixhealth_format;
		
?>

			<ul class="info-post">
            <?php if($pixhealth_options['pixhealth_blog_show_date']): ?>
            	<li class="date color_primary"><?php echo get_the_time('j'); ?></li>
                <li class="month"><?php echo get_the_time('M'); ?></li>
            <?php endif?>
            <?php if( 'open' == $post->comment_status && $pixhealth_options['pixhealth_blog_show_comments']) : ?>
                <li class="comments li-last">
                	<i class="icon icon-bubbles color_second"></i>
                  	<?php comments_popup_link( __( '0', 'PixHealth' ), __( '1', 'PixHealth' ), __( '%', 'PixHealth' ), "comments-link"); ?>
                </li>
           	<?php endif; ?>
            </ul>
            <?php
				get_template_part( 'template-parts/post-format/blog', $pixhealth_format);            
			?>
            <h2 class="entry-title"><a href="<?php esc_url(the_permalink())?>"><?php the_title() ?></a></h2>
            <ul class="entry-meta unstyled clearfix">
            	<li>
                	<i class="icon color_second icon-user"></i><?php the_author_posts_link(); ?>
            	</li>
                <?php 
				if(!empty($pixhealth_options['pixhealth_blog_show_category']) && $pixhealth_options['pixhealth_blog_show_category'] == 1){
					$pixhealth_categories = get_the_category($post->ID);
					if($pixhealth_categories){
						$pixhealth_output = '<li class="cat-meta-a"><i class="icon color_second icon-folder"></i>';						
						foreach($pixhealth_categories as $pixhealth_category) {
							$pixhealth_output .= '<a href="'.esc_url(get_category_link( $pixhealth_category->term_id )).'">'.wp_kses_post($pixhealth_category->cat_name).'</a> ';
						}
						$pixhealth_output .= '</li>';
						echo wp_kses_post($pixhealth_output);
					}
				}
				?>
                <?php if( 'open' == $post->comment_status && pixhealth_get_option('pixhealth_blog_show_comments',1)) : ?>
            	<li class="li-last">
                	<i class="icon color_second icon-eye"></i><?php comments_popup_link( __( '0', 'PixHealth' ), __( '1', 'PixHealth' ), __( '%', 'PixHealth' )); ?>
            	</li>
                <?php endif; ?>
            </ul>
            <div class="entry-content ui-text"><?php echo do_shortcode(get_the_excerpt()); ?></div>
            <footer class="entry-footer">
            	<a class="btn bg-color_primary" href="<?php esc_url(the_permalink())?>"><?php _e( 'READ MORE', 'PixHealth' ); ?></a>
            </footer>
    
