<?php
/**
 * The template includes blog post format gallery.
 *
 * @package Pix-Theme
 * @since 1.0
 */
$pixhealth_options = get_option('pixhealth_general_settings');	
	// get the gallery images
	$size = (is_front_page()) && !is_home() ? 'full' : 'blog-post';
	$gallery = class_exists( 'RW_Meta_Box' ) ? rwmb_meta('post_gallery', 'type=image&size='.$size.'') : '';
 
	$argsThumb = array(
		'order'          => 'ASC',
		'post_type'      => 'attachment',
		'post_parent'    => $post->ID,
		'post_mime_type' => 'image',
		'post_status'    => null,
		//'exclude' => get_post_thumbnail_id()
	);
	$attachments = get_posts($argsThumb);
	
	//$pixhealth_format = !in_array($pixhealth_format, array("quote", "gallery", "video")) ? "standared" : $pixhealth_format;
	$icon = array("standared" => "icon-picture", "quote" => "fa fa-pencil", "gallery" => "icon-camera", "video" => "fa fa-video-camera");
	
if ($gallery || $attachment){
?>

<div class="blog-slider">

  <ul data-pager="false" data-infinite-slides="true" data-move-slides="1" data-auto-slides="true" data-margin-slides="0" data-width-slides="770" data-min-slides="1" data-max-slides="1" class="bxslider slider_gallery">   
	<?php
        if($gallery){
            foreach ($gallery as $slide) {
                echo '
		<li>';
                echo '<img src="' . esc_url($slide['url']) . '" width="' . esc_attr($slide['width']) . '" height="' . esc_attr($slide['height']) . '" alt="' .esc_attr($slide['alt']).'" title="' .esc_attr($slide['title']). '" />';
                echo '
        </li>';
            }
        }elseif ($attachments) {
            foreach ($attachments as $attachment) {
                echo '
		<li>
			<img src="'.esc_url(wp_get_attachment_url($attachment->ID, 'full', false, false)).'" alt="'.esc_attr(get_post_meta($attachment->ID, '_wp_attachment_image_alt', true)).'" title="'.esc_attr(get_post_meta($attachment->ID, '_wp_attachment_image_title', true)).'" />
        </li>';
            }
        }
        
    ?>
</ul>
</div>
<?php 
}
else{
?>
<div class="blog-slider">
    <div class="owl-blog_carousel white-control owl-carousel owl-theme">
        <div class="owl-item">  
            <div class="owl-blog_carousel_img"> 
            	<a href="<?php esc_url(the_permalink())?>">
				<?php if ( has_post_thumbnail() ):?>
                <?php the_post_thumbnail(); ?>
                <?php else : ?>
                <img src="<?php echo esc_url(get_template_directory_uri()) ?>/images/noimage.jpg" alt="" />
                <?php the_post_thumbnail(); ?>
                <?php endif; ?>
                </a> 
             </div>
         </div>
	</div>
<?php	
}
?>
