<?php
/**
 * This template is for displaying part of blog.
 *
 * @package Pix-Theme
 * @since 1.0
 */
	$pixhealth_format  = get_post_format();
	$pixhealth_options = get_option('pixhealth_general_settings');
	$custom =  get_post_custom($post->ID);
	$layout = isset ($custom['_page_layout']) ? $custom['_page_layout'][0] : '1';
	
	$pixhealth_format = !in_array($pixhealth_format, array("quote", "gallery", "video")) ? "standared" : $pixhealth_format;
	
	$get_avatar = get_avatar(get_the_author_meta('ID'), 85);
	preg_match("/src=['\"](.*?)['\"]/i", $get_avatar, $matches);
	$src = !empty($matches[1]) ? $matches[1] : '';
	
?>

	<?php if ( has_post_thumbnail() ):?>
    <div class="hover__figure"><?php the_post_thumbnail(); ?></div>
    <div class="entry-autor"> <img src="<?php echo esc_url($src) ?>" height="82" width="80" alt="Author"></div> 
    <?php else : ?>
    <div class="hover__figure"><img src="<?php echo esc_url(get_template_directory_uri()) ?>/images/noimage.jpg"  /></div>
    <?php endif; ?>
      
