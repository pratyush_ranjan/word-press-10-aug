<?php
	// get meta options/values
	$pixhealth_options = get_option('pixhealth_general_settings');
	$pixhealth_content = class_exists( 'RW_Meta_Box' ) ? rwmb_meta('post_quote_content') : '';
	$pixhealth_source = class_exists( 'RW_Meta_Box' ) ? rwmb_meta('post_quote_source') : '';
	//$pixhealth_format  = get_post_format();
	//$pixhealth_format = !in_array($pixhealth_format, array("quote", "gallery", "video")) ? "standared" : $pixhealth_format;
$icon = array("standared" => "icon-picture", "quote" => "fa fa-pencil", "gallery" => "icon-camera", "video" => "fa fa-video-camera");
?>

<div class="post entry-review post-quotes">
    <i class="quotes fa fa-quote-left bg-color_primary"></i>
    <blockquote>
        <p><?php echo wp_kses_post($pixhealth_content); ?></p>
    </blockquote>
    
    <span class="entry-review__name text-right"><?php echo wp_kses_post($pixhealth_source); ?></span>

</div>


