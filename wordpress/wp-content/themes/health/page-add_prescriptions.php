<?php
/**
* The main template file.
*
* This is the most generic template file in a WordPress theme
* and one of the two required files for a theme (the other being style.css).
* It is used to display a page when nothing more specific matches a query.
* E.g., it puts together the home page when no home.php file exists.
* Learn more: http://codex.wordpress.org/Template_Hierarchy
*
* @package storefront
*/
get_header(); ?>
<div id="primary" class="content-area container">
	
	<?php
	global $wpdb;
	
	//################ Add prescriptions   ##################################
	                echo '<form action="' . esc_url( $_SERVER['REQUEST_URI'] ) . '" method="post" enctype="multipart/form-data">';
					echo '<p>';
				    echo 'Your prescriptions file (image) <br/>';
					echo '<input type="file" name="fileToUpload">';
					echo '</p>';
					echo '<p><input type="submit" name="cf-submitted" value="upload"></p>';
					echo '</form>';
	
	//########################## set image of product ############################
	
					// Check if image file is a actual image or fake image
					if(isset($_POST["cf-submitted"])) {
						
							$file_name = $_FILES['fileToUpload']['name'];
									$tmp_file_name = $_FILES['fileToUpload']['tmp_name'];
									$imageFileType = pathinfo($file_name,PATHINFO_EXTENSION);
									$new_fileName=md5(uniqid().current_time( 'mysql' )).'.'.$imageFileType;
									$upload_dir = wp_upload_dir();
									$file_path=$upload_dir['basedir'].'/'.$new_fileName;
	                                
	
									if($file_name){
	
									$status=move_uploaded_file($tmp_file_name,$file_path);
	                                  if($status == true){
	
		                               echo '<h3> File uploaded success !</h3>';
						//#################### post prescriptions data  ##############################
										$option_name = 'prescription_user_'.get_current_user_id().'_'.$new_fileName;
										$new_value =$new_fileName;
					
										// The option hasn't been added yet. We'll add it with $autoload set to 'no'.
										$deprecated = null;
															$autoload = 'no';
										add_option( $option_name, $new_value, $deprecated, $autoload );
										$post_id=$wpdb->get_results("SELECT option_id  FROM $wpdb->options WHERE option_name = '$option_name'");
										$_SESSION['prescription_id']=$post_id[0]->option_id;
																
										//##################################################
										}else{
							             echo '<h3> File uploads failed !</h3>';
						                   }
					                    }
					
					 }
					
	
	// ######################### end image of product set ########################
	$login_id=get_current_user_id();
	if($login_id == true){
	$post_id = $wpdb->get_results("SELECT option_id , option_value FROM $wpdb->options WHERE option_name LIKE '%prescription_user_" . $login_id . "%'");
	
	?>
	<!-- <p><span>prescription id</span>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<span>prescription file</span></p> -->
	<?php
	$upload_dir = wp_upload_dir();
	?>
	<div class="">
		<div class="">
			<?php
			if(isset($_POST["set_prescription"]) && isset( $_POST['prescription'])) {
			$_SESSION['prescription_id']=$_POST['prescription'];
			echo $_SESSION['prescription_id']." (your prescription has selected !)" ;
			}
			if(isset($_POST["unset_prescription"])) {
			$_SESSION['prescription_id']='';
			echo $_SESSION['prescription_id']." (your prescription is unselected !)" ;
			}
			?>
			<div class="row">
			<?php
			echo '<form action="' . esc_url( $_SERVER['REQUEST_URI'] ) . '" method="post" >';     
					foreach ($post_id as $key => $_value) {
				?>
				<div class="col-lg-3 col-md-3 col-sm-4 col-xs-12"><img src="<?php echo $upload_dir['baseurl'];?>/<?php echo $post_id[$key]->option_value; ?> " style="height:200px;width:290px;"><p class="text-center"><input type="radio"  name="prescription" value="<?php echo $post_id[$key]->option_id;?>"<?php echo  ( @$_SESSION['prescription_id'] == $post_id[$key]->option_id )? 'checked':'' ?> > <?php echo $post_id[$key]->option_id;?></p></div>
				<?php
				}
		     ?>
		     </div>
		     <div style="height : 10px;"></div>
		     <div class="row">
		     <div class="col-lg-12 text-center">
		     <?php 		
				echo '<p><input type="submit" name="set_prescription" value="set prescription "></p>';
			echo '</form>';
			//#################### unset prescriptions ###########################
			echo '<form action="' . esc_url( $_SERVER['REQUEST_URI'] ) . '" method="post" >';
				echo '<p><input type="submit" name="unset_prescription" value="unset prescription "></p>';
			echo '</form>';
			?>
			</div>
			</div>
		</div>


	</div>
	<?php
    }
	?>
	</div><!-- #primary -->
	<?php
	do_action( 'storefront_sidebar' );
	get_footer();