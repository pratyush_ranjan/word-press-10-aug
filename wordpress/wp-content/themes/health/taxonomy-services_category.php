<?php 
/*** The taxonomy for portfolio category. ***/
get_header(); 
	
	//global $paged;
 	$pixhealth_term = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) ); 
	
	
?>

<main class="main-content services">

      <div class="container">
      
      
      <div class="row">
          <div class="col-xs-12">
            <h2 class="ui-title-block text-center"><?php echo wp_kses_post($pixhealth_term->name) ?> <?php _e('Services', 'PixHealth') ?> <strong><?php _e('For Patients', 'PixHealth') ?></strong></h2>
            <div class="ui-subtitle-block"><?php _e('Our medical specialists care about you &amp; your family\'s health', 'PixHealth') ?></div>
          </div>
        </div>
        
        <i class="decor-brand"></i>
    
    
		<div class="row text-center"> 

        
						<?php
							$pixhealth_services = get_objects_in_term( $pixhealth_term->term_id, 'services_category');
							$args = array(
										'post_type' => 'services', 
										'orderby' => 'menu_order',
										'post__in' => $pixhealth_services,			 
										'order' => 'ASC'
									);
								
							$wp_query = new WP_Query( $args );
															
							if ($wp_query->have_posts()):
								while ($wp_query->have_posts()) :
									$wp_query->the_post();
									$pixhealth_thumbnail = get_the_post_thumbnail($post->ID, 'pixhealth-services-thumb');
						?>
									<div class="col-sm-4">
                                        <div class="services__item">
                                            <div class="service__figure">
                                                <div class="hover__figure"><?php echo wp_kses_post($pixhealth_thumbnail) ?></div>
                        <?php if ( get_post_meta( get_the_ID(), 'service_icon', true ) !== '' ) : ?>
                                                <span class="icon-round icon-round_small helper">
                                                    <i class="icon <?php echo esc_attr(get_post_meta( get_the_ID(), 'service_icon', true )) ?>"></i>
                                                </span> 
                        <?php endif; ?>
                                            </div>
                                            <h3 class="ui-title-inner"><?php echo wp_kses_post(get_the_title()) ?></h3>
                                            <p class="ui-text"><?php echo pixhealth_limit_words(get_the_excerpt(), 20) ?></p>
                                            <a class="btn btn_small" href="<?php echo esc_url(get_permalink(get_the_ID())) ?>"><?php _e("READ MORE", "PixHealth") ?></a> 
                                        </div>
                                    </div>
                        <?php
								endwhile;
							endif;
						?>
      

                    
		
			     
           
    	</div>
	</div>
</main>


<?php get_footer() ?>