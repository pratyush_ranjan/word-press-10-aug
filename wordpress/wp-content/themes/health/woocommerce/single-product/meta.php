<?php
/**
 * Single Product Meta
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $post, $product , $wpdb;

$pixhealth_cat_count = sizeof( get_the_terms( $post->ID, 'product_cat' ) );
$pixhealth_tag_count = sizeof( get_the_terms( $post->ID, 'product_tag' ) );

?>
<div class="product_meta">

	<?php do_action( 'woocommerce_product_meta_start' ); ?>

	<?php if ( wc_product_sku_enabled() && ( $product->get_sku() || $product->is_type( 'variable' ) ) ) : ?>

		<span class="sku_wrapper"><?php _e( 'SKU:', 'PixHealth' ); ?> <span class="sku" itemprop="sku"><?php echo ( $pixhealth_sku = $product->get_sku() ) ? wp_kses_post($pixhealth_sku) : __( 'N/A', 'PixHealth' ); ?></span>.</span>

	<?php endif; ?>

	<?php echo wp_kses_post($product->get_categories( ', ', '<span class="posted_in">' . _n( 'Category:', 'Categories:', $pixhealth_cat_count, 'PixHealth' ) . ' ', '.</span>' )); ?>

	<?php echo wp_kses_post($product->get_tags( ', ', '<span class="tagged_as">' . _n( 'Tag:', 'Tags:', $pixhealth_tag_count, 'PixHealth' ) . ' ', '.</span>' )); ?>

	<?php do_action( 'woocommerce_product_meta_end' ); ?>









	<!-- display post meta data packet size -->
<div>

	<h4>packet size : <?php echo get_post_meta( get_the_ID(), 'packet_size', true ); ?> <?php echo get_post_meta( get_the_ID(), 'Unit', true ); ?></h4>
	<h4>DrugForm : <?php echo get_post_meta( get_the_ID(), 'Drug_form', true ); ?></h4>
	<h4>PackForm : <?php echo get_post_meta( get_the_ID(), 'Pack_form', true ); ?></h4>
	<h4>Ingredients : <?php echo get_post_meta( get_the_ID(), 'Ingredients', true ); ?></h4>
	<h4>Users : <?php echo get_post_meta( get_the_ID(), 'Users', true ); ?></h4>
	<h4>Form : <?php echo get_post_meta( get_the_ID(), 'Form', true ); ?></h4>
	<h4>Rx/Otc : <?php echo get_post_meta( get_the_ID(), 'Rx/Otc', true ); ?></h4>
	
	<?php if( isset( get_post_meta(get_the_ID() , 'Salt_id' )[0])  ){ if( get_post_meta(get_the_ID() , 'Salt_id' )[0] != ''){ ?>
	<h4>Salts data :
		
		<?php 
		$saltid = get_post_meta(get_the_ID() , 'Salt_id' )[0];
		//echo $saltid;
		//echo "SELECT `option_value` FROM $wpdb->options WHERE option_name = 'salt_id_".$saltid."'";
		$salts  = $wpdb->get_results("SELECT `option_value` FROM $wpdb->options WHERE option_name = 'salt_id_".$saltid."'"); 

		$salts = json_decode( $salts[0]->option_value );
		 ?>

		 <table>
		 	<thead>
		 		<tr>
		 			<td>
		 				Name
		 			</td>

		 			<td>
		 				Strength
		 			</td>


		 		</tr>
		 	</thead>

		 	<tbody>
		 		
		 		<?php foreach($salts as $salt){ ?>
					<tr>
						<td>
							<?php  echo $salt->name; ?>
						</td>

						<td>
							<?php echo $salt->strength; ?>
						</td>

					</tr>

		 		<?php } ?>

		 	</tbody>
		 </table>

	 <?php } } ?>
	 </h4>
</div>
</div>
