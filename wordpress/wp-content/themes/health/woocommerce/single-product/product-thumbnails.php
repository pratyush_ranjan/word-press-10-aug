<?php
/**
 * Single Product Thumbnails
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.3.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $post, $product, $woocommerce;

$pixhealth_attachment_ids = $product->get_gallery_attachment_ids();

if ( $pixhealth_attachment_ids ) {
	?>
    <div id="carousel" class="flexslider">
	<ul class="slides"><?php

		$pixhealth_image_title 		= esc_attr( get_the_title( get_post_thumbnail_id() ) );
		$pixhealth_image_link  		= wp_get_attachment_url( get_post_thumbnail_id() );
		$pixhealth_image       		= get_the_post_thumbnail( $post->ID, apply_filters( 'single_product_large_thumbnail_size', 'shop_thumbnail' ), array(
			'title' => $pixhealth_image_title
			) );
			
		echo apply_filters( 'woocommerce_single_product_image_html', sprintf( '<li>%s</li>', $pixhealth_image ), $post->ID );
		
		foreach ( $pixhealth_attachment_ids as $pixhealth_attachment_id ) {

			$pixhealth_image_link = wp_get_attachment_url( $pixhealth_attachment_id );

			$pixhealth_image       = wp_get_attachment_image( $pixhealth_attachment_id, apply_filters( 'single_product_small_thumbnail_size', 'shop_thumbnail' ) );
			$pixhealth_image_class = esc_attr('');
			$pixhealth_image_title = esc_attr( get_the_title( $pixhealth_attachment_id ) );

			echo apply_filters( 'woocommerce_single_product_image_thumbnail_html', sprintf( '<li>%s</li>', $pixhealth_image ), $pixhealth_attachment_id, $post->ID, $pixhealth_image_class );

		}

	?></ul>
    </div>
	<?php
}