<?php
/**
 * Single Product Image
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.0.14
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $post, $woocommerce, $product;

?>
<div class="clearfix" id="image-block">
	<div id="slider-product" class="flexslider">
    	<ul class="slides">     
       
       <?php
		if ( has_post_thumbnail() ) {

			$pixhealth_image_title 		= esc_attr( get_the_title( get_post_thumbnail_id() ) );
			$pixhealth_image_link  		= wp_get_attachment_url( get_post_thumbnail_id() );
			$pixhealth_image       		= get_the_post_thumbnail( $post->ID, apply_filters( 'single_product_large_thumbnail_size', 'shop_single' ), array(
				'title' => $pixhealth_image_title
				) );
				
			$pixhealth_attachment_ids = $product->get_gallery_attachment_ids();
			
			echo apply_filters( 'woocommerce_single_product_image_html', sprintf( '<li><a rel="prettyPhoto[gallery1]" href="%s">%s</a></li>', $pixhealth_image_link, $pixhealth_image ), $post->ID );			
			foreach ( $pixhealth_attachment_ids as $pixhealth_attachment_id ) {

				$pixhealth_image_link = wp_get_attachment_url( $pixhealth_attachment_id );
	
				$pixhealth_image       = wp_get_attachment_image( $pixhealth_attachment_id, apply_filters( 'single_product_small_thumbnail_size', 'shop_single' ) );
				$pixhealth_image_class = '';
				$pixhealth_image_title = esc_attr( get_the_title( $pixhealth_attachment_id ) );
	
				echo apply_filters( 'woocommerce_single_product_image_thumbnail_html', sprintf( '<li><a rel="prettyPhoto[gallery1]" href="%s" title="%s" >%s</a></li>', $pixhealth_image_link, $pixhealth_image_title, $pixhealth_image ), $pixhealth_attachment_id, $post->ID, $pixhealth_image_class );
	
			}
		} else {

			echo apply_filters( 'woocommerce_single_product_image_html', sprintf( '<li><a rel="prettyPhoto" href="%s"><img src="%s" alt="Placeholder" /></a></li>', woocommerce_placeholder_img_src() ), $post->ID );

		}
	?>
    	</ul>
	</div>

	
	<?php do_action( 'woocommerce_product_thumbnails' ); ?>
    
</div>
