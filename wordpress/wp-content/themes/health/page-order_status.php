<?php
/**
* The main template file.
*
* This is the most generic template file in a WordPress theme
* and one of the two required files for a theme (the other being style.css).
* It is used to display a page when nothing more specific matches a query.
* E.g., it puts together the home page when no home.php file exists.
* Learn more: http://codex.wordpress.org/Template_Hierarchy
*
* @package storefront
*/
get_header(); ?>

<div id="primary" class="content-area">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				
				<section id="cd-timeline" class="cd-container">
					
					
					<div class="cd-timeline-block" id="dob"><!-- START DEFAULT BLOCK -->
					<div title="Add Event" id="date-of-birth" data-toggle="tooltip" data-placement="top"  class="cd-timeline-img cd-picture">
						<div class="timeline-time">
							<time> DOB HERE </time>
						</div>
					</div>
					<div class="cd-timeline-content bg-text col-xs-6 timeline-hack" id="date-of-birth-container">
						<div>BORN : 13/13/1313</div>
					</div>
					<!-- cd-timeline-img -->
					<!-- cd-timeline-content -->
					
					</div><!-- END DEFAULT BLOCK -->
					
					<div class="cd-timeline-block" id="SOMEID"><!-- START BLOCK -->
					<div class="cd-timeline-img cd-picture">
						<div class="timeline-time">
							<time>
							<subtime  id="tl-ID-date">DATE HERE</subtime>
						<achievement  id="tl-ID-type" class="award-icon"><img src = "http://theatrekingdom.com/assets/img/medal.png" alt="Award"/></achievement>
						</time>
					</div></div>
					<!-- cd-timeline-img -->
					<div class="cd-timeline-content  timeline-content-award  bg-text col-xs-6 timeline-hack">
						<h2 id="tl--title">TITLE</h2>
						<p id="tl--desc"> DESC </p>
						<!--  <p><button class="btn btn-primary tk-edit-tl" data="ID">Edit</button> <button class="btn btn-primary delete-tl-item" data="ID">Delete</button></p> -->
					</div>
					<!-- cd-timeline-content -->
					</div><!-- END BLOCK -->
					<div class="cd-timeline-block" id="SOMEID1"><!-- START BLOCK -->
					<div class="cd-timeline-img cd-picture">
						<div class="timeline-time">
							<time>
							<subtime  id="tl-ID-date">DATE HERE</subtime>
						<achievement  id="tl-ID-type" class="award-icon"><img src = "http://theatrekingdom.com/assets/img/medal.png" alt="Award"/></achievement>
						</time>
					</div></div>
					<!-- cd-timeline-img -->
					<div class="cd-timeline-content  timeline-content-award  bg-text col-xs-6 timeline-hack">
						<h2 id="tl--title">TITLE</h2>
						<p id="tl--desc"> DESC </p>
						<!-- <p><button class="btn btn-primary tk-edit-tl" data="ID">Edit</button> <button class="btn btn-primary delete-tl-item" data="ID">Delete</button></p> -->
					</div>
					<!-- cd-timeline-content -->
					</div><!-- END BLOCK -->
					
					<div class="cd-timeline-block" id="att"><!-- START DEFAULT BLOCK -->
					<div title="Add Event" id="add-to-timeline" data-toggle="tooltip" data-placement="top"  class="cd-timeline-img cd-picture cd-add-timeline"></div>
					<!-- cd-timeline-img -->
					<!-- cd-timeline-content -->
					</div><!-- END DEFAULT BLOCK -->
				</section>
				
				<!-- TESTIMONIAL -->
			</div>
		</div>
		
		<!-- ========================================================= -->
		<!-- ====================End Main Content===================== -->
		<!-- ========================================================= -->
	</div>
</div>
</div>
</div>
</div>
</div>