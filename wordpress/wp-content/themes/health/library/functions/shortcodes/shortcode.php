<?php
define ( 'JS_PATH' , get_template_directory_uri().'/library/functions/shortcodes/shortcode.js');

$GLOBALS['middletab_count']=0;
$GLOBALS['middletab_active_count']=0;
$GLOBALS['middletabs']='';
$GLOBALS['vertical_count']=0;
$GLOBALS['vertical_active_count']=0;
$GLOBALS['verticals']='';
$GLOBALS['caurusel_count']=0;
$GLOBALS['caurusel_items']='';
$GLOBALS['review_count']=0;
$GLOBALS['reviews']='';
$GLOBALS['offer_count']=0;
$GLOBALS['offers']='';
$GLOBALS['testimonial_count']=0;
$GLOBALS['testimonial_active_count']=0;
$GLOBALS['testimonials']='';
$GLOBALS['member_count']=0;
$GLOBALS['members']='';
$GLOBALS['social_count']=0;
$GLOBALS['socials']='';
$GLOBALS['pricecol_count']=0;
$GLOBALS['pricecols']='';
$GLOBALS['itemoptions']='';


add_action('admin_head','html_quicktags');
function html_quicktags() {

	$output = "<script type='text/javascript'>\n
	/* <![CDATA[ */ \n";
	wp_print_scripts( 'quicktags' );
	
	
	
	$buttons[] = array(
		'name' => 'thin_title',
		'options' => array(
			'display_name' => 'thin_title',
			'open_tag' => '\n[thin_title]',
			'close_tag' => '[/thin_title]\n',
			'key' => ''
	));

  $buttons[] = array(
		'name' => 'highlight_text',
		'options' => array(
			'display_name' => 'highlight_text',
			'open_tag' => '\n[highlight_text]',
			'close_tag' => '[/highlight_text]\n',
			'key' => ''
	));
	
	
	
	$buttons[] = array(
		'name' => 'btn_icon',
		'options' => array(
			'display_name' => 'btn_icon',
			'open_tag' => '\n[btn_icon  link="http://google.com"  type="right"]',
			'close_tag' => '[/btn_icon]\n',
			'key' => ''
	));
	
	
		$buttons[] = array(
		'name' => 'btn_download',
		'options' => array(
			'display_name' => 'btn_download',
			'open_tag' => '\n[btn_download  link="http://google.com" ]',
			'close_tag' => '[/btn_download]\n',
			'key' => ''
	));
	
	
	
	$buttons[] = array(
		'name' => 'icon_box',
		'options' => array(
			'display_name' => 'icon_box',
			'open_tag' => '\n[icon_box  icon="fa-flag"  type="fa-2x"]',
			'close_tag' => '[/icon_box]\n',
			'key' => ''
	));
	
	
	
	
	$buttons[] = array(
		'name' => 'marked_list1',
		'options' => array(
			'display_name' => 'marked_list1',
			'open_tag' => '\n[marked_list1]',
			'close_tag' => '[/marked_list1]\n',
			'key' => ''
	));
	
	
	$buttons[] = array(
		'name' => 'marked_list2',
		'options' => array(
			'display_name' => 'marked_list2',
			'open_tag' => '\n[marked_list2]',
			'close_tag' => '[/marked_list2]\n',
			'key' => ''
	));

			
	for ($i=0; $i <= (count($buttons)-1); $i++) {
		$output .= "edButtons[edButtons.length] = new edButton('ed_{$buttons[$i]['name']}'
			,'{$buttons[$i]['options']['display_name']}'
			,'{$buttons[$i]['options']['open_tag']}'
			,'{$buttons[$i]['options']['close_tag']}'
			,'{$buttons[$i]['options']['key']}'
		); \n";
	}
	
	$output .= "\n /* ]]> */ \n
	</script>";
	echo $output;
}

function pixhealth_addbuttons() {
	if ( ! current_user_can('edit_posts') && ! current_user_can('edit_pages') )
		return;

	if ( get_user_option('rich_editing') == 'true') {
		add_filter("mce_external_plugins", "add_pixhealth_custom_tinymce_plugin");
		add_filter('mce_buttons_3', 'register_pixhealth_custom_button');
	}
}
function register_pixhealth_custom_button($buttons) {
	array_push(
		$buttons,
		"title_block",
		"title_image",
		"thin_title",
		"highlight_text",
	   "banner",
	   "btn_download",
		"content_block",
		"VideoBox",
		"FeatServ",
		"FeatServ2",
		"DealPanel",
		"ReviewsPanel",
		"Progress",
		"AddButton",
		"Dropdown",
		"Tabs",
		"fTabs",
		"AboutTabs",
		"Toggle",
		"Accordion",
		"Testimonial",
		"btn_icon",
		"Banner",
		"Carousel",
		"Contact",
		"Fblock",
		"Tblock",
		"Offerblock",
		"BrandBlock",
		"hexagon",
		"state"
		
		); 
	return $buttons;
} 
function add_pixhealth_custom_tinymce_plugin($plugin_array) {
	$plugin_array['PixHealthShortcodes'] = JS_PATH;
	return $plugin_array;
}
add_action('init', 'pixhealth_addbuttons');


/******************* thin_title *******************/

function alc_thin_title( $atts, $content = null ) {
     extract(shortcode_atts(array(
		"title"=>''
	), $atts));	
	$out = '<h2 class="ui-title-block ui-title-block_small">'.wp_kses_post($content).'</h2>';
   return $out;
}
add_shortcode('thin_title', 'alc_thin_title');



/******************* highlight_text *******************/

function alc_highlight_text( $atts, $content = null ) {
     extract(shortcode_atts(array(
		"title"=>''
	), $atts));	
	$out = '<span class="highlight_text">'.wp_kses_post($content).'</span>';
   return $out;
}
add_shortcode('highlight_text', 'alc_highlight_text');







/******************* btn_icon *******************/

function alc_btn_icon( $atts, $content = null ) {
     extract(shortcode_atts(array(
		"link"=>'',
		"type"=>''
	), $atts));	
	$out = '<div class="btn-icon-wrap"><a href="'.esc_url($link).'" class="btn  btn-primary btn-icon-'.esc_attr($type).' ">'.wp_kses_post($content).'
          <div class="btn-icon"><i class="fa fa-long-arrow-'.esc_attr($type).'"></i></div>
          </a></div>';
   return $out;
}
add_shortcode('btn_icon', 'alc_btn_icon');


/******************* btn_download *******************/

function alc_btn_download( $atts, $content = null ) {
     extract(shortcode_atts(array(
		"link"=>''
	), $atts));	
	$out = '
	
	<a class="btn-download" href="'.esc_url($link).'"><i class="fa fa-file-pdf-o"></i>'.wp_kses_post($content).'</a>
	
';
   return $out;
}
add_shortcode('btn_download', 'alc_btn_download');





/******************* icon_box *******************/

function alc_icon_box( $atts, $content = null ) {
     extract(shortcode_atts(array(
		"icon"=>'',
		"type"=>''
	), $atts));	
	$out = '<span class="icon_box_wrap"><i class="fa '.esc_attr($icon).'  '.esc_attr($type).'"></i></span>';
   return $out;
}
add_shortcode('icon_box', 'alc_icon_box');


/******************* marked_list1 *******************/

function alc_marked_list1( $atts, $content = null ) {
     extract(shortcode_atts(array(
		"icon"=>'',
		"type"=>''
	), $atts));	
	$out = '<div class="list-mark list-mark_big">'.wp_kses_post($content).'</div>';
   return $out;
}
add_shortcode('marked_list1', 'alc_marked_list1');


/******************* marked_list2 *******************/

function alc_marked_list2( $atts, $content = null ) {
     extract(shortcode_atts(array(
		"icon"=>'',
		"type"=>''
	), $atts));	
	$out = '<div class="marked_list2">'.wp_kses_post($content).'</div>';
   return $out;
}
add_shortcode('marked_list2', 'alc_marked_list2');


/********************* TITLE BLOCK**********************/

function pixhealth_title_block( $atts, $content = null ) {
   return '
   <h4 class="title_block"><span>' . do_shortcode($content) . '</span></h4>';
}
add_shortcode('title_block', 'pixhealth_title_block');

function pixhealth_content_block( $atts, $content = null ) {
   return '<div class="content_block">' . do_shortcode($content) . '</div>';
}
add_shortcode('content_block', 'pixhealth_content_block');

 





		  
	
/********************* VC TITLE IMAGE **********************/

function pixhealth_title_image( $atts, $content = null ) {
	$out = $image = '';
 extract(shortcode_atts(array(
		"title" => '',
		"image" => $image,
		"after" => ''
	), $atts));	
	
	$img = wp_get_attachment_image_src( $image, 'large' );
		
	$img_output = $img[0];
	
	$out = '
	
	<div class="row">
      <div class="col-lg-12 text-center">
	<div data-animation="bounceInUp" class="heading-wrap animated animation-done bounceInUp">
          <div class="small-logo"><img width="106" height="36" src="'.esc_url($img_output).'" alt="logo"></div>
          <h2 class="section-heading">'.wp_kses_post($title).'</h2>
          <h3 class="section-subheading hang">'.wp_kses_post($after).'</h3>
        </div>     </div>     </div>
	
	<div class="text-center after-title-info">   '.do_shortcode($content).'  </div>
	   

	
	';
		
    return $out;	
}

add_shortcode('title_image', 'pixhealth_title_image');

/**************************************************/	  

		  
	
/********************* VC PROMO **********************/

function pixhealth_promo( $atts, $content = null ) {
	$out = $image = '';
 extract(shortcode_atts(array(
		"title" => '',
		"image" => $image,
		"link" => '',
		"text" => ''
	), $atts));	
	
	$img = wp_get_attachment_image_src( $image, 'large' );
		
	$img_output = $img[0];
	
	$out = '
	
	
<div class="promo-item">
            <div class="overlay-promo"><a class="btn" href="'.esc_url($link).'" >'.wp_kses_post($text).'</a></div>
            <a href="'.esc_url($link).'"><img src="'.esc_url($img_output).'"></a>
            <div class="promo-caption">
              <div class="promo-content">
               '.do_shortcode($content).'
              </div>
            </div>
          </div>
	
	';
		
    return $out;	
}

add_shortcode('promo', 'pixhealth_promo');

/**************************************************/	  


/********************* VC FEATURED SERVICES **********************/

function pixhealth_featserv( $atts, $content = null ) {
 extract(shortcode_atts(array(
		"title" => '',
		"icon" => '' 
	), $atts));	
	
	$out = '';
	$finaltitle = ($title == '') ? '': ' <h4 class="service-heading">'.wp_kses_post($title).'</h4>';
	$finallicon = ($icon == '') ? '': '<i class="fa '.esc_attr($icon).'"></i>';
	
	$out = '
	
	<div  class="service-item text-center">
          <div class="service-icon"> '.wp_kses_post($finallicon).'</div>
         	'.wp_kses_post($finaltitle).'

	'.do_shortcode($content).'
	</div>'; 
		
    return $out;	
}

add_shortcode('featserv', 'pixhealth_featserv');

/**************************************************/







/********************* VC FEATURED SERVICES 2 **********************/

function pixhealth_featserv2( $atts, $content = null ) {
 extract(shortcode_atts(array(
		"title" => '',
		"icon" => '' 
	), $atts));	
	
	$out = '';
	$finaltitle = ($title == '') ? '': '<h4 class="sub-title  text-left">'.wp_kses_post($title).'</h4>';
	$finallicon = ($icon == '') ? '': '<i class="fa '.esc_attr($icon).'"></i>';
	
	$out = '
	
	<div class="ft-box text-center">
          <div class="ft-icon-box "> '.wp_kses_post($finallicon).'</div>
          <hr style="max-width:30px;">
          <h4>'.wp_kses_post($finaltitle).'</h4>
	'.do_shortcode($content).'
	   </div>'; 
		
    return $out;	
}

add_shortcode('featserv2', 'pixhealth_featserv2');

/**************************************************/



/********************* DEAl PANEL **********************/

function pixhealth_dealpan( $atts, $content = null ) {
 extract(shortcode_atts(array(
		"title" => '',
		"icon" => '',
		"offers" => '', 
		"href" => '' 
	), $atts));	
	
	$out = '';
	$finaltitle = ($title == '') ? '': wp_kses_post($title);
	$finallicon = ($icon == '') ? '': '<i class="fa '.esc_attr($icon).'"></i>';
	$finalloffers = ($offers == '') ? '': '    <span class="chart" data-percent="'.esc_attr($offers).'"> <span class="percent"></span> </span>  ';
	$finallhref = ($href == '') ? '#': $href;
	
	
	$out = '<div class="featured-item-simple-icon text-center" >
              <div class="ft-icons-simple"> '.wp_kses_post($finallicon).' </div>
			  
			  '.wp_kses_post($finalloffers).'
			  
           
              <h6>'.wp_kses_post($finaltitle).'  </h6>
            </div> '; 

		
    return $out;	
}

add_shortcode('dealpan', 'pixhealth_dealpan');

/**************************************************/

/********************* REVIEWS PANEL **********************/

function pixhealth_review_group( $atts, $content ) {
	
	$GLOBALS['review_count'] = 0;
	
	do_shortcode( $content );
	if( is_array( $GLOBALS['reviews'] ) ){
		$count = 1;
		foreach( $GLOBALS['reviews'] as $review ){
			
			$finalimage = ($review['image'] == '') ? '': '<div class="avatar-review"><img src="'.esc_url($review['image']).'" alt="Avatar"></div>';
			$finalname = ($review['name'] == '') ? '': '<h3 class="heading">'.wp_kses_post($review['name']).'</h3>';
			$finaljob = ($review['job'] == '') ? '': '<h4 class="sub-heading">'.wp_kses_post($review['job']).'</h4>';
			
			$out = '<li> <div data-animation="fadeIn" class="team-member-item animated  fadeIn ">';
			$out .= 	$finalimage;
			$out .= '	<div class="details-review">';
			$out .= '		<div class="desc-det">'.do_shortcode($review['content']).'</div>';
			$out .= '		<div class="review-autor">'.wp_kses_post($finalname.$finaljob).'</div>';
			$out .= '	</div>';
			$out .= '</div> </li>';			
			
			$reviews[] = $out;
			
			$count ++;
		}
                
		$return = ' <div data-animation="bounceInRight" class="animated reviews-frame">
        				<ul class="review-slider" >
							'.implode( "\n", $reviews ).'
						</ul>
					</div>
					 <div class="sly_scrollbar">
      <div class="handle"></div>
    </div>';	
	}
	return $return;
			
}

add_shortcode('reviewgroup', 'pixhealth_review_group');



/*************** TESTIMONIALS ********************/
function pixhealth_testimonial( $atts, $content = null ) {
    extract(shortcode_atts(array(
		"authorname"	=> '', 
		"authorposition"	=> ''
	), $atts));

	$out = '<div class="testimonial-block"><div class="testimonial-content"><p>'.do_shortcode($content).'</p></div><cite>'.wp_kses_post($authorname).'</cite><p class="test_author">'.wp_kses_post($authorposition).'</p></div>';
    return $out;
}
add_shortcode('testimonial', 'pixhealth_testimonial');

/************************************************/


add_shortcode( 'icontexttab', 'pixhealth_vertical_tab' );
add_shortcode('icontexttabs', 'pixhealth_vertical_tabs');

// changed by VC dev team
function pixhealth_vertical_tabs( $atts, $content = null ) {
	$output='';
	
	extract(shortcode_atts(array(
	 	"title" => '',
		"title_strong" => '',
		"title_desc" => '',
	), $atts));
	
    $GLOBALS['verticals'] = array();
	$GLOBALS['vertical_active_count'] = 1;
	$content_do = do_shortcode($content);	
	$GLOBALS['vertical_count'] = 0;
	if( is_array( $GLOBALS['verticals'] ) && ! empty( $GLOBALS['verticals'] ) ){
		$i=0;
		foreach( $GLOBALS['verticals'] as $vertical ){
			$i++;
			$active = $vertical['active'] ? 'active' : '';
			$last = $i == count($GLOBALS['verticals']) ? 'li-last' : '';
			$tabs[] = '
					<li class="'.esc_attr($active).' '.esc_attr($last).'">
              			<a href="#icontab'.esc_attr($vertical['id']).'" aria-controls="icontab'.esc_attr($vertical['id']).'"  data-toggle="tab"><span class="icon-round bg-color_second helper "><i class="icon fa '.esc_attr($vertical['icon']).'"></i></span></a>
					</li>
					';								   
		}
		
		$strong_t = ($title_strong == '') ? '' : '<strong>'.wp_kses_post($title_strong).'</strong>';        
		$output = '
		
		<div class="icon-tabs-wrapper">
			<div class="container">
				<div class="row">
					<div class="col-xs-12 text-center">
						<h2 class="ui-title-block color_white">'.wp_kses_post($title).$strong_t.'</h2>
						<div class="ui-subtitle-block no-spacing">'.wp_kses_post($title_desc).'</div>
					</div>
					<div class="icon-tabs">
				  		<div class="col-md-6">
							<ul class="list-icons ">
								'.implode( "\n", $tabs ).'              
							</ul>				  
				  		</div>
				  		<div class="col-md-6 text-right">					
							<!-- Tab panes -->
							<div class="tab-content">
								' . $content_do . '
							</div>
				  		</div>
					</div>
				</div>
			  	<!-- end row --> 
			</div>  
		</div>
			
		';		        
	}
	return $output;   
}

// rewritted by vc dev team
function pixhealth_vertical_tab( $atts, $content ){
	
	$out = $image = '';
		
	extract(shortcode_atts(array(
	 	"title" => '',
		"image" => $image,
	 	"icon" => '',
	), $atts));
	
	$img = wp_get_attachment_image_src( $image, 'large' );
		
	$img_output = $img[0];
	
	$x = isset($GLOBALS['vertical_count'])?$GLOBALS['vertical_count']:0;
    $randomId = mt_rand(0, 100000);
    $count = isset($GLOBALS['vertical_active_count'])?$GLOBALS['vertical_active_count']:0;
	$GLOBALS['verticals'][$x] = array( 'title' => $title, 'image' => $img_output, 'icon' => $icon, 'id'=> $randomId, 'active'=>$count===1, 'content' => $content );
	$GLOBALS['vertical_count']++;
    
	$active = $count == 1 ? ' active' : '';
	$cont = '
			<div role="tabpanel" class="tab-pane '.esc_attr($active).'" id="icontab'.esc_attr($randomId).'">
				<img src="'.esc_url($img_output).'" alt="Foto'.esc_attr($randomId).'">
              	<div class="tab-info">              
                	<p class="title-small">'.wp_kses_post($title).'</p>
            		<p class="ui-text text-center color_light-grey">'.do_shortcode($content).'</p>            
            	</div>
            </div>';
	
    $GLOBALS['vertical_active_count']++;
	return $cont;
}

add_shortcode( 'tab_middle', 'pixhealth_tab_middle' );
add_shortcode('tabs_middle', 'pixhealth_tabs_middle');

// changed by VC dev team
function pixhealth_tabs_middle( $atts, $content = null ) {
	$output='';
	
	extract(shortcode_atts(array(
	 	"title" => '',
		"title_strong" => '',
		"title_desc" => '',
	), $atts));
	
    $GLOBALS['middletabs'] = array();
	$GLOBALS['middletab_active_count'] = 1;
	$content_do = do_shortcode($content);	
	$GLOBALS['middletab_count'] = 0;
	if( is_array( $GLOBALS['middletabs'] ) && ! empty( $GLOBALS['middletabs'] ) ){
		$i=0;
		foreach( $GLOBALS['middletabs'] as $middletab ){
			$i++;
			$active = $middletab['active'] ? 'active' : '';
			$last = $i == count($GLOBALS['middletabs']) ? 'li-last' : '';
			$tabs[] = '
					<li class="'.esc_attr($active).' '.esc_attr($last).'">
						<a href="#tab'.esc_attr($middletab['id']).'" data-toggle="tab">'.wp_kses_post($middletab['title']).'</a>
					</li>
					';								   
		}
		       
		$output = '
		
		<div class="pix-middle-tabs-wrapper" >
			<ul class="nav nav-tabs">
				'.implode( "\n", $tabs ).'
			</ul>
			
			<div class="tab-content">
				' . $content_do . '				
			</div><!-- end tab-content -->
		</div>
			
		';		        
	}
	return $output;   
}

// rewritted by vc dev team
function pixhealth_tab_middle( $atts, $content ){
	
	$out = $image = '';
		
	extract(shortcode_atts(array(
	 	"title" => '',
		"title_cont" => '',
		"image" => $image,
	 	"icon" => '',
	), $atts));
	
	$img = wp_get_attachment_image_src( $image, 'large' );
		
	$img_output = $img[0];
	
	$x = isset($GLOBALS['middletab_count'])?$GLOBALS['middletab_count']:0;
    $randomId = mt_rand(0, 100000);
    $count = isset($GLOBALS['middletab_active_count'])?$GLOBALS['middletab_active_count']:0;
	$GLOBALS['middletabs'][$x] = array( 'title' => $title, 'image' => $img_output, 'title_cont' => $title_cont, 'id'=> $randomId, 'active'=>$count===1, 'content' => $content );
	$GLOBALS['middletab_count']++;
    
	$active = $count == 1 ? ' active' : '';
	$cont = '
			<div class="tab-pane '.esc_attr($active).'" id="tab'.esc_attr($randomId).'">
			<img src="'.esc_url($img_output).'" height="200" width="570" alt="Foto'.esc_attr($randomId).'">
			<h2 class="ui-title-inner">'.wp_kses_post($title_cont).'</h2>
			'.do_shortcode($content).'
			</div>
			';
	
    $GLOBALS['middletab_active_count']++;
	return $cont;
}

/*************** Carousel Image Info ****************/

function pixhealth_caurusel_reviews( $atts, $content = null ) {
    $output = '';
	
	extract(shortcode_atts(array(
	 	'reviews_per_page' => '',
		'disable_carousel' => '',
	), $atts));
	
	$GLOBALS['reviews'] = array();
	do_shortcode($content);	 	
	
	if( is_array( $GLOBALS['reviews'] ) ){
		$count = 1;
		foreach( $GLOBALS['reviews'] as $item ){
						
			$finaldesc = ($item['short_desc'] == '') ? '': '<span class="categories">'.wp_kses_post($item['short_desc']).'</span>';
			$finalplace = ($item['place'] == '') ? '': '<span class="categories">'.wp_kses_post($item['place']).'</span>';
			
			$out = '
			<li class="slide">
				<div class="info"> 
					<img class="avatar" src="'.esc_attr($item['image']).'" height="130" width="130" alt="'.esc_attr($item['title']).'"> 
					<span class="name">'.wp_kses_post($item['title']).'</span> 
					'.wp_kses_post($finaldesc.$finalplace).'
				</div>
				<div class="quote">
					<blockquote>
					  '.do_shortcode($item['content']).'
					</blockquote>
				</div>
			</li>
			
			';			
			
			$reviews[] = $out;
			 
			$count ++;
		}
        
		$div_slides = $reviews_per_page == 2 ? 'col-xs-12' : 'col-md-11';
		$max_slides = $reviews_per_page == 2 ? 2 : 1;
		$min_slides = 1;
		$width_slides = $reviews_per_page == 2 ? 570 : 1000;
		$margin_slides = $reviews_per_page == 2 ? 30 : 0;
		$auto_slides = 'false';
		$move_slides = 1;
		$infinite_slides = 'false';
		$disable_carousel = $disable_carousel != '0' ? 'bxslider' : 'carousel-disable';
				       
		$output = ' 
			<div class="'.esc_attr($div_slides).' slider-reviews  slider-reviews_1-col  ">	
				<ul class="'.esc_attr($disable_carousel).'" 
						data-max-slides="'.esc_attr($max_slides).'" 
						data-min-slides="'.esc_attr($min_slides).'" 
						data-width-slides="'.esc_attr($width_slides).'" 
						data-margin-slides="'.esc_attr($margin_slides).'" 
						data-auto-slides="'.esc_attr($auto_slides).'" 
						data-move-slides="'.esc_attr($move_slides).'"   
						data-infinite-slides="'.esc_attr($infinite_slides).'" 
						data-controls="false">
				
							'.implode( "\n", $reviews ).'							
												
				</ul>
			</div>
		';
					
	}
	return $output;
	
   
}
add_shortcode('caurusel_reviews', 'pixhealth_caurusel_reviews');

///////////////////////////

function pixhealth_caurusel_review( $atts, $content ){
	$image = '';
	extract(shortcode_atts(array(
	 	'image' => $image,
		'title' => '',
		'short_desc' => '',
		'place' => '',
		//'button_text' => '',
		//'link' => '',
	), $atts));
	
	$img = wp_get_attachment_image_src( $image, 'full' );		
	$img_output = $img[0];
	
	$GLOBALS['reviews'][] = array( 'image' => $img_output, 'title' => $title, 'short_desc' => $short_desc, 'place' => $place, /*'button_text' => $button_text, 'link' => $link,*/ 'content' => $content );
	
}
add_shortcode( 'caurusel_review', 'pixhealth_caurusel_review' );
/************************************************/


/*************** Carousel Offers ****************/

function pixhealth_caurusel_offers( $atts, $content = null ) {
    $output = '';
	extract(shortcode_atts(array(
		'max_slides' => '',
		'min_slides' => '',
		'width_slides' => '',
		'margin_slides' => '',
		'auto_slides' => '',
		'move_slides' => '',
		'infinite_slides' => '',
		'disable_carousel' => '',
	), $atts));
	
	$GLOBALS['offers'] = array();
	do_shortcode($content);	 	
	
	if( is_array( $GLOBALS['offers'] ) ){
		$count = 1;
		foreach( $GLOBALS['offers'] as $item ){
			$class = $count == 1 ? 'class="active"' : '';
			
			$out = '
			<li>
                <div class="media">
					<a href="'.esc_url($item['link']).'"><img src="'.esc_attr($item['image']).'" alt="'.esc_attr($item['title']).'"/></a>
				</div>
				<div class="carousel-item-content">
					<a href="'.esc_url($item['link']).'" class="carousel-title">'.wp_kses_post($item['title']).'</a>
					<div class="carousel-text">
						'.do_shortcode($item['content']).' 
					</div>					
                </div>
			</li>';
			
			$offers[] = $out;
			 
			$count ++;
		}
        
		$max_slides = $max_slides == '' ? 2 : $max_slides;
		$min_slides = $min_slides == '' ? 2 : $min_slides;
		$width_slides = $width_slides == '' ? 700 : $width_slides;
		$margin_slides = $margin_slides == '' ? 10 : $margin_slides;
		$auto_slides = $auto_slides != '1' ? 'false' : 'true';
		$move_slides = $move_slides == ''? 1 : $move_slides;
		$infinite_slides = $infinite_slides != '0' ? 'true' : 'false';
		$disable_carousel = $disable_carousel != '0' ? 'bxslider' : 'carousel-disable';
		       
		$output = ' 
				<section class="carousel carousel-3">
					<ul class="'.esc_attr($disable_carousel).'" 
						data-mode="vertical" 
						data-max-slides="'.esc_attr($max_slides).'" 
						data-min-slides="'.esc_attr($min_slides).'" 
						data-width-slides="'.esc_attr($width_slides).'" 
						data-margin-slides="'.esc_attr($margin_slides).'" 
						data-auto-slides="'.esc_attr($auto_slides).'" 
						data-move-slides="'.esc_attr($move_slides).'"   
						data-infinite-slides="'.esc_attr($infinite_slides).'" >
						'.implode( "\n", $offers ).'
					</ul>
				</section>';
					
	}
	return $output;
	
   
}
add_shortcode('caurusel_offers', 'pixhealth_caurusel_offers');

///////////////////////////

function pixhealth_caurusel_offer( $atts, $content ){
	$image = '';
	extract(shortcode_atts(array(
	 	'image' => $image,
		'title' => '',
		'link' => '',
	), $atts));
	
	$img = wp_get_attachment_image_src( $image, 'full' );		
	$img_output = $img[0];
	
	$GLOBALS['offers'][] = array( 'image' => $img_output, 'title' => $title, 'link' => $link, 'content' => $content );
	
}
add_shortcode( 'caurusel_offer', 'pixhealth_caurusel_offer' );
/************************************************/

/*************** Carousel Testimonials ****************/
function pixhealth_caurusel_testimonials( $atts, $content = null ) {
    $output = '';
	extract(shortcode_atts(array(
		'max_slides' => '',
		'min_slides' => '',
		'width_slides' => '',
		'margin_slides' => '',
		'auto_slides' => '',
		'move_slides' => '',
		'infinite_slides' => '',
		'disable_carousel' => '',
	), $atts));
	
	$GLOBALS['testimonials'] = array();	
	do_shortcode($content);	 	
	$GLOBALS['testimonial_count'] = 0;
	
	if( is_array( $GLOBALS['testimonials'] ) ){
		$count = 1;
		foreach( $GLOBALS['testimonials'] as $item ){
			$class = $count == 1 ? 'class="active"' : '';
			
			$rat5 = $item['rating'] == 5 ? ' gold' : '-o';
			$rat4 = $item['rating'] >= 4 ? ' gold' : '-o';
			$rat3 = $item['rating'] >= 3 ? ' gold' : '-o';
			$rat2 = $item['rating'] >= 2 ? ' gold' : '-o';
			$rat1 = $item['rating'] >= 1 ? ' gold' : '-o';
			
			$out = '
			<li>
				<div class="testi-box">
				  <div class="person-text"> <i class="icomoon-quote-left"></i>
					<div class="testi-title">
					  <h4>'.wp_kses_post($item['title']).'</h4>
					
					</div>
					'.do_shortcode($item['content']).'
				  </div>
				  <div class="person-info">
					<div class="person-avatar"> <img src="'.esc_attr($item['image']).'" width="50" height="55" alt="'.esc_attr($item['name']).'"/> </div>
					<div class="person-name">
					  <h5>'.wp_kses_post($item['name']).'</h5>
					  <p>'.wp_kses_post($item['info']).'</p>
					</div>
					  <div class="product-rating"> 
					  	<i class="fa fa-star'.esc_attr($rat1).'"></i> 
						<i class="fa fa-star'.esc_attr($rat2).'"></i> 
						<i class="fa fa-star'.esc_attr($rat3).'"></i> 
						<i class="fa fa-star'.esc_attr($rat4).'"></i> 
						<i class="fa fa-star'.esc_attr($rat5).'"></i>
					  </div>
				  </div>
				</div>
			</li>
			';
			
			$testimonials[] = $out;
			 
			$count ++;
		}
        
		$max_slides = $max_slides == '' ? 2 : $max_slides;
		$min_slides = $min_slides == '' ? 1 : $min_slides;
		$width_slides = $width_slides == '' ? 560 : $width_slides;
		$margin_slides = $margin_slides == '' ? 10 : $margin_slides;
		$auto_slides = $auto_slides != '0' ? 'true' : 'false';
		$move_slides = $move_slides == ''? 1 : $move_slides;
		$infinite_slides = $infinite_slides != '1' ? 'false' : 'true';
		$disable_carousel = $disable_carousel != '0' ? 'bxslider' : 'carousel-disable';
		       
		$output = ' 
				<div class="carousel-testimonials"> 
					<ul class="carousel-4 '.esc_attr($disable_carousel).'" 
						data-max-slides="'.esc_attr($max_slides).'" 
						 data-min-slides="'.esc_attr($min_slides).'" 
						data-width-slides="'.esc_attr($width_slides).'" 
						data-margin-slides="'.esc_attr($margin_slides).'" 
							data-auto-slides="'.esc_attr($auto_slides).'" 
						data-move-slides="'.esc_attr($move_slides).'"   
						data-infinite-slides="'.esc_attr($infinite_slides).'" >
						'.implode( "\n", $testimonials ).'
					</ul>
				</div>';
					
	}
	return $output;
	
   
}
add_shortcode('caurusel_testimonials', 'pixhealth_caurusel_testimonials');

///////////////////////////

function pixhealth_caurusel_testimonial( $atts, $content ){
	$image = '';
	extract(shortcode_atts(array(
		'name' => '',
	 	'image' => $image,
		'info' => '',
		'title' => '',
		'rating' => '',
	), $atts));
	
	$img = wp_get_attachment_image_src( $image, 'preview-thumb' );		
	$img_output = $img[0];
	
	$x = $GLOBALS['testimonial_count'];
	$GLOBALS['testimonials'][$x] = array( 'name' => $name, 'image' => $img_output, 'info' => $info, 'title' => $title, 'rating' => $rating, 'content' => $content );
	
	$GLOBALS['testimonial_count']++;
}
add_shortcode( 'caurusel_testimonial', 'pixhealth_caurusel_testimonial' );
/************************************************/

/*************** Item Options ****************/
function pixhealth_itemoptions( $atts, $content = null ) {
    $output = $image = '';
	extract(shortcode_atts(array(
		'image' => $image,
	), $atts));
	
	$img = wp_get_attachment_image_src( $image, 'large' );
	$img_output = $img[0];
	
	$GLOBALS['itemoptions'] = array();	
	do_shortcode($content);	
	
	if( is_array( $GLOBALS['itemoptions'] ) ){
		foreach( $GLOBALS['itemoptions'] as $item ){
			
			$dots = $item['usedots'] == true ? '<span class="option-dots"></span>' : '';
			$clear = $item['itempos'] == "Left" ? 'clear' : '';
			$align = $item['itempos'] == "Left" ? 'right' : 'left';
			
			
			$out = '
			<li class="pull-'.esc_attr(strtolower($item['itempos'])).' col-lg-4 col-md-4 col-sm-4 col-xs-12 '.esc_attr($clear).'">
				<div class="options-item wow fadeIn'.esc_attr($item['itempos']).'" data-wow-delay="0.3s">
					<div class="circle smallCircle">
						<span><i class="fa '.esc_attr($item['icon']).' customColor"></i></span>
					</div>
					<h4 class="options-name ralewaySemiBold text-uppercase text-'.esc_attr($align).' blackTxtColor">'.wp_kses_post($item['title']).'</h4>
					<span class="options-desc robotoLight text-'.esc_attr($align).'">'.do_shortcode($item['content']).'</span>
					'.wp_kses_post($dots).'
				</div>
			</li>
			';
			
			$itemoptions[] = $out;
		}
		       
		$output = '
				<div class="optionsBox col-lg-12 col-md-12 col-sm-12 col-xs-12 clearfix">
					<div class="centerPhone col-lg-4 col-md-4 col-sm-4 col-xs-12 text-center">
						<img class="wow bounceIn" src="'.esc_url($img_output).'" alt="Itemoptions" data-wow-delay="0.3s" >
					</div>
					<ul class="optionsList pageRow">
						'.implode( "\n", $itemoptions ).'
					</ul>
				</div>
				';
					
	}
	return $output;
	
   
}
add_shortcode('itemoptions', 'pixhealth_itemoptions');

///////////////////////////

function pixhealth_itemoption( $atts, $content ){
	
	extract(shortcode_atts(array(
		'title' => '',
		'icon' => '',
		'itempos' => '',
		'usedots' => '',
	), $atts));
		
	$GLOBALS['itemoptions'][] = array( 'title' => $title, 'icon' => $icon, 'itempos' => $itempos, 'usedots' => $usedots, 'content' => $content );
		
}
add_shortcode( 'itemoption', 'pixhealth_itemoption' );
/************************************************/

/*************** Our Team ****************/
function pixhealth_ourteam( $atts, $content = null ) {
    $output = '';
	
	extract(shortcode_atts(array(
	 	'max_slides' => '',
		'min_slides' => '',
		'width_slides' => '',
		'margin_slides' => '',
		'auto_slides' => '',
		'move_slides' => '',
		'infinite_slides' => '',
		'disable_carousel' => '',
		'btntext' => __( 'BOOK AN APPOINTMENT', 'PixHealth' ),
	), $atts));
	
	$GLOBALS['members'] = array();
	do_shortcode($content);	
	
	if( is_array( $GLOBALS['members'] ) ){
		$count = 1;
		foreach( $GLOBALS['members'] as $item ){
			
			$facebook = $item['facebook'] != '' ? '<li><a target="_blank" href="'.esc_url($item['facebook']).'"><i class="social_icons fa fa-facebook-square"></i></a></li>'."\n" : '';
			$twitter = $item['twitter'] != '' ? '<li><a target="_blank" href="'.esc_url($item['twitter']).'"><i class="social_icons fa fa-twitter-square"></i></a></li>'."\n" : '';
			$googleplus = $item['googleplus'] != '' ? '<li><a target="_blank" href="'.esc_url($item['googleplus']).'"><i class="social_icons fa fa-google-plus-square"></i></a></li>'."\n" : '';
			$linkedin = $item['linkedin'] != '' ? '<li class="li-last"><a target="_blank" href="'.esc_url($item['linkedin']).'"><i class="social_icons fa fa-linkedin-square"></i></a></li>'."\n" : ''; 
			
			$out = '
			<li class="slide">
				<img src="'.esc_url($item['image']).'" height="250" width="270" alt="'.esc_attr($item['name']).'">
				<span class="name">'.wp_kses_post($item['name']).'</span>
				<span class="category">'.wp_kses_post($item['position']).'</span>
				<a href="'.esc_url($item['link']).'" class="btn btn_small">'.wp_kses_post($btntext).'</a>
			';
			if($facebook || $twitter || $googleplus || $linkedin){
				$out .= '
				<ul class="social-links">
					'.wp_kses_post($facebook.$twitter.$googleplus.$linkedin).'
				</ul>';
			}
			$out .= '
            </li>			
			';
			
			$members[] = $out;
			 
			$count ++;
		}
        
		$max_slides = $max_slides == '' ? 4 : $max_slides;
		$min_slides = $min_slides == '' ? 1 : $min_slides;
		$width_slides = $width_slides == '' ? 270 : $width_slides;
		$margin_slides = $margin_slides == '' ? 30 : $margin_slides;
		$auto_slides = $auto_slides != '0' ? 'true' : 'false';
		$move_slides = $move_slides == ''? 1 : $move_slides;
		$infinite_slides = $infinite_slides != '1' ? 'true' : 'false';
		$disable_carousel = $disable_carousel != '0' ? 'bxslider' : 'carousel-disable';
		       
		$output = ' 
		<div class="slider_team">
			<ul class="'.esc_attr($disable_carousel).'" 
				data-max-slides="'.esc_attr($max_slides).'" 
				data-min-slides="'.esc_attr($min_slides).'" 
				data-width-slides="'.esc_attr($width_slides).'" 
				data-margin-slides="'.esc_attr($margin_slides).'" 
				data-auto-slides="'.esc_attr($auto_slides).'" 
				data-move-slides="'.esc_attr($move_slides).'"   
				data-infinite-slides="'.esc_attr($infinite_slides).'"
				data-pager="true" 
				data-controls="false" >
				'.implode( "\n", $members ).'
			</ul>
		</div>				
		';
					
	}
	return $output;	
   
}
add_shortcode('ourteam', 'pixhealth_ourteam');

///////////////////////////

function pixhealth_teammember( $atts, $content ){
	$image = '';
	extract(shortcode_atts(array(
	 	'image' => $image,
		'name' => '',
		'position' => '',
		'link' => '',
		'facebook' => '',
		'twitter' => '',
		'googleplus' => '',
		'linkedin' => '',
	), $atts));
	
	$img = wp_get_attachment_image_src( $image, 'full' );		
	$img_output = $img[0];
	
	$GLOBALS['members'][] = array( 'image' => $img_output, 'name' => $name,  'position' => $position, 'link' => $link,  'facebook' => $facebook, 'twitter' => $twitter,  'googleplus' => $googleplus, 'linkedin' => $linkedin );
	
}
add_shortcode( 'teammember', 'pixhealth_teammember' );
/************************************************/


/*************** Social Buttons ****************/
function pixhealth_social_buts( $atts, $content = null ) {
    $output = '';
	
	$GLOBALS['socials'] = array();
	do_shortcode($content);	 	
	$GLOBALS['social_count'] = 0;
	
	if( is_array( $GLOBALS['socials'] ) ){
		$count = 1;
		foreach( $GLOBALS['socials'] as $item ){
			$class = $count == 1 ? 'class="active"' : '';
			
			$out = '
			<li >
				<a  style="border-color:'.esc_attr($item['color']).'" href="'.esc_url($item['link']).'" target="_blank" title="'.wp_kses_post($item['title']).'">
					<i style="color:'.esc_attr($item['color']).'" class="fa '.esc_attr($item['icon']).'"></i>
				</a>
			</li>
			';
			
			$socials[] = $out;
			 
			$count ++;
		}
		       
		$output = ' 
	<div class="social-box">
                <ul class="social-links">
						'.implode( "\n", $socials ).'
						</ul>
					</div>
				';
					
	}
	return $output;	
   
}
add_shortcode('socialbuts', 'pixhealth_social_buts');

///////////////////////////

function pixhealth_social_but( $atts, $content ){
	$image = '';
	extract(shortcode_atts(array(
		'title' => '',
		'icon' => '',
		'link' => '',
		'color' => '',
	), $atts));
		
	$x = $GLOBALS['social_count'];
	$GLOBALS['socials'][$x] = array( 'title' => $title, 'icon' => $icon, 'link' => $link, 'color' => $color );
	
	$GLOBALS['social_count']++;
}
add_shortcode( 'socialbut', 'pixhealth_social_but' );
/************************************************/

/*************** Price Table ****************/
function pixhealth_price_table( $atts, $content = null ) {
    $output = '';
	/*
	extract(shortcode_atts(array(
		'monthtext' => '',
		'yeartext' => '',
		'monthshort' => '',
		'yearshort' => '',
		'currency' => '',
	), $atts));
	*/
	$GLOBALS['pricecols'] = array();
	do_shortcode($content);
	
	if( is_array( $GLOBALS['pricecols'] ) ){
		$count = 1;
		foreach( $GLOBALS['pricecols'] as $item ){
			$class = $item['ispopular'] == true ? 'best' : '';
			$color = $item['ispopular'] == true ? 'whiteTxtColor' : 'robotoLight';
			$colorTxt = $item['ispopular'] == true ? 'white' : 'black';
			
			$out = '
			
			<li class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
				<div class="package-item center-block '.esc_attr($class).'">
					<div class="package-header ralewaySemiBold text-center '.esc_attr($colorTxt).'TxtColor">
						'.wp_kses_post($item['title']).'
						<div class="circle middleCircle center-block">
							<span class="'.esc_attr($color).'">
								<b class="robotoCondensedBold">'.wp_kses_post($item['price']).'</b>
								'.wp_kses_post($item['pricetime']).'
							</span>
						</div>
					</div>
					<div class="package-body text-center">
						'.do_shortcode($item['content']).'
						<a href="'.esc_url($item['link']).'" class="ellipseLink middleLink center-block robotoMedium text-uppercase '.esc_attr($colorTxt).'TxtColor text-center hvr-pop">'.wp_kses_post($item['btntext']).'</a>
					</div>
				</div>
			</li>
			
			';
			
			$pricecols[] = $out;
			 
			$count ++;
		}
			       
		$output = ' 
		
		<ul class="package-list">
			'.implode( "\n", $pricecols ).'			
		</ul>
		
		';
					
	}
	return $output;	
   
}
add_shortcode('pricetable', 'pixhealth_price_table');

///////////////////////////

function pixhealth_price_col( $atts, $content ){
	$image = '';
	extract(shortcode_atts(array(
		'title' => '',
		'price' => '',
		'pricetime' => '',
		'ispopular' => '',
		'btntext' => '',
		'link' => '',
	), $atts));
		
	$GLOBALS['pricecols'][] = array( 'title' => $title, 'price' => $price, 'pricetime' => $pricetime, 'ispopular' => $ispopular, 'btntext' => $btntext, 'link' => $link, 'content' => $content );
	
}
add_shortcode( 'pricecol', 'pixhealth_price_col' );
/************************************************/

/**************************VC FEATURED BLOCK****************/
function pixhealth_fblock($atts, $content=NULL){
    extract(shortcode_atts(array(
		'icon'=>'', 
        'link'=>''
    ), $atts));
    
   
    $out='<div class="offers"><figure>';
    $out.='<a href="'.esc_url($link).'"><img src="'.esc_url($icon).'" alt="" /></a>';
    $out.='<div class="overlay">'.do_shortcode($content).'</div>';
    $out.='</figure></div>';
    return $out;
}
add_shortcode('fblock', 'pixhealth_fblock');


/*********************************************************/


/*************** VC TITLE BLOCK 2 ***************************/
function pixhealth_tblock2($atts, $content=NULL){
    extract(shortcode_atts(array(
        'title'=>'',

		'before'=>'',
		'css_animation' => '',
    ), $atts));
	
	//$css_class = getCSSAnimation($css_animation);
	
	$fullcontent = ($content == "") ? "" : '<div class="text-center after-title-info">'.do_shortcode($content).'  </div>';
    $out='
	
	
	
	<div class="col-lg-12 text-center">
        <div class="heading-wrap  heading-wrap-style-2   '.esc_attr($css_animation).' " >
          <div class="small-logo">'.wp_kses_post($before).'</div>
          <h2 class="section-heading">'.wp_kses_post($title).'</h2>
     '.wp_kses_post($fullcontent).'
        </div>
      </div>
	  
	 
	   <hr class="double-line">  
	
	'; 
    
    return $out;
}

add_shortcode('tblock2', 'pixhealth_tblock2');

/******************************************************/






/*************** VC  SUBTITLE BLOCK***************************/
function pixhealth_subtblock($atts, $content=NULL){
    extract(shortcode_atts(array(
        'title'=>'',
		'css_animation' => '',
    ), $atts));
	
	//$css_class = getCSSAnimation($css_animation);
	
	$fullcontent = ($content == "") ? "" : '<div class="title-content"><div class="info-box">'.do_shortcode($content).' </div> </div>';
    $out='
	
	<h4 class="sub-title primary-color text-center '.esc_attr($css_animation).' ">'.wp_kses_post($title).'</h4>

		'.wp_kses_post($fullcontent); 
    
    return $out;
}

add_shortcode('subtblock', 'pixhealth_subtblock');

/******************************************************/




/***********VC HEXAGON *************/

function pixhealth_hexagon($atts, $content=NULL){
    extract(shortcode_atts(array(
        'title'=>'',
		'link'=>'',
		'icon'=>'',
    ), $atts));
	
	$fullcontent = ($content == "") ? "" : '<div class="hb-footer-content">'.do_shortcode($content).'  </div>';
    $out='
	 
	
		<div class="hb-wrap hb-icon-box"> <a href="'.esc_url($link).'"><span class="hb hb-md"></span> <span class="hb2 hb hb-sm"> <span class="hb-content"> <span class="hb-icon"><i class="fa '.esc_attr($icon).'"></i></span> <span class="hb-title"> '.wp_kses_post($title).' </span> </span> </span> </a></div>

		'.wp_kses_post($fullcontent); 
    
    return $out;
}

add_shortcode('hexagon', 'pixhealth_hexagon');

/******************************************************/





/***********VC STATE *************/

function pixhealth_state($atts, $content=NULL){
    extract(shortcode_atts(array(
        'title'=>'',
		'amount'=>'',
		'icon'=>'',
    ), $atts));
	
	$fullcontent = ($content == "") ? "" : '<div class="hb-footer-content">'.do_shortcode($content).'  </div>';
    $out='
	
		<div  class="featured-item-simple-icon  text-center ">
              <div class="ft-icons-simple"><i class="fa '.esc_attr($icon).' "></i> </div>
              <span data-percent="'.esc_attr($amount).' " class="chart"> <span class="percent">'.wp_kses_post($amount).' </span> <canvas height="0" width="0"></canvas></span>
              <h6>'.wp_kses_post($title).' </h6>
            </div>

		'.wp_kses_post($fullcontent); 
    
    return $out;
}

add_shortcode('state', 'pixhealth_state');

/******************************************************


/***************BRAND BLOCK***************************/
function pixhealth_brandblock($atts, $content=NULL){
	
		$out = $image = '';
		
		
    extract(shortcode_atts(array(
        'url'=>'',
		'image'=>'',
    ), $atts));
	
	
	
		$img = wp_get_attachment_image_src( $image, 'large' );
		
		
		$img_output = $img[0];
    
    $out='<div class="logo-box"> <a href="'.esc_url($url).'"><img src="'.esc_url($img_output).'"  alt="logo"></a></div>';
    
    return $out;
}

add_shortcode('brandblock', 'pixhealth_brandblock');






/******************************************************/




/******************* COLUMNS ********************/

function PixHealth_one_whole( $atts, $content = null ) {
   return '<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 ">' . do_shortcode($content) . '</div>';
}
add_shortcode('one_whole', 'PixHealth_one_whole');

function PixHealth_one_half( $atts, $content = null ) {
   return '<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">' . do_shortcode($content) . '</div>';
}
add_shortcode('one_half', 'PixHealth_one_half');

function PixHealth_one_third( $atts, $content = null ) {
   return '<div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">' . do_shortcode($content) . '</div>';
}
add_shortcode('one_third', 'PixHealth_one_third');

function PixHealth_two_third( $atts, $content = null ) {
   return '<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">' . do_shortcode($content) . '</div>';
}
add_shortcode('two_third', 'PixHealth_two_third');

function PixHealth_one_fourth( $atts, $content = null ) {
   return '<div class="col-xs-12 col-sm-12 col-md-6 col-lg-3">' . do_shortcode($content) . '</div>';
}
add_shortcode('one_fourth', 'PixHealth_one_fourth');

function PixHealth_three_fourth( $atts, $content = null ) {
   return '<div class="col-xs-9 col-sm-9 col-md-9 col-lg-9">' . do_shortcode($content) . '</div>';
}
add_shortcode('three_fourth', 'PixHealth_three_fourth');

function PixHealth_one_sixth( $atts, $content = null ) {
   return '<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">' . do_shortcode($content) . '</div>';
}
add_shortcode('one_sixth', 'PixHealth_one_sixth');

function PixHealth_five_twelveth( $atts, $content = null ) {
   return '<div class="col-xs-5 col-sm-5 col-md-5 col-lg-5">' . do_shortcode($content) . '</div>';
}
add_shortcode('five_twelveth', 'PixHealth_five_twelveth');

function PixHealth_seven_twelveth( $atts, $content = null ) {
   return '<div class="col-xs-7 col-sm-7 col-md-7 col-lg-7">' . do_shortcode($content) . '</div>';
}
add_shortcode('seven_twelveth', 'PixHealth_seven_twelveth');


function PixHealth_one_twelveth( $atts, $content = null ) {
   return '<div class="col-xs-1 col-sm-1 col-md-1 col-lg-1">' . do_shortcode($content) . '</div>';
}
add_shortcode('one_twelveth', 'PixHealth_one_twelveth');

function PixHealth_eleven_twelveth( $atts, $content = null ) {
   return '<div class="col-xs-11 col-sm-11 col-md-11 col-lg-11">' . do_shortcode($content) . '</div>';
}
add_shortcode('eleven_twelveth', 'PixHealth_eleven_twelveth');

function PixHealth_five_sixth( $atts, $content = null ) {
   return '<div class="col-xs-10 col-sm-10 col-md-10 col-lg-10">' . do_shortcode($content) . '</div>';
}
add_shortcode('five_sixth', 'PixHealth_five_sixth');

function PixHealth_row( $atts, $content = null ) {
   return '<div class="row">' . do_shortcode($content) . '</div>';
}
add_shortcode('row', 'PixHealth_row');

function PixHealth_div_carousel( $atts, $content = null ) {
   return '<div class="width-carousel">' . do_shortcode($content) . '</div>';
}
add_shortcode('div_carousel', 'PixHealth_div_carousel');


/************************************************/



/***************** CLEAR ************************/

function alc_clear($atts, $content = null) {	
	return '<div class="clear"></div>';
}
add_shortcode('clear', 'alc_clear');


/******** SHORTCODE SUPPORT FOR WIDGETS *********/

if (function_exists ('shortcode_unautop')) {
	add_filter ('widget_text', 'shortcode_unautop');
}
add_filter ('widget_text', 'do_shortcode');

/************************************************/

function pixhealth_share_buttons($atts, $content=NULL){
   
	global $post;
	if(!isset($post->ID)) return;
	$permalink = get_permalink($post->ID);	
	$image =  wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'preview-thumb' );
	
	$post_title = rawurlencode(get_the_title($post->ID)); 
	
    $out='	
			<div class="social-links__inner pull-right">
				<span class="social-links__name">SHARE THIS ARTICLE</span>            
				<a href="http://www.facebook.com/sharer.php?u='.esc_url($permalink).'&amp;images='.esc_url($image[0]).'" title="'.__('Facebook', 'PixHealth').'" target="_blank" class="text-center"><i class="icon fa fa-facebook"></i></a>
				<a href="https://twitter.com/share?url='.esc_url($permalink).'&text='.esc_attr($post_title).'" title="'.__('Twitter', 'PixHealth').'" target="_blank" class="text-center"><i class="icon fa fa-twitter"></i></a>
				<a href="http://plus.google.com/share?url='.esc_url($permalink).'&title='.esc_attr($post_title).'" title="'.__('Google +', 'PixHealth').'" target="_blank" class="text-center"><i class="icon fa fa-google-plus"></i></a>
				<a href="http://pinterest.com/pin/create/button/?url='.esc_url($permalink).'&amp;media='.esc_url($image[0]).'&amp;description='.esc_attr($post_title).'" title="'.__('Pinterest', 'PixHealth').'" target="_blank" class="text-center"><i class="icon fa fa-pinterest-p"></i></a>
			</div>
		'; 
    
    return $out;
}

add_shortcode('share', 'pixhealth_share_buttons');


?>