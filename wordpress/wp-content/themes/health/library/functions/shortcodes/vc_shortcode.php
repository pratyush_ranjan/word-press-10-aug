<?php

/**
 * Force Visual Composer to initialize as "built into the theme". This will hide certain tabs under the Settings->Visual Composer page
 */
 
add_action( 'init', 'pixhealth_integrateWithVC', 200 );
function pixhealth_integrateWithVC() {

	$args = array( 'taxonomy' => 'portfolio_category', 'hide_empty' => '0');
	$categories = get_categories($args);
	$cats = array();
	if(empty($categories['errors'])){
		foreach($categories as $category){
			$cats[$category->name] = $category->term_id;
		}
	}
	
	$args = array( 'taxonomy' => 'product_cat', 'hide_empty' => '0');
	$categories_woo = get_categories($args);
	$cats_woo = array();
	if(empty($categories_woo['errors'])){
		foreach($categories_woo as $category){
			$cats_woo[$category->name] = $category->term_id;
		}
	}
	
	$args = array( 'taxonomy' => 'services_category', 'hide_empty' => '0');
	$categories_serv = get_categories($args);
	$cats_serv = array();
	if(empty($categories_serv['errors'])){
		foreach($categories_serv as $category){
			$cats_serv[$category->name] = $category->term_id;
		}
	}
	
	$args = array( 'posts_per_page' => 0, 'post_type' => 'services');
	$services = get_posts($args);
	$serv = array();
	if(empty($services['errors'])){
		foreach($services as $service){		
			$serv[$service->post_title] = $service->ID;
		}
	}
	
	$args = array( 'posts_per_page' => 0, 'post_type' => 'portfolio');
	$portfolio = get_posts($args);
	$port = array();
	if(empty($portfolio['errors'])){
		foreach($portfolio as $port_card){		
			$port[$port_card->post_title] = $port_card->ID;
		}
	}
	
	$args = array( 'post_type' => 'wpcf7_contact_form');
	$forms = get_posts($args);
	$cform7 = array();
	if(empty($forms['errors'])){
		foreach($forms as $form){		
			$cform7[$form->post_title] = $form->ID;
		}
	}
	
	$add_css_animation = array(
		'type' => 'dropdown',
		'heading' => __( 'CSS Animation', 'PixHealth' ),
		'param_name' => 'css_animation',
		'admin_label' => true,
		'value' => array(
			__( 'No', 'PixHealth' ) => '',
			__( 'bounce', 'PixHealth' ) => 'bounce',
			__( 'flash', 'PixHealth' ) => 'flash',
			__( 'pulse', 'PixHealth' ) => 'pulse',
			__( 'rubberBand', 'PixHealth' ) => 'rubberBand',
			__( 'shake', 'PixHealth' ) => 'shake',
			__( 'swing', 'PixHealth' ) => 'swing',
			__( 'tada', 'PixHealth' ) => 'tada',
			__( 'wobble', 'PixHealth' ) => 'wobble',
			__( 'jello', 'PixHealth' ) => 'jello',
			
			__( 'bounceIn', 'PixHealth' ) => 'bounceIn',
			__( 'bounceInDown', 'PixHealth' ) => 'bounceInDown',
			__( 'bounceInLeft', 'PixHealth' ) => 'bounceInLeft',
			__( 'bounceInRight', 'PixHealth' ) => 'bounceInRight',
			__( 'bounceInUp', 'PixHealth' ) => 'bounceInUp',
			__( 'bounceOut', 'PixHealth' ) => 'bounceOut',
			__( 'bounceOutDown', 'PixHealth' ) => 'bounceOutDown',
			__( 'bounceOutLeft', 'PixHealth' ) => 'bounceOutLeft',
			__( 'bounceOutRight', 'PixHealth' ) => 'bounceOutRight',
			__( 'bounceOutUp', 'PixHealth' ) => 'bounceOutUp',
			
			__( 'fadeIn', 'PixHealth' ) => 'fadeIn',
			__( 'fadeInDown', 'PixHealth' ) => 'fadeInDown',
			__( 'fadeInDownBig', 'PixHealth' ) => 'fadeInDownBig',
			__( 'fadeInLeft', 'PixHealth' ) => 'fadeInLeft',
			__( 'fadeInLeftBig', 'PixHealth' ) => 'fadeInLeftBig',
			__( 'fadeInRight', 'PixHealth' ) => 'fadeInRight',
			__( 'fadeInRightBig', 'PixHealth' ) => 'fadeInRightBig',
			__( 'fadeInUp', 'PixHealth' ) => 'fadeInUp',
			__( 'fadeInUpBig', 'PixHealth' ) => 'fadeInUpBig',			
			__( 'fadeOut', 'PixHealth' ) => 'fadeOut',
			__( 'fadeOutDown', 'PixHealth' ) => 'fadeOutDown',
			__( 'fadeOutDownBig', 'PixHealth' ) => 'fadeOutDownBig',
			__( 'fadeOutLeft', 'PixHealth' ) => 'fadeOutLeft',
			__( 'fadeOutLeftBig', 'PixHealth' ) => 'fadeOutLeftBig',
			__( 'fadeOutRight', 'PixHealth' ) => 'fadeOutRight',
			__( 'fadeOutRightBig', 'PixHealth' ) => 'fadeOutRightBig',
			__( 'fadeOutUp', 'PixHealth' ) => 'fadeOutUp',
			__( 'fadeOutUpBig', 'PixHealth' ) => 'fadeOutUpBig',
			
			__( 'flip', 'PixHealth' ) => 'flip',
			__( 'flipInX', 'PixHealth' ) => 'flipInX',
			__( 'flipInY', 'PixHealth' ) => 'flipInY',
			__( 'flipOutX', 'PixHealth' ) => 'flipOutX',
			__( 'flipOutY', 'PixHealth' ) => 'flipOutY',
			
			__( 'lightSpeedIn', 'PixHealth' ) => 'lightSpeedIn',
			__( 'lightSpeedOut', 'PixHealth' ) => 'lightSpeedOut',
			
			__( 'rotateIn', 'PixHealth' ) => 'rotateIn',
			__( 'rotateInDownLeft', 'PixHealth' ) => 'rotateInDownLeft',
			__( 'rotateInDownRight', 'PixHealth' ) => 'rotateInDownRight',
			__( 'rotateInUpLeft', 'PixHealth' ) => 'rotateInUpLeft',
			__( 'rotateInUpRight', 'PixHealth' ) => 'rotateInUpRight',			
			__( 'rotateOut', 'PixHealth' ) => 'rotateOut',
			__( 'rotateOutDownLeft', 'PixHealth' ) => 'rotateOutDownLeft',
			__( 'rotateOutDownRight', 'PixHealth' ) => 'rotateOutDownRight',
			__( 'rotateOutUpLeft', 'PixHealth' ) => 'rotateOutUpLeft',
			__( 'rotateOutUpRight', 'PixHealth' ) => 'rotateOutUpRight',
			
			__( 'slideInUp', 'PixHealth' ) => 'slideInUp',
			__( 'slideInDown', 'PixHealth' ) => 'slideInDown',
			__( 'slideInLeft', 'PixHealth' ) => 'slideInLeft',
			__( 'slideInRight', 'PixHealth' ) => 'slideInRight',
			__( 'slideOutUp', 'PixHealth' ) => 'slideOutUp',			
			__( 'slideOutDown', 'PixHealth' ) => 'slideOutDown',
			__( 'slideOutLeft', 'PixHealth' ) => 'slideOutLeft',
			__( 'slideOutRight', 'PixHealth' ) => 'slideOutRight',
			
			__( 'zoomIn', 'PixHealth' ) => 'zoomIn',
			__( 'zoomInDown', 'PixHealth' ) => 'zoomInDown',
			__( 'zoomInLeft', 'PixHealth' ) => 'zoomInLeft',
			__( 'zoomInRight', 'PixHealth' ) => 'zoomInRight',
			__( 'zoomInUp', 'PixHealth' ) => 'zoomInUp',			
			__( 'zoomOut', 'PixHealth' ) => 'zoomOut',
			__( 'zoomOutDown', 'PixHealth' ) => 'zoomOutDown',
			__( 'zoomOutLeft', 'PixHealth' ) => 'zoomOutLeft',
			__( 'zoomOutRight', 'PixHealth' ) => 'zoomOutRight',
			__( 'zoomOutUp', 'PixHealth' ) => 'zoomOutUp',
			
			__( 'hinge', 'PixHealth' ) => 'hinge',			
			__( 'rollIn', 'PixHealth' ) => 'rollIn',
			__( 'rollOut', 'PixHealth' ) => 'rollOut',
			
		),
		'description' => __( 'Select type of animation if you want this element to be animated when it enters into the browsers viewport. Note: Works only in modern browsers.', 'PixHealth' )
	);
	
$pix_libs = $pix_fonts = $pix_fonts_str = $params = $params1 = $params2 = array();

if ( function_exists( 'fil_init' ) ) {

	if ( array_key_exists( 'vc_iconpicker-type-pixflaticon', $GLOBALS['wp_filter'] ) ) {
		$pix_libs[ __( 'Flaticon', 'PixTheme' ) ] = 'pixflaticon';
	}
	if ( array_key_exists( 'vc_iconpicker-type-pixfontawesome', $GLOBALS['wp_filter'] ) ) {
		$pix_libs[ __( 'Font Awesome', 'PixTheme' ) ] = 'pixfontawesome';
	}
	if ( array_key_exists( 'vc_iconpicker-type-pixelegant', $GLOBALS['wp_filter'] ) ) {
		$pix_libs[ __( 'Elegant', 'PixTheme' ) ] = 'pixelegant';
	}
	if ( array_key_exists( 'vc_iconpicker-type-pixicomoon', $GLOBALS['wp_filter'] ) ) {
		$pix_libs[ __( 'Icomoon', 'PixTheme' ) ] = 'pixicomoon';
	}
	if ( array_key_exists( 'vc_iconpicker-type-pixsimple', $GLOBALS['wp_filter'] ) ) {
		$pix_libs[ __( 'Simple', 'PixTheme' ) ] = 'pixsimple';
	}
	if ( array_key_exists( 'vc_iconpicker-type-pixcustom1', $GLOBALS['wp_filter'] ) ) {
		$pix_libs[ __( 'Custom 1', 'PixTheme' ) ] = 'pixcustom1';
	}
	if ( array_key_exists( 'vc_iconpicker-type-pixcustom2', $GLOBALS['wp_filter'] ) ) {
		$pix_libs[ __( 'Custom 2', 'PixTheme' ) ] = 'pixcustom2';
	}
	if ( array_key_exists( 'vc_iconpicker-type-pixcustom3', $GLOBALS['wp_filter'] ) ) {
		$pix_libs[ __( 'Custom 3', 'PixTheme' ) ] = 'pixcustom3';
	}
	if ( array_key_exists( 'vc_iconpicker-type-pixcustom4', $GLOBALS['wp_filter'] ) ) {
		$pix_libs[ __( 'Custom 4', 'PixTheme' ) ] = 'pixcustom4';
	}
	if ( array_key_exists( 'vc_iconpicker-type-pixcustom5', $GLOBALS['wp_filter'] ) ) {
		$pix_libs[ __( 'Custom 5', 'PixTheme' ) ] = 'pixcustom5';
	}
	if ( array_key_exists( 'vc_iconpicker-type-fontawesome', $GLOBALS['wp_filter'] ) ) {
		$pix_libs[ __( 'VC Font Awesome', 'PixTheme' ) ] = 'fontawesome';
	}
	if ( array_key_exists( 'vc_iconpicker-type-openiconic', $GLOBALS['wp_filter'] ) ) {
		$pix_libs[ __( 'VC Open Iconic', 'PixTheme' ) ] = 'openiconic';
	}
	if ( array_key_exists( 'vc_iconpicker-type-typicons', $GLOBALS['wp_filter'] ) ) {
		$pix_libs[ __( 'VC Typicons', 'PixTheme' ) ] = 'typicons';
	}
	if ( array_key_exists( 'vc_iconpicker-type-entypo', $GLOBALS['wp_filter'] ) ) {
		$pix_libs[ __( 'VC Entypo', 'PixTheme' ) ] = 'entypo';
	}
	if ( array_key_exists( 'vc_iconpicker-type-linecons', $GLOBALS['wp_filter'] ) ) {
		$pix_libs[ __( 'VC Linecons', 'PixTheme' ) ] = 'linecons';
	}

	$add_icon_libs = array(
			'type'        => 'dropdown',
			'heading'     => __( 'Icon library', 'PixTheme' ),
			'param_name'  => 'type',
			'value'       => $pix_libs,
			'admin_label' => true,
			'description' => __( 'Select icon library.', 'PixTheme' ),
	);

	if ( is_array( $pix_libs ) ) {
		$pix_fonts_str[] = $add_icon_libs;

		foreach ( $pix_libs as $val ) {
			if ( $val != '' ) {
				$pix_fonts[ $val ] = array(
						'type'        => 'iconpicker',
						'heading'     => __( 'Icon', 'PixTheme' ),
						'param_name'  => 'icon_' . $val,
						'value'       => '',
						'settings'    => array(
								'emptyIcon'    => true,
								'type'         => $val,
								'iconsPerPage' => 4000,
						),
						'dependency'  => array(
								'element' => 'type',
								'value'   => $val,
						),
						'description' => __( 'Select icon from library.', 'PixTheme' ),
				);
			}

			$pix_fonts_str[] = $pix_fonts[ $val ];
		}
	}

}
	
	$attributes1 = array(
		array(
			'type' => 'dropdown',
			'heading' => "Use Section Anchor",
			'param_name' => 'panchor',
			'value' => array("No" , "Yes"),
			'description' => __( "Need Row ID. ", "PixShortcode" )
		),
	);
		
	$attributes2 = array(
		array(
			"type" => "dropdown",
			"heading" => __( "Padding", "PixHealth" ),
			"param_name" => "ppadding",
			'value' => array(
				__( "Default", "PixHealth" ) => '',
				__( "Top", "PixHealth" ) => 'vc_row-padding-top',
				__( "Bottom", "PixHealth" ) => 'vc_row-padding-bottom',
				__( "Both", "PixHealth" ) => 'vc_row-padding-both',
			),
			"description" => __( "Top, bottom, both", "PixHealth" ),
			'group' => __( 'Row Options', 'PixShortcode' ),
		),
		array(
			'type' => 'attach_images',
			'heading' => __( 'Background Slides', 'PixHealth' ),
			'param_name' => 'pbgslides',
			'description' => __( 'Background Slides.', 'PixHealth' ),
			'group' => __( 'Row Options', 'PixShortcode' ),
		),
		/*
		array(
			'type' => 'dropdown',
			'heading' => "Page Decor",
			'param_name' => 'pdecor',
			'value' => array("None" , "Top", "Bottom", "Both" ),
			'description' => __( "Page Decor Option", "PixHealth" ),
			'group' => __( 'Row Options', 'PixShortcode' ),
		),
		*/
		array(
			'type' => 'dropdown',
			'heading' => "Text Color",
			'param_name' => 'ptextcolor',
			'value' => array("Default" , "White" , "Black"),
			'description' => __( "Text Color", "PixHealth" ),
			'group' => __( 'Row Options', 'PixShortcode' ),
		),
		
		array(
			'type' => 'dropdown',
			'heading' => __( 'Row Overflow', 'PixHealth' ),
			'param_name' => 'bg_row_overflow',
			'value' => array(
				__( "Default", "PixHealth" ) => '',				
				__( "Overflow", "PixHealth" ) => 'pix-overflow',
				__( "Overflow-X", "PixHealth" ) => 'pix-overflow-x',
				__( "Overflow-Y", "PixHealth" ) => 'pix-overflow-y',
			),
			'description' => __( 'Select row overflow type.', 'PixHealth' ),
			'group' => __( 'Row Elements', 'PixHealth' ),
		),
		array(
			'type' => 'dropdown',
			'heading' => __( 'Elements Parallax', 'PixHealth' ),
			'param_name' => 'bg_pix_image_parallax',
			'value' => array(
				__( "No", "PixHealth" ) => '',				
				__( "Yes", "PixHealth" ) => 'pix-parallax-elements',
			),
			'description' => __( 'Use parallax for elements.', 'PixHealth' ),
			'group' => __( 'Row Elements', 'PixHealth' ),
		),
		array(
			'type' => 'attach_image',
			'heading' => __( 'Left Backgroung Image', 'PixHealth' ),
			'param_name' => 'bg_image_left',
			'value' => '',
			'description' => __( 'Select image from media library.', 'PixHealth' ),
			'group' => __( 'Row Elements', 'PixHealth' ),
		),
		array(
			'type' => 'dropdown',
			'heading' => "Vertical Position",
			'param_name' => 'bg_imgl_top_bottom',
			'value' => array(
				__( "Bottom", "PixHealth" ) => 'bottom',				
				__( "Top", "PixHealth" ) => 'top',				
			),
			'description' => __( "Select top or bottom position", "PixHealth" ),
			'dependency' => array(
				'element' => 'bg_image_left',
				'not_empty' => true,
			),
			'group' => __( 'Row Elements', 'PixHealth' ),
		),
		array(
			'type' => 'textfield',
			'holder' => 'div',
			'class' => '',
			'heading' => __( 'Vertical px', 'PixHealth' ),
			'param_name' => 'bg_imgl_vpos',
			'value' => '',
			'description' => '',
			'dependency' => array(
				'element' => 'bg_image_left',
				'not_empty' => true,
			),
			'group' => __( 'Row Elements', 'PixHealth' ),
		),
		array(
			'type' => 'dropdown',
			'heading' => "Horizontal Position",
			'param_name' => 'bg_imgl_left_right',
			'value' => array(				
				__( "Left", "PixHealth" ) => 'left',
				__( "Right", "PixHealth" ) => 'right',
			),
			'description' => __( "Select left or right position", "PixHealth" ),
			'dependency' => array(
				'element' => 'bg_image_left',
				'not_empty' => true,
			),
			'group' => __( 'Row Elements', 'PixHealth' ),
		),
		array(
			'type' => 'textfield',
			'holder' => 'div',
			'class' => '',
			'heading' => __( 'Horizontal px', 'PixHealth' ),
			'param_name' => 'bg_imgl_hpos',
			'value' => '',
			'description' => '',
			'dependency' => array(
				'element' => 'bg_image_left',
				'not_empty' => true,
			),
			'group' => __( 'Row Elements', 'PixHealth' ),
		),
		
		array(
			'type' => 'attach_image',
			'heading' => __( 'Right Backgroung Image', 'PixHealth' ),
			'param_name' => 'bg_image_right',
			'value' => '',
			'description' => __( 'Select image from media library.', 'PixHealth' ),
			'group' => __( 'Row Elements', 'PixHealth' ),
		),
		array(
			'type' => 'dropdown',
			'heading' => "Vertical Position",
			'param_name' => 'bg_imgr_top_bottom',
			'value' => array(		
				__( "Bottom", "PixHealth" ) => 'bottom',		
				__( "Top", "PixHealth" ) => 'top',				
			),
			'description' => __( "Select top or bottom position", "PixHealth" ),
			'dependency' => array(
				'element' => 'bg_image_right',
				'not_empty' => true,
			),
			'group' => __( 'Row Elements', 'PixHealth' ),
		),
		array(
			'type' => 'textfield',
			'holder' => 'div',
			'class' => '',
			'heading' => __( 'Vertical px', 'PixHealth' ),
			'param_name' => 'bg_imgr_vpos',
			'value' => '',
			'description' => '',
			'dependency' => array(
				'element' => 'bg_image_right',
				'not_empty' => true,
			),
			'group' => __( 'Row Elements', 'PixHealth' ),
		),
		array(
			'type' => 'dropdown',
			'heading' => "Horizontal Position",
			'param_name' => 'bg_imgr_left_right',
			'value' => array(	
				__( "Right", "PixHealth" ) => 'right',			
				__( "Left", "PixHealth" ) => 'left',				
			),
			'description' => __( "Select left or right position", "PixHealth" ),
			'dependency' => array(
				'element' => 'bg_image_right',
				'not_empty' => true,
			),
			'group' => __( 'Row Elements', 'PixHealth' ),
		),
		array(
			'type' => 'textfield',
			'holder' => 'div',
			'class' => '',
			'heading' => __( 'Horizontal px', 'PixHealth' ),
			'param_name' => 'bg_imgr_hpos',
			'value' => '',
			'description' => '',
			'dependency' => array(
				'element' => 'bg_image_right',
				'not_empty' => true,
			),
			'group' => __( 'Row Elements', 'PixHealth' ),
		),		
		
	);
	
	if(!function_exists('fil_init')){
		$attributes = array_merge($attributes1, $attributes2);
	}else{
		$attributes = array_merge($attributes1, $pix_fonts_str, $attributes2);
	}
	vc_add_params( 'vc_row', $attributes );
		

	
	vc_map(
		array(
			"name" => __( "Title", "PixHealth" ),
			"base" => "titleblock",
			"class" => "pix-theme-icon4",
			"category" => __( "Templines", "PixHealth"),
			"params" => array(
				array(
					"type" => "textfield",
					"holder" => "div",
					"class" => "",
					"heading" => __( "Title", "PixHealth" ),
					"param_name" => "title",
					"value" => '',
					"description" => __( "Simple title text.", "PixHealth" )
				),
				array(
					"type" => "textfield",
					"holder" => "div",
					"class" => "",
					"heading" => __( "Strong Title", "PixHealth" ),
					"param_name" => "title_strong",
					"value" => '',
					"description" => __( "Strong title text.", "PixHealth" )
				),
				array(
					"type" => "textfield",
					"holder" => "div",
					"class" => "",
					"heading" => __( "Color Title", "PixHealth" ),
					"param_name" => "title_color",
					"value" => '',
					"description" => __( "Color title text.", "PixHealth" )
				),
				array(
					"type" => "checkbox",
					"class" => "",
					"heading" => __( "Use Decor", "PixHealth" ),
					"param_name" => "usedecor", 
					"description" => __( "Use decor after icon.", "PixHealth" )
				),
				array(
					'type' => 'dropdown',
					'heading' => __( 'Title Align', 'PixHealth' ),
					'param_name' => 'align',
					'value' => array(
						__( "Default", "PixHealth" ) => 'title-middle',
						__( "Left", "PixHealth" ) => 'title-left',
						__( "Right", "PixHealth" ) => 'title-right',
					),
					'description' => '',
				),
				array(
					'type' => 'dropdown',
					'heading' => __( 'Text Color', 'PixHealth' ),
					'param_name' => 'color',
					'value' => array(
						__( "Black", "PixHealth" ) => '',
						__( "White", "PixHealth" ) => 'color_white',						
					),
					'description' => '',
				),	
				$add_css_animation,
				array(
					"type" => "textarea_html",
					"holder" => "div",
					"class" => "",
					"heading" => __( "Content", "PixHealth" ),
					"param_name" => "content", 
					"value" => __( "<p>I am test text block. Click edit button to change this text.</p>", "PixHealth" ),
					"description" => __( "Enter your content.", "PixHealth" )
				)
			)
		) 
	);	
	if ( class_exists( 'WPBakeryShortCode' ) ) {
		class WPBakeryShortCode_Titleblock extends WPBakeryShortCode {
			
		}
	}


	/// buttonblock
	$params1 = array(
                array(
					'type' => 'textfield',
					'heading' => __( 'Title', 'PixHealth' ),
					'param_name' => 'title',
					'value' => '',
					'description' => __( 'Button title', 'PixHealth' )
				),
				array(
					"type" => "textfield",
					"holder" => "div",
					"class" => "",
					"heading" => __( "Link", "PixHealth" ),
					"param_name" => "link",
					"value" => __( "", "PixHealth" ),
					"description" => ''
				),
				array(
					"type" => "textfield",
					"holder" => "div",
					"class" => "",
					"heading" => __( "Icon", "PixHealth" ),
					"param_name" => "icon",
					"value" => '',
					"description" => __( "Add icon <a href='//fortawesome.github.io/Font-Awesome/icons/' target='_blank'>See all icons</a>", "PixHealth" )
				),
            );
    $params2 = array(
                array(
					'type' => 'dropdown',
					'heading' => __( 'Color', 'PixHealth' ),
					'param_name' => 'color',
					'value' => array(
						__( "White", "PixHealth" ) => 'whiteBtn',
						__( "Color", "PixHealth" ) => 'colorBtn',
					),
					'description' => '',
				),
            );
    if(!function_exists('fil_init')){
        $params = array_merge($params1, $params2);
    }else{
        $params = array_merge($params1, $pix_fonts_str, $params2);
    }
	vc_map(
		array(
			"name" => __( "Simple Button", "PixHealth" ),
			"base" => "buttonblock",
			"class" => "pix-theme-icon",
			"category" => __( "Templines", "PixHealth"),
			"params" => $params,
		) 
	);
	if ( class_exists( 'WPBakeryShortCode' ) ) {
		class WPBakeryShortCode_Buttonblock extends WPBakeryShortCode {
			
		}
	}
	
	
	vc_map(
		array(
			"name" => __( "Banner Box", "PixHealth" ),
			"base" => "bannerbox",
			"class" => "pix-theme-icon2",
			"category" => __( "Templines", "PixHealth"),
			"params" => array(
				 array(
					"type" => "textfield",
					"holder" => "div",
					"class" => "",
					"heading" => __( "Title", "PixHealth" ),
					"param_name" => "title",
					"value" => __( "Are you ready to buy this template?", "PixHealth" ),
					"description" => __( "Banner Title", "PixHealth" )
				 ),
				 array(
					"type" => "textfield",
					"holder" => "div",
					"class" => "",
					"heading" => __( "Link", "PixHealth" ),
					"param_name" => "link",
					"value" => __( "https:/templines.com", "PixHealth" ),
					"description" => __( "Button link", "PixHealth" )
				 ),
				 array(
					"type" => "textfield",
					"holder" => "div",
					"class" => "",
					"heading" => __( "Button Text", "PixHealth" ),
					"param_name" => "btntext",
					"value" => __( 'PURCHASE NOW +', "PixHealth" ),
					"description" => __( "Button Title", "PixHealth" )
				 ),
				 $add_css_animation,
				 array(
					"type" => "textarea_html",
					"holder" => "div",
					"class" => "",
					"heading" => __( "Content", "PixHealth" ),
					"param_name" => "content", 
					"value" => __( "We are an Experienced & Affordable Construction Company", "PixHealth" ),
					"description" => __( "Banner Text", "PixHealth" )
				 )
			)
		) 
	);
	if ( class_exists( 'WPBakeryShortCode' ) ) {
		class WPBakeryShortCode_Bannerbox extends WPBakeryShortCode {
			
		}
	}
	
	vc_map(
		array(
			"name" => __( "Mailchimp Box", "PixHealth" ),
			"base" => "mailchimpbox",
			"class" => "pix-theme-icon2",
			"category" => __( "Templines", "PixHealth"),
			"show_settings_on_create" => false,
			"content_element" => true,
			"params" => array(),
		) 
	);
	if ( class_exists( 'WPBakeryShortCode' ) ) {
		class WPBakeryShortCode_Mailchimpbox extends WPBakeryShortCode {
			
		}
	}


	/// boxamount
	$params1 = array(
                 array(
					"type" => "textfield",
					"holder" => "div",
					"class" => "",
					"heading" => __( "Title", "PixHealth" ),
					"param_name" => "title",
					"value" => __( "Project", "PixHealth" ),
					"description" => __( "Title.", "PixHealth" )
				 ),
				  array(
					"type" => "textfield",
					"holder" => "div",
					"class" => "",
					"heading" => __( "Amount", "PixHealth" ),
					"param_name" => "amount",
					"value" => __( "999", "PixHealth" ),
					"description" => __( "Amount.", "PixHealth" )
				 ),
				array(
					"type" => "textfield",
					"holder" => "div",
					"class" => "",
					"heading" => __( "Icon", "PixHealth" ),
					"param_name" => "icon",
					"value" => __( "fa-rocket", "PixHealth" ),
					"description" => __( "Add icon <a href='//fortawesome.github.io/Font-Awesome/icons/' target='_blank'>See all icons</a>", "PixHealth" )
				),
            );
    if(!function_exists('fil_init')){
        $params = $params1;
    }else{
        $params = array_merge($params1, $pix_fonts_str);
    }
	vc_map(
		array(
			"name" => __( "Amount Box", "PixHealth" ),
			"base" => "boxamount",
			"class" => "pix-theme-icon2",
			"category" => __( "Templines", "PixHealth"),
			"params" => $params,
		) 
	);
	if ( class_exists( 'WPBakeryShortCode' ) ) {
		class WPBakeryShortCode_Boxamount extends WPBakeryShortCode {
			
		}
	}


	/// box_icon
	$params1 = array(
                array(
					"type" => "textfield",
					"holder" => "div",
					"class" => "",
					"heading" => __( "Title", "PixHealth" ),
					"param_name" => "title",
					"value" => __( "I am title", "PixHealth" ),
					"description" => __( "Add Title ", "PixHealth" )
				),
				array(
					"type" => "textfield",
					"holder" => "div",
					"class" => "",
					"heading" => __( "Icon", "PixHealth" ),
					"param_name" => "icon",
					"value" => '',
					"description" => __( "Add icon <a href='//www.flaticon.com/' target='_blank'>Flaticon</a> or Use theme icons (please check documentation)", "PixHealth" )
				),
            );
    $params2 = array(
                array(
					'type' => 'dropdown',
					'heading' => __( 'Icon Position', 'PixHealth' ),
					'param_name' => 'position',
					'value' => array(
						__( "Left", "PixHealth" ) => 'icon-left',
						__( "Right", "PixHealth" ) => 'icon-right',
						__( "Top", "PixHealth" ) => 'icon-top',
						__( "Only Icon", "PixHealth" ) => 'icon-only',
					),
					'description' => '',
				),
				$add_css_animation,
				array(
					"type" => "textarea_html",
					"holder" => "div",
					"class" => "",
					"heading" => __( "Content", "PixHealth" ),
					"param_name" => "content",
					"value" => __( "<p>I am test text block. Click edit button to change this text.</p>", "PixHealth" ),
					"description" => __( "Enter your content.", "PixHealth" )
				),
            );
    if(!function_exists('fil_init')){
        $params = array_merge($params1, $params2);
    }else{
        $params = array_merge($params1, $pix_fonts_str, $params2);
    }
	vc_map( 
		array(
			"name" => __( "Icon", "PixHealth" ),
			"base" => "box_icon",
			"class" => "pix-theme-icon2",
			"category" => __( "Templines", "PixHealth"),
			"params" => $params,
		)
	);
	if ( class_exists( 'WPBakeryShortCode' ) ) {
		class WPBakeryShortCode_Box_Icon extends WPBakeryShortCode {
			
		}
	}


	/// boxservices
	$params1 = array(
                array(
					"type" => "textfield",
					"holder" => "div",
					"class" => "",
					"heading" => __( "Title", "PixHealth" ),
					"param_name" => "title",
					"value" => __( "I am title", "PixHealth" ),
					"description" => __( "Add Title ", "PixHealth" )
				),
				array(
					"type" => "textfield",
					"holder" => "div",
					"class" => "",
					"heading" => __( "Icon", "PixHealth" ),
					"param_name" => "icon",
					"value" => '',
					"description" => __( "Add icon <a href='//www.flaticon.com/' target='_blank'>Flaticon</a> or Use theme icons (please check documentation)", "PixHealth" )
				),
            );
    $params2 = array(
                array(
					"type" => "checkbox",
					"class" => "",
					"heading" => __( "Use Decor", "PixHealth" ),
					"param_name" => "usedecor",
					"description" => __( "Use decor after icon.", "PixHealth" )
				),
				array(
					"type" => "textfield",
					"holder" => "div",
					"class" => "",
					"heading" => __( "Link", "PixHealth" ),
					"param_name" => "link",
					"description" => __( "Link to service.", "PixHealth" )
				),
				array(
					"type" => "textfield",
					"holder" => "div",
					"class" => "",
					"heading" => __( "Button Text", "PixHealth" ),
					"param_name" => "btntext",
					"value" => __( "LEARN MORE", "PixHealth" ),
					"description" => ''
				),
				$add_css_animation,
				array(
					"type" => "textarea_html",
					"holder" => "div",
					"class" => "",
					"heading" => __( "Content", "PixHealth" ),
					"param_name" => "content",
					"value" => __( "<p>I am test text block. Click edit button to change this text.</p>", "PixHealth" ),
					"description" => __( "Enter your content.", "PixHealth" )
				),
            );
    if(!function_exists('fil_init')){
        $params = array_merge($params1, $params2);
    }else{
        $params = array_merge($params1, $pix_fonts_str, $params2);
    }
	vc_map( 
		array(
			"name" => __( "Services Box", "PixHealth" ),
			"base" => "boxservices",
			"class" => "pix-theme-icon2",
			"category" => __( "Templines", "PixHealth"),
			"params" => $params,
		)
	);
	if ( class_exists( 'WPBakeryShortCode' ) ) {
		class WPBakeryShortCode_Boxservices extends WPBakeryShortCode {
			
		}
	}	
		
		
	vc_map(
		array(
			"name" => __( "Brands", "PixHealth" ),
			"base" => "brandblock",
			"class" => "pix-theme-icon",
			"category" => __( "Templines", "PixHealth"),
			"params" => array(				
				array(
					'type' => 'attach_image',
					'heading' => __( 'Image', 'PixHealth' ),
					'param_name' => 'image',
					'value' => '',
					'description' => __( 'Select image from media library.', 'PixHealth' )
				),
				array(
					"type" => "textfield",
					"holder" => "div",
					"class" => "",
					"heading" => __( "url", "PixHealth" ),
					"param_name" => "url",
					"value" => __( "https://wordpress.com", "PixHealth" ), 
					"description" => __( ".", "PixHealth" )
				)			
			)
		) 
	);
	if ( class_exists( 'WPBakeryShortCode' ) ) {
		class WPBakeryShortCode_Brandblock extends WPBakeryShortCode {
			
		}
	}	
	
	vc_map(
		array(
			"name" => __( "Portfolio", "PixHealth" ),
			"base" => "portfolioblock",
			"class" => "pix-theme-icon",
			"category" => __( "Templines", "PixHealth"),
			"params" => array(	
			/*
				array(
					'type' => 'textfield',
					'heading' => __( 'Title', 'PixHealth' ),
					'param_name' => 'title',
					'value' => __( "FEATURED PROJECTS", "PixHealth" ),
					'description' => __( 'Portfolio title.', 'PixHealth' )
				),
				*/
				array(
					'type' => 'checkbox',
					'heading' => __( 'Categories', 'PixHealth' ),
					'param_name' => 'cats',
					'value' => $cats,
					'description' => __( 'Select categories to show', 'PixHealth' )
				),
				array(
					'type' => 'textfield',
					'heading' => __( 'Items Count', 'PixHealth' ),
					'param_name' => 'count',
					'description' => __( 'Select number images for portfolio.', 'PixHealth' )
				),
				array(
					'type' => 'dropdown',
					'heading' => __( 'Items Per Row', 'PixHealth' ),
					'param_name' => 'perrow',
					'value' => array(
						"5" => '5',
						"4" => '4',
						"3" => '3',
					),
					'description' => '',
				),
				array(
					'type' => 'textfield',
					'heading' => __( 'Button text', 'PixHealth' ),
					'param_name' => 'buttext',
					'value' => __( 'BOOK AN APPOINTMENT', 'PixHealth' ),
					'description' => '',
				),
				array(
					'type' => 'dropdown',
					'heading' => __( 'Layout', 'PixHealth' ),
					'param_name' => 'container',
					'value' => array(
						__( "Wide", "PixHealth" ) => 'container-wide',
						__( "Boxed", "PixHealth" ) => 'container',
					),
					'description' => '',
				),	
				/*
				array(
					'type' => 'dropdown',
					'heading' => __( 'Show Content', 'PixHealth' ),
					'param_name' => 'showcont',
					'value' => array(
						__( "Open Link", "PixHealth" ) => '',
						__( "Show Modal", "PixHealth" ) => 'modal',
					),
					'description' => '',
				),*/
				array(
					'type' => 'dropdown',
					'heading' => __( 'Use margin', 'PixHealth' ),
					'param_name' => 'skin',
					'value' => array(
						__( "No", "PixHealth" ) => 'isotope-skin1',
						__( "Yes", "PixHealth" ) => 'isotope-skin2',
					),
					'description' => '',
				),				
				$add_css_animation,			
			)
		) 
	);
	if ( class_exists( 'WPBakeryShortCode' ) ) {
		class WPBakeryShortCode_Portfolioblock extends WPBakeryShortCode {
			
		}
	}
	
	vc_map(
		array(
			"name" => __( "Portfolio Card", "PixHealth" ),
			"base" => "box_portfolio",
			"class" => "pix-theme-icon",
			"category" => __( "Templines", "PixHealth"),
			"params" => array(
				array(
					'type' => 'dropdown',
					'heading' => __( 'Portfolio Card', 'PixHealth' ),
					'param_name' => 'port',
					'value' => $port,
					'description' => __( 'Select portfolio info to show', 'PixHealth' )
				),
				array(
					'type' => 'textfield',
					'heading' => __( 'Button text', 'PixHealth' ),
					'param_name' => 'buttext',
					'value' => __( 'BOOK AN APPOINTMENT', 'PixHealth' ),
					'description' => '',
				),			
				$add_css_animation,			
			)
		) 
	);
	if ( class_exists( 'WPBakeryShortCode' ) ) {
		class WPBakeryShortCode_Box_Portfolio extends WPBakeryShortCode {
			
		}
	}
	
	vc_map(
		array(
			"name" => __( "Departments", "PixHealth" ),
			"base" => "section_services_cat",
			"class" => "pix-theme-icon",
			"category" => __( "Templines", "PixHealth"),
			"params" => array(	
				/*
				array(
					'type' => 'textfield',
					'heading' => __( 'Title', 'PixHealth' ),
					'param_name' => 'title',
					'value' => __( "FEATURED PROJECTS", "PixHealth" ),
					'description' => __( 'Portfolio title.', 'PixHealth' )
				),
				*/
				array(
					'type' => 'checkbox',
					'heading' => __( 'Departments', 'PixHealth' ),
					'param_name' => 'cats',
					'value' => $cats_serv,
					'description' => __( 'Select departments to show', 'PixHealth' )
				),
				array(
					'type' => 'textfield',
					'heading' => __( 'Items Count', 'PixHealth' ),
					'param_name' => 'count',
					'description' => __( 'Select number departments to show.', 'PixHealth' )
				),
				
				array(
					'type' => 'textfield',
					'heading' => __( 'Button text', 'PixHealth' ),
					'param_name' => 'buttext',
					'value' => __( 'READ MORE', 'PixHealth' ),
					'description' => '',
				),

				$add_css_animation,			
			)
		) 
	);
	if ( class_exists( 'WPBakeryShortCode' ) ) {
		class WPBakeryShortCode_Section_Services_Cat extends WPBakeryShortCode {
			
		}
	}
	
	vc_map(
		array(
			"name" => __( "Services", "PixHealth" ),
			"base" => "section_services",
			"class" => "pix-theme-icon",
			"category" => __( "Templines", "PixHealth"),
			"params" => array(	
				array(
					'type' => 'dropdown',
					'heading' => __( 'Use Template With', 'PixHealth' ),
					'param_name' => 'image_icon',
					'value' => array(
						__( "Images", "PixHealth" ) => 'image',
						__( "Icons", "PixHealth" ) => 'icon',
					),
					'description' => '',
				),
				array(
					'type' => 'checkbox',
					'heading' => __( 'Services', 'PixHealth' ),
					'param_name' => 'serv',
					'value' => $serv,
					'description' => __( 'Select services to show', 'PixHealth' )
				),
				array(
					'type' => 'textfield',
					'heading' => __( 'Items Count', 'PixHealth' ),
					'param_name' => 'count',
					'description' => __( 'Select number departments to show.', 'PixHealth' )
				),
				
				array(
					'type' => 'textfield',
					'heading' => __( 'Button text', 'PixHealth' ),
					'param_name' => 'buttext',
					'value' => __( 'READ MORE', 'PixHealth' ),
					'description' => '',
				),

				$add_css_animation,			
			)
		) 
	);
	if ( class_exists( 'WPBakeryShortCode' ) ) {
		class WPBakeryShortCode_Section_Services extends WPBakeryShortCode {
			
		}
	}
	
	
	vc_map(
		array(
			"name" => __( "Woocommerce Products", "PixHealth" ),
			"base" => "woocommerceblock",
			"class" => "pix-theme-icon",
			"category" => __( "Templines", "PixHealth"),
			"params" => array(
				array(
					'type' => 'textfield',
					'heading' => __( 'Title', 'PixHealth' ),
					'param_name' => 'title',
					'value' => '',
					'description' => __( 'Woocommerce title.', 'PixHealth' )
				),
				array(
					'type' => 'checkbox',
					'heading' => __( 'Categories', 'PixHealth' ),
					'param_name' => 'cats',
					'value' => $cats_woo,
					'description' => __( 'Select categories to show', 'PixHealth' )
				),				
				array(
					'type' => 'textfield',
					'heading' => __( 'Items Count', 'PixHealth' ),
					'param_name' => 'count',
					'description' => __( 'Select number products.', 'PixHealth' )
				),
				array(
					'type' => 'dropdown',
					'heading' => __( 'Carousel', 'PixHealth' ),
					'param_name' => 'carousel',
					'value' => array(
						__( "Enable", "PixHealth" ) => 'enable-owl',
						__( "Disable", "PixHealth" ) => 'disable-owl',						
					),
					'description' => ''
				),				
				array(
					'type' => 'dropdown',
					'heading' => __( 'Slider Controls', 'PixHealth' ),
					'param_name' => 'controls',
					'value' => array(
						__( "Default", "PixHealth" ) => '',
						__( "Controls Right", "PixHealth" ) => 'full-width-slider-controls-right',
						__( "Controls Left", "PixHealth" ) => 'full-width-slider-controls-left',
					),
					'description' => __( 'Select controls position.', 'PixHealth' )
				),
				array(
					'type' => 'textfield',
					'heading' => __( 'Min slides', 'PixHealth' ),
					'param_name' => 'min_slides',
					'description' => __( 'Min slides on page. Default 3.', 'PixHealth' )
				),	
				$add_css_animation,			
			)
		) 
	);
	if ( class_exists( 'WPBakeryShortCode' ) ) {
		class WPBakeryShortCode_Woocommerceblock extends WPBakeryShortCode {
			
		}
	}
	
	
	vc_map(
		array(
			"name" => __( "Sidebar", "PixHealth" ),
			"base" => "sidebarblock",
			"class" => "pix-theme-icon",
			"category" => __( "Templines", "PixHealth"),
			"params" => array(				
				array(
					'type' => 'widgetised_sidebars',
					'heading' => __( 'Select Sidebar', 'PixHealth' ),
					'param_name' => 'sidebar',
					'description' => ''
				),		
				$add_css_animation,			
			)
		) 
	);
	if ( class_exists( 'WPBakeryShortCode' ) ) {
		class WPBakeryShortCode_Sidebarblock extends WPBakeryShortCode {
			
		}
	}
	
	vc_map(
		array(
			"name" => __( "Screenshots Carousel", "PixHealth" ),
			"base" => "screenshots",
			"class" => "pix-theme-icon3",
			"category" => __( "Templines", "PixHealth"),
			"params" => array(
				array(
					'type' => 'attach_images',
					'heading' => __( 'Screenshots', 'PixHealth' ),
					'param_name' => 'images',
					'value' => '',
					'description' => __( 'Select images from media library.', 'PixHealth' )
				),	
			)
		) 
	);
	if ( class_exists( 'WPBakeryShortCode' ) ) {
		class WPBakeryShortCode_Screenshots extends WPBakeryShortCode {
			
		}
	}
		
	vc_map(
		array(
			"name" => __( "Posts Block", "PixHealth" ),
			"base" => "postsblock",
			"class" => "pix-theme-icon",
			"category" => __( "Templines", "PixHealth"),
			"params" => array(				
				array(
					'type' => 'textfield',
					'heading' => __( 'Posts Count', 'PixHealth' ),
					'param_name' => 'count',
					'description' => __( 'Number of posts in carousel. Default 6.', 'PixHealth' )
				),
				array(
					'type' => 'textfield',
					'heading' => __( 'Post Button Text', 'PixHealth' ),
					'param_name' => 'read_more',
					'description' => __( 'Default "READ MORE"', 'PixHealth' )
				),
				array(
					'type' => 'textfield',
					'heading' => __( 'Max slides', 'PixHealth' ),
					'param_name' => 'max_slides',
					'description' => __( 'Max slides on page. Default 2.', 'PixHealth' )
				),
				array(
					'type' => 'textfield',
					'heading' => __( 'Min slides', 'PixHealth' ),
					'param_name' => 'min_slides',
					'description' => __( 'Min slides on page. Default 1.', 'PixHealth' )
				),
				array(
					'type' => 'textfield',

					'heading' => __( 'Width slides', 'PixHealth' ),
					'param_name' => 'width_slides',
					'description' => __( 'Default 370.', 'PixHealth' )
				),
				array(
					'type' => 'textfield',
					'heading' => __( 'Margin slides', 'PixHealth' ),
					'param_name' => 'margin_slides',
					'description' => __( 'Default 30.', 'PixHealth' )
				),
				array(
					'type' => 'textfield',
					'heading' => __( 'Disable Carousel', 'PixHealth' ),
					'param_name' => 'disable_carousel',
					'description' => __( 'Default 1. Type 0 to disable carousel.', 'PixHealth' )
				),		
			)
		) 
	);
	if ( class_exists( 'WPBakeryShortCode' ) ) {
		class WPBakeryShortCode_Postsblock extends WPBakeryShortCode {
			
		}
	}
	
	vc_map(
		array(
			"name" => __( "Quote Box", "PixHealth" ),
			"base" => "boxquote",
			"class" => "pix-theme-icon",
			"category" => __( "Templines", "PixHealth"),
			"params" => array(
				array(
					"type" => "textfield",
					"holder" => "div",
					"class" => "",
					"heading" => __( "Name", "PixHealth" ),
					"param_name" => "name",
					"description" => '',
				),					
				array(
					"type" => "textfield",
					"holder" => "div",
					"class" => "",
					"heading" => __( "Position", "PixHealth" ),
					"param_name" => "position",
					"description" => '',
				),
				array(
					'type' => 'attach_image',
					'heading' => __( 'Image', 'PixHealth' ),
					'param_name' => 'image',
					'value' => '',
					'description' => __( 'Select image from media library.', 'PixHealth' )
				),			
				$add_css_animation,
				array(
					"type" => "textarea_html",
					"holder" => "div",
					"class" => "",
					"heading" => __( "Content", "PixHealth" ),
					"param_name" => "content",
					"value" => '',
					"description" => __( "Enter your content.", "PixHealth" )
				)
			)
		) 
	);	
	if ( class_exists( 'WPBakeryShortCode' ) ) {
		class WPBakeryShortCode_Boxquote extends WPBakeryShortCode {
			
		}
	}	
	
	vc_map(
		array(
			"name" => __( "Video Box", "PixHealth" ),
			"base" => "videobox",
			"class" => "pix-theme-icon",
			"category" => __( "Templines", "PixHealth"),
			"params" => array(
				array(
					"type" => "textfield",
					"holder" => "div",
					"class" => "",
					"heading" => __( "Strong Title", "PixHealth" ),
					"param_name" => "title_strong",
					"description" => '',
				),					
				array(
					"type" => "textfield",
					"holder" => "div",
					"class" => "",
					"heading" => __( "Title", "PixHealth" ),
					"param_name" => "title",
					"description" => '',
				),
				array(
					'type' => 'attach_image',
					'heading' => __( 'Image', 'PixHealth' ),
					'param_name' => 'image',
					'value' => '',
					'description' => __( 'Select image from media library.', 'PixHealth' )
				),
				array(
					"type" => "textfield",
					"holder" => "div",
					"class" => "",
					"heading" => __( "Button Text", "PixHealth" ),
					"param_name" => "btntext",
					"value" => __( "WATCH THE VIDEO", "PixHealth" ),
					"description" => '',
				),	
				array(
					"type" => "textfield",
					"holder" => "div",
					"class" => "",
					"heading" => __( "Link", "PixHealth" ),
					"param_name" => "link",
					"description" => __( "Link to video.", "PixHealth" ),
				),
				$add_css_animation,				
				array(
					"type" => "textarea_html",
					"holder" => "div",
					"class" => "",
					"heading" => __( "Content", "PixHealth" ),
					"param_name" => "content",
					"value" => '',
					"description" => __( "Enter your content.", "PixHealth" )
				)
			)
		) 
	);	
	if ( class_exists( 'WPBakeryShortCode' ) ) {
		class WPBakeryShortCode_Videobox extends WPBakeryShortCode {
			
		}
	}
	
	vc_map(
		array(
			"name" => __( "Appointment Box", "PixHealth" ),
			"base" => "boxappointment",
			"class" => "pix-theme-icon",
			"category" => __( "Templines", "PixHealth"),
			"params" => array(
				 array(
					"type" => "textfield",
					"holder" => "div",
					"class" => "",
					"heading" => __( "Title", "PixHealth" ),
					"param_name" => "title",
					"value" => '',
					"description" => '',
				 ),
				 array(
					'type' => 'textfield',
					'heading' => __( 'Strong Title', 'PixHealth' ),
					'param_name' => 'title_strong',
					'value' => '',
					'description' => __( 'Second title.', 'PixHealth' )
				 ),
				 array(
					"type" => "textfield",
					"holder" => "div",
					"class" => "",
					"heading" => __( "Button Text", "PixHealth" ),
					"param_name" => "btntext",
					"value" => __( "GET AN APPOINTMENT", "PixHealth" ),
					"description" => ''
				 ),	
				 array(
					"type" => "textfield",
					"holder" => "div",
					"class" => "",
					"heading" => __( "Link", "PixHealth" ),
					"param_name" => "link",
					"value" => '',
					"description" => ''
				 ),
				 
				 array(
					"type" => "textfield",
					"holder" => "div",
					"class" => "",
					"heading" => __( "Info Title", "PixHealth" ),
					"param_name" => "title_info",
					"value" => '',
					"description" => __( "Title for right part.", "PixHealth" ),
				 ),
				 $add_css_animation,
				 array(
					"type" => "textarea_html",
					"holder" => "div",
					"class" => "",
					"heading" => __( "Content", "PixHealth" ),
					"param_name" => "content",
					"description" => __( "Enter your content.", "PixHealth" )
				 ),	
			)
		) 
	);	
	if ( class_exists( 'WPBakeryShortCode' ) ) {
		class WPBakeryShortCode_Boxappointment extends WPBakeryShortCode {
			
		}
	}


	/// box_contact
	$params1 = array(
                array(
					"type" => "textfield",
					"holder" => "div",
					"class" => "",
					"heading" => __( "Icon", "PixHealth" ),
					"param_name" => "icon",
					"value" => '',
					"description" => __( "Add icon <a href='//www.flaticon.com/' target='_blank'>Flaticon</a> or Use theme icons (please check documentation)", "PixHealth" )
				),
            );
    $params2 = array(
                $add_css_animation,
				array(
					"type" => "textarea_html",
					"holder" => "div",
					"class" => "",
					"heading" => __( "Content", "PixHealth" ),
					"param_name" => "content",
					"value" => __( "<p>Plot 38, Street 39, UpHill Town, Newyork, USA</p>", "PixHealth" ),
					"description" => __( "Enter your content.", "PixHealth" )
				),
            );
    if(!function_exists('fil_init')){
        $params = array_merge($params1, $params2);
    }else{
        $params = array_merge($params1, $pix_fonts_str, $params2);
    }
	vc_map( 
		array(
			"name" => __( "Contact Box", "PixHealth" ),
			"base" => "box_contact",
			"class" => "pix-theme-icon2",
			"category" => __( "Templines", "PixHealth"),
			"params" => $params,
		)
	);
	if ( class_exists( 'WPBakeryShortCode' ) ) {
		class WPBakeryShortCode_Box_Contact extends WPBakeryShortCode {
			
		}
	}
	
	
	vc_map(
		array(
			"name" => __( "Contact Form 7", "PixHealth" ),
			"base" => "block_cform7",
			"class" => "pix-theme-icon2",
			"category" => __( "Templines", "PixHealth"),
			"params" => array(
				array(
					'type' => 'dropdown',
					'heading' => __( 'Contact Form', 'PixHealth' ),
					'param_name' => 'form_id',
					'value' => $cform7,
					'description' => __( 'Select contact form to show', 'PixHealth' )
				),			
				$add_css_animation,			
			)
		) 
	);
	if ( class_exists( 'WPBakeryShortCode' ) ) {
		class WPBakeryShortCode_Block_Cform7 extends WPBakeryShortCode {
			
		}
	}
	
	vc_map( 
		array(
			"name" => __( "Content Box", "PixHealth" ),
			"base" => "box_content",
			"class" => "pix-theme-icon2",
			"category" => __( "Templines", "PixHealth"),
			"params" => array(
				 array(
					"type" => "textfield",
					"holder" => "div",
					"class" => "",
					"heading" => __( "Title", "PixHealth" ),
					"param_name" => "title",
					"value" => __( "I am title", "PixHealth" ),
					"description" => __( "Add Title", "PixHealth" )
				 ),
				 array(
					"type" => "checkbox",
					"class" => "",
					"heading" => __( "Use Decor", "PixHealth" ),
					"param_name" => "usedecor", 
					"description" => __( "Use decor after title.", "PixHealth" )
				 ),
				 $add_css_animation,
				 array(
					"type" => "textarea_html",
					"holder" => "div",
					"class" => "",
					"heading" => __( "Content", "PixHealth" ),
					"param_name" => "content",
					"value" => __( "<p>I am test text block. Click edit button to change this text.</p>", "PixHealth" ),
					"description" => __( "Enter your content.", "PixHealth" )
				 )
			)
		)
	);
	if ( class_exists( 'WPBakeryShortCode' ) ) {
		class WPBakeryShortCode_Box_Content extends WPBakeryShortCode {
			
		}
	}
	
//if(empty($_GET['vc_action'])){
	
	//////// Vertical Tabs ////////	
	vc_map( array(
		'name' => __( 'Tabs', 'PixHealth' ),
		'base' => 'icontexttabs',
			'class' => 'pix-theme-icon', 
		'as_parent' => array('only' => 'icontexttab'), // Use only|except attributes to limit child shortcodes (separate multiple values with comma)
		'content_element' => true,
		'show_settings_on_create' => true,
		'category' => __( 'Templines', 'PixHealth'),
		'front_enqueue_js' => get_template_directory_uri() . '/library/functions/shortcodes/shortcode.js',
		'params' => array(
			array(
				'type' => 'textfield',
				'heading' => __( 'Title', 'PixHealth' ),
				'param_name' => 'title',
				'description' => __( 'Tabs title.', 'PixHealth' )
			),
			array(
				'type' => 'textfield',
				'heading' => __( 'Strong Title ', 'PixHealth' ),
				'param_name' => 'title_strong',
				'description' => __( 'Strong tabs title text.', 'PixHealth' )
			),
			array(
				'type' => 'textfield',
				'heading' => __( 'Title Description', 'PixHealth' ),
				'param_name' => 'title_desc',
				'description' => __( 'Tabs title description.', 'PixHealth' )
			),
		),
		'js_view' => 'VcColumnView', // must be added for all Containers ( or should be extended in js ). VC Dev team
	) );
	/// icontexttab
	$params1 = array(
                array(
					'type' => 'textfield',
					'heading' => __( 'Title', 'PixHealth' ),
					'param_name' => 'title',
					'description' => __( 'Tab title.', 'PixHealth' )
				),
				array(
					'type' => 'attach_image',
					'heading' => __( 'Image', 'PixHealth' ),
					'param_name' => 'image',
					'value' => '',
					'description' => __( 'Select image from media library.', 'PixHealth' )
				),
				array(
					'type' => 'textfield',
					'heading' => __( 'Icon', 'PixHealth' ),
					'param_name' => 'icon',
					'description' => __( 'Add icon fa-ambulance <a href="//fortawesome.github.io/Font-Awesome/icons/" target="_blank">See all icons</a>', 'PixHealth' )
				),
            );
    $params2 = array(
                array(
					"type" => "textarea_html",
					"holder" => "div",
					"class" => "",
					"heading" => __( "Content", "PixHealth" ),
					"param_name" => "content",
					"value" => __( "<p>I am test text block. Click edit button to change this text.</p>", "PixHealth" ),
					"description" => __( "Enter your content.", "PixHealth" )
				),
            );
    if(!function_exists('fil_init')){
        $params = array_merge($params1, $params2);
    }else{
        $params = array_merge($params1, $pix_fonts_str, $params2);
    }
	vc_map( array(
		'name' => __( 'Tab', 'PixHealth' ),
		'base' => 'icontexttab',
		'as_child' => array('only' => 'icontexttabs'),
		'content_element' => true,
		'front_enqueue_js' => get_template_directory_uri() . '/library/functions/shortcodes/shortcode.js',
		'params' => $params,
	) );
	if ( class_exists( 'WPBakeryShortCodesContainer' ) ) {
		class WPBakeryShortCode_Icontexttabs extends WPBakeryShortCodesContainer {
		}
	}
	if ( class_exists( 'WPBakeryShortCode' ) ) {
		class WPBakeryShortCode_Icontexttab extends WPBakeryShortCode {
		}
	}
	//////////////////////////////////
	
	
	//////// Middle Vertical Tabs ////////	
	vc_map( array(
		'name' => __( 'Tabs and Image', 'PixHealth' ),
		'base' => 'tabs_middle',
		'class' => 'pix-theme-icon', 
		'as_parent' => array('only' => 'tab_middle'), // Use only|except attributes to limit child shortcodes (separate multiple values with comma)
		'content_element' => true,
		'show_settings_on_create' => false,
		'category' => __( 'Templines', 'PixHealth'),
		'front_enqueue_js' => get_template_directory_uri() . '/library/functions/shortcodes/shortcode.js',
		'js_view' => 'VcColumnView', // must be added for all Containers ( or should be extended in js ). VC Dev team
	) );
	vc_map( array(
		'name' => __( 'Tab', 'PixHealth' ),
		'base' => 'tab_middle',
		'as_child' => array('only' => 'tabs_middle'),
		'content_element' => true,
		'front_enqueue_js' => get_template_directory_uri() . '/library/functions/shortcodes/shortcode.js',
		'params' => array(
			array(
				'type' => 'textfield',
				'heading' => __( 'Title', 'PixHealth' ),
				'param_name' => 'title',
				'description' => __( 'Tab title.', 'PixHealth' )
			),
			array(
				'type' => 'textfield',
				'heading' => __( 'Content Title', 'PixHealth' ),
				'param_name' => 'title_cont',
				'description' => __( 'Title for content.', 'PixHealth' )
			),
			array(
				'type' => 'attach_image',
				'heading' => __( 'Image', 'PixHealth' ),
				'param_name' => 'image',
				'value' => '',
				'description' => __( 'Select image from media library.', 'PixHealth' )
			),	
			array(
				"type" => "textarea_html",
				"holder" => "div",
				"class" => "",
				"heading" => __( "Content", "PixHealth" ),
				"param_name" => "content",
				"value" => __( "<p>I am test text block. Click edit button to change this text.</p>", "PixHealth" ),
				"description" => __( "Enter your content.", "PixHealth" )
			),
		)
	) );
	if ( class_exists( 'WPBakeryShortCodesContainer' ) ) {
		class WPBakeryShortCode_Tabs_Middle extends WPBakeryShortCodesContainer {
		}
	}
	if ( class_exists( 'WPBakeryShortCode' ) ) {
		class WPBakeryShortCode_Tab_Middle extends WPBakeryShortCode {
		}
	}
	//////////////////////////////////
	
	

	
	//////// Carousel Reviews ////////
	vc_map( array(
		'name' => __( 'Reviews', 'PixHealth' ),
		'base' => 'caurusel_reviews',
		'class' => 'pix-theme-icon3', 
		'as_parent' => array('only' => 'caurusel_review'), // Use only|except attributes to limit child shortcodes (separate multiple values with comma)
		'content_element' => true,
		'show_settings_on_create' => true,
		'category' => __( 'Templines', 'PixHealth'),
		'description' => __( 'Don\'t use in FRONTEND EDITOR ', 'PixHealth'),
		'front_enqueue_js' => get_template_directory_uri() . '/library/functions/shortcodes/shortcode.js',
		'params' => array(
			array(
				'type' => 'dropdown',
				'heading' => __( 'Reviews per page', 'PixHealth' ),
				'param_name' => 'reviewS_per_page',
				'value' => array(
					__( "1", "PixHealth" ) => '1',
					__( "2", "PixHealth" ) => '2',
				),
				'description' => __( 'Select reviews number.', 'PixHealth' )
			),
		),
		'js_view' => 'VcColumnView',
		
	) );
	vc_map( array(
		'name' => __( 'Review', 'PixHealth' ),
		'base' => 'caurusel_review',
		'class' => 'pix-theme-icon', 
		'as_child' => array('only' => 'caurusel_reviews'),
		'content_element' => true,
		'front_enqueue_js' => get_template_directory_uri() . '/library/functions/shortcodes/shortcode.js',
		'params' => array(
			array(
				'type' => 'attach_image',
				'heading' => __( 'Image', 'PixHealth' ),
				'param_name' => 'image',
				'description' => __( 'Select image.', 'PixHealth' )
			),
			array(
				'type' => 'textfield',
				'heading' => __( 'Name', 'PixHealth' ),
				'param_name' => 'title',
				'description' => __( 'Person name.', 'PixHealth' )
			),
			array(
				'type' => 'textfield',
				'heading' => __( 'Short description', 'PixHealth' ),
				'param_name' => 'short_desc',
				'description' => __( 'Left text under the name.', 'PixHealth' )
			),
			array(
				'type' => 'textfield',
				'heading' => __( 'Place', 'PixHealth' ),
				'param_name' => 'place',
				'description' => __( 'Right text under the name.', 'PixHealth' )
			),
			array(
				"type" => "textarea_html",
				"holder" => "div",
				"class" => "",
				"heading" => __( "Review Text", "PixHealth" ),
				"param_name" => "content",
				"value" => __( "<p>I am test text block. Click edit button to change this text.</p>", "PixHealth" ),
				"description" => __( "Enter text.", "PixHealth" )
			),
		)
	) );
	if ( class_exists( 'WPBakeryShortCodesContainer' ) ) {
		class WPBakeryShortCode_Caurusel_Reviews extends WPBakeryShortCodesContainer {
		}
	}
	if ( class_exists( 'WPBakeryShortCode' ) ) {
		class WPBakeryShortCode_Caurusel_Review extends WPBakeryShortCode {
		}
	}
	/////////////////////////////////
	
	
	
	//////// Our Team ////////
	vc_map( array(
		'name' => __( 'Team carousel', 'PixHealth' ),
		'base' => 'ourteam',
		'class' => 'pix-theme-icon3', 
		'as_parent' => array('only' => 'teammember'), // Use only|except attributes to limit child shortcodes (separate multiple values with comma)
		'content_element' => true,
		'show_settings_on_create' => false,
		'category' => __( 'Templines', 'PixHealth'),
		'params' => array(
			array(
				'type' => 'dropdown',
				'heading' => __( 'Carousel', 'PixHealth' ),
				'param_name' => 'disable_carousel',
				'value' => array(
					__('Enable', 'PixHealth') => 1,
					__('Disable', 'PixHealth') => 0,
				),
				'description' => __( 'On/off carousel', 'PixHealth' )
			),
			array(
				'type' => 'textfield',
				'heading' => __( 'Button Text', 'PixHealth' ),
				'param_name' => 'btntext',
				'value' => __( 'BOOK AN APPOINTMENT', 'PixHealth' ),
				'description' => '',
			),
			$add_css_animation,
		),
		'js_view' => 'VcColumnView',
		
	) );
	vc_map( array(
		'name' => __( 'Team Member', 'PixHealth' ),
		'base' => 'teammember',
		'class' => 'pix-theme-icon', 
		'as_child' => array('only' => 'ourteam'),
		'content_element' => true,
		'params' => array(
			array(
				'type' => 'attach_image',
				'heading' => __( 'Image', 'PixHealth' ),
				'param_name' => 'image',
				'description' => __( 'Select image.', 'PixHealth' )
			),
			array(
				'type' => 'textfield',
				'heading' => __( 'Name', 'PixHealth' ),
				'param_name' => 'name',
				'description' => __( 'Team member name.', 'PixHealth' )
			),
			array(
				'type' => 'textfield',
				'heading' => __( 'Position', 'PixHealth' ),
				'param_name' => 'position',
				'description' => __( 'President.', 'PixHealth' )
			),
			array(
				'type' => 'textfield',
				'heading' => __( 'Link', 'PixHealth' ),
				'param_name' => 'link',
				'description' => __( 'Link to profile.', 'PixHealth' )
			),
			array(
				'type' => 'textfield',
				'heading' => __( 'Facebook', 'PixHealth' ),
				'param_name' => 'facebook',
				'description' => __( 'Link to facebook.', 'PixHealth' )
			),
			array(
				'type' => 'textfield',
				'heading' => __( 'Twitter', 'PixHealth' ),
				'param_name' => 'twitter',
				'description' => __( 'Link to twitter.', 'PixHealth' )
			),
			array(
				'type' => 'textfield',
				'heading' => __( 'Google+', 'PixHealth' ),
				'param_name' => 'googleplus',
				'description' => __( 'Link to google+.', 'PixHealth' )
			),
			array(
				'type' => 'textfield',
				'heading' => __( 'LinkedIn', 'PixHealth' ),
				'param_name' => 'linkedin',
				'description' => __( 'Link to linkedin.', 'PixHealth' )
			),
		
		)
	) );
	if ( class_exists( 'WPBakeryShortCodesContainer' ) ) {
		class WPBakeryShortCode_Ourteam extends WPBakeryShortCodesContainer {
		}
	}
	if ( class_exists( 'WPBakeryShortCode' ) ) {
		class WPBakeryShortCode_Teammember extends WPBakeryShortCode {
		}
	}
	////////////////////////
	
	
//} ////// <= End vc_inline
	
}

