<?php /* Template Name: Portfolio page */ 

$pixhealth_custom =  get_post_custom($post->ID);
$pixhealth_layout = isset ($pixhealth_custom['pixhealth_page_layout']) ? $pixhealth_custom['pixhealth_page_layout'][0] : '2';
$pixhealth_sidebar = isset ($pixhealth_custom['pixhealth_selected_sidebar'][0]) ? $pixhealth_custom['pixhealth_selected_sidebar'][0] : 'Blog Sidebar';
$pixhealth_options = get_option('pixhealth_general_settings');
?>
<?php get_header();?>
<main class="section" id="main">
  <?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
  <?php the_content(); ?>
  <?php endwhile; ?>
</main>
<?php get_footer();?>
