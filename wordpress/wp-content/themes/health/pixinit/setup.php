<?php

if(file_exists(get_template_directory() .'/one-click-demo-install/init.php'))
	require get_template_directory() .'/one-click-demo-install/init.php';

function pixhealth_extended_upload ( $mime_types =array() ) { 
   // The MIME types listed here will be allowed in the media library.
   // You can add as many MIME types as you want.
   $mime_types['gz']  = 'application/x-gzip';
   $mime_types['zip']  = 'application/zip';
   return $mime_types;
}
 
add_filter('upload_mimes', 'pixhealth_extended_upload');


if (is_admin() && isset($_GET['activated'])){
	wp_redirect(admin_url('admin.php?page=adminpanel'));
	unregister_sidebar('header-sidebar');
}

/*************** AFTER THEME SETUP FUNCTIONS ****************/
function pixhealth_setup(){
	
	global $pixhealth_options;
	
	// Language support
	load_theme_textdomain('PixHealth', get_template_directory() . '/languages');
	$pixhealth_locale = get_locale();
	$pixhealth_locale_file = get_template_directory() . "/languages/$pixhealth_locale.php";
	if (is_readable($pixhealth_locale_file))	{
		require_once ($pixhealth_locale_file);
	}

	// ADD SUPPORT FOR POST THUMBS
	add_theme_support('post-thumbnails');

	// Define various thumbnail sizes
	$width = (!empty($pixhealth_options['pixhealth_portfolio_width'])) ? $pixhealth_options['pixhealth_portfolio_width'] : 250;
	$height = (!empty($pixhealth_options['pixhealth_portfolio_height'])) ? $pixhealth_options['pixhealth_portfolio_height'] : 270;
	add_image_size('pixhealth-portfolio-thumb', $width, $height, true);
	add_image_size('pixhealth-services-thumb', 370, 300, true);
	add_image_size('pixhealth-post-thumb', 275, 275, true);
	add_theme_support("title-tag");
	add_theme_support('automatic-feed-links');
	add_theme_support('post-formats', array(
		'gallery',
		'quote'
	));

	// ADD SUPPORT FOR WORDPRESS 3 MENUS ************/
	add_theme_support('menus');

	// Register Navigations	
	function pixhealth_custom_menus()	{
		register_nav_menus(array(
			'primary_nav' => __('Primary Navigation', 'PixHealth') ,
			'top_nav' => __('Top Navigation', 'PixHealth') ,
			'footer_nav' => __('Footer Navigation', 'PixHealth')
		));
	}
	
	add_action('init', 'pixhealth_custom_menus');
}
	
add_action('after_setup_theme', 'pixhealth_setup');


$args = array(
	'flex-width' => true,
	'width' => 350,
	'flex-height' => true,
	'height' => 'auto',
	'default-image' => get_template_directory_uri() . '/images/logo.jpg'
);

add_theme_support('custom-header', $args);


$args = array(
	'default-color' => 'FFFFFF'
);

add_theme_support('custom-background', $args);

/************************************************************/

function pixhealth_wp_get_attachment( $attachment_id ) {
	$attachment = get_post( $attachment_id );
	return array(
		'alt' => get_post_meta( $attachment->ID, '_wp_attachment_image_alt', true ),
		'caption' => $attachment->post_excerpt,
		'description' => $attachment->post_content,
		'href' => get_permalink( $attachment->ID ),
		'src' => $attachment->guid,
		'title' => $attachment->post_title
	);
}

?>