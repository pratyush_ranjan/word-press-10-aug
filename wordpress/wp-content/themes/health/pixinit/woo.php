<?php

/*****WOOCOMERCE**********/
add_theme_support('woocommerce');
add_filter('woocommerce_enqueue_styles', '__return_false');
remove_action('woocommerce_before_shop_loop', 'woocommerce_result_count', 20);
remove_action('woocommerce_before_shop_loop', 'woocommerce_catalog_ordering', 30);
add_action('woocommerce_before_shop_loop_tool', 'woocommerce_result_count', 20);
add_action('woocommerce_before_shop_loop_tool', 'woocommerce_catalog_ordering', 30);
remove_action('woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_rating', 5);
add_action('woocommerce_after_shop_loop_item', 'woocommerce_template_loop_add_to_cart', 10);

add_filter( 'loop_shop_per_page', create_function( '$cols', 'return '.pixhealth_get_option('pixhealth_products_per_page','6').';' ), 20 );

function pixhealth_override_page_title(){
	return false;
}

add_filter('woocommerce_show_page_title', 'pixhealth_override_page_title');


function pixhealth_woocommerce_breadcrumbs(){
	return array(
		'delimiter' => '',
		'wrap_before' => '  <div class="breadcrumbsBox"><ol class="breadcrumb">',
		'wrap_after' => '</ol></div>',
		'before' => '<li>',
		'after' => '</li>',
		'home' => __('&lt;i class="icon icon-home color_primary"&gt;&lt;/i&gt;', 'PixHealth'),
	);
}

add_filter('woocommerce_breadcrumb_defaults', 'pixhealth_woocommerce_breadcrumbs');


function pixhealth_related_products_args($args){
	global $pixhealth_options;
	$args['posts_per_page'] = $pixhealth_options['pixhealth_pelated_products']; // 4 related products
	return $args;
}
	
add_filter('woocommerce_output_related_products_args', 'pixhealth_related_products_args');

?>