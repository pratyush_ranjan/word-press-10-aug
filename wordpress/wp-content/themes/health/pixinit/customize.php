<?php
/********************* Customize *********************/
add_action('customize_register', 'pixhealth_remove_customize_sections');

function pixhealth_remove_customize_sections($wp_customize)
	{
	$wp_customize->remove_section('header_image');
	$wp_customize->remove_section('background_image');
	$wp_customize->remove_section('colors');
	$wp_customize->remove_section('nav');
	$wp_customize->remove_panel('widgets');

	// / Global Colors ///

	$wp_customize->add_section('pixhealth_colors', array(
		'title' => __('Global Colors', 'PixHealth') ,
		'priority' => 20
	));
	$wp_customize->add_setting('pixhealth_customize_options[first_color]', array(
		'default' => '',
		'transport' => 'refresh',
		'type' => 'option',

		'sanitize_callback' => 'esc_attr',

	));
	$wp_customize->add_setting('pixhealth_customize_options[second_color]', array(
		'default' => '',
		'transport' => 'refresh',
		'type' => 'option',

		'sanitize_callback' => 'esc_attr',

	));
	$wp_customize->add_setting('pixhealth_customize_options[third_color]', array(
		'default' => '',
		'transport' => 'refresh',
		'type' => 'option',

		'sanitize_callback' => 'esc_attr',

	));
	$wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'first_color', array(
		'label' => __('First Color', 'PixHealth') ,
		'section' => 'pixhealth_colors',
		'settings' => 'pixhealth_customize_options[first_color]'
	)));
	$wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'second_color', array(
		'label' => __('Second Color', 'PixHealth') ,
		'section' => 'pixhealth_colors',
		'settings' => 'pixhealth_customize_options[second_color]'
	)));
	$wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'third_color', array(
		'label' => __('Third Color', 'PixHealth') ,
		'section' => 'pixhealth_colors',
		'settings' => 'pixhealth_customize_options[third_color]'
	)));

	// ////////////////////////////////////////////////////////
	// / Global Font ///

	$wp_customize->add_section('pixhealth_font', array(
		'title' => __('Global Font', 'PixHealth') ,
		'priority' => 25,
		'description' => 'Add new <a href="//www.google.com/fonts/" target="_blank">Google Web Fonts</a>'
	));
	$wp_customize->add_setting('pixhealth_customize_options[font_family]', array(
		'default' => '',
		'transport' => 'refresh',
		'type' => 'option',

		'sanitize_callback' => 'esc_html',

	));
	$wp_customize->add_setting('pixhealth_customize_options[font_weight]', array(
		'default' => '',
		'transport' => 'refresh',
		'type' => 'option',

		'sanitize_callback' => 'esc_html',

	));
	$wp_customize->add_control('pixhealth_font_family_control', array(
		'label' => __('Font Family', 'PixHealth') ,
		'section' => 'pixhealth_font',
		'settings' => 'pixhealth_customize_options[font_family]',
		'description' => 'Example: Oswald'
	));
	$wp_customize->add_control('pixhealth_font_weight_control', array(
		'label' => __('Font Weight', 'PixHealth') ,
		'section' => 'pixhealth_font',
		'settings' => 'pixhealth_customize_options[font_weight]',
		'description' => 'Example: 300'
	));

	// ////////////////////////////////////////////////////////
	// / Title Font ///

	$wp_customize->add_section('pixhealth_font_title', array(
		'title' => __('Title Font', 'PixHealth') ,
		'priority' => 30,
		'description' => 'Add new <a href="//www.google.com/fonts/" target="_blank">Google Web Fonts</a>'
	));
	$wp_customize->add_setting('pixhealth_customize_options[font_title_family]', array(
		'default' => '',
		'transport' => 'refresh',
		'type' => 'option',

		'sanitize_callback' => 'esc_html',

	));
	$wp_customize->add_setting('pixhealth_customize_options[font_title_weight]', array(
		'default' => '',
		'transport' => 'refresh',
		'type' => 'option',

		'sanitize_callback' => 'esc_html',

	));
	$wp_customize->add_control('pixhealth_font_title_family_control', array(
		'label' => __('Font Family', 'PixHealth') ,
		'section' => 'pixhealth_font_title',
		'settings' => 'pixhealth_customize_options[font_title_family]',
		'description' => 'Example: Oswald'
	));
	$wp_customize->add_control('pixhealth_font_title_weight_control', array(
		'label' => __('Font Weight', 'PixHealth') ,
		'section' => 'pixhealth_font_title',
		'settings' => 'pixhealth_customize_options[font_title_weight]',
		'description' => 'Example: 700'
	));

	// ////////////////////////////////////////////////////////

}

function pixhealth_customize_live_preview(){
	if (isset($_POST['wp_customize']) && $_POST['wp_customize'] == 'on') {
		$settings = json_decode(stripslashes($_POST['customized']));
		$settings_array = array();
		foreach($settings as $key => $val){
			$key = substr(str_replace("pixhealth_customize_options[", "", $key), 0, -1);
			$settings_array[$key] = $val;
		}
		if(class_exists('WP_Session')) {
			$pixhealth_wp_session = WP_Session::get_instance();
			$pixhealth_wp_session['customize_live_preview'] = $settings_array;
		}else {
			update_option('pixhealth_customize_live_preview', $settings_array);
		}
	}
}
add_action( 'customize_preview_init', 'pixhealth_customize_live_preview');

?>