<?php
function pixhealth_fonts(){
	$pixhealth_customize = get_option('pixhealth_customize_options');
	$bodyFont = !empty($pixhealth_customize['font_family']) && $pixhealth_customize['font_family'] != '' ? $pixhealth_customize['font_family'] : '';
	$bodyWeight = !empty($pixhealth_customize['font_weight']) && $pixhealth_customize['font_weight'] != '' ? $pixhealth_customize['font_weight'] : '';
	$titleFont = !empty($pixhealth_customize['font_title_family']) && $pixhealth_customize['font_title_family'] != '' ? $pixhealth_customize['font_title_family'] : '';
	$titleWeight = !empty($pixhealth_customize['font_title_weight']) && $pixhealth_customize['font_title_weight'] != '' ? $pixhealth_customize['font_title_weight'] : '';
	if (($bodyFont != '' || $titleFont != '') && ($bodyFont == $titleFont)){
		$api_font = str_replace(" ", '+', $bodyFont);
		if ($bodyWeight != '' || $titleWeight != ''){
			$api_font.= ':';
			if ($bodyWeight == $titleWeight){
				$api_font.= $bodyWeight;
			}
			elseif ($bodyWeight != '' && $titleWeight != ''){
				$api_font.= $bodyWeight < $titleWeight ? $bodyWeight . ',' . $titleWeight : $titleWeight . ',' . $bodyWeight;
			}
		}

		$font_name = str_replace(" ", '-', $bodyFont);
		wp_enqueue_style('pixtheme-font-' . $font_name, "//fonts.googleapis.com/css?family=" . $api_font);
	}else{
		if ($bodyFont != ''){
			$api_font = str_replace(" ", '+', $bodyFont);
			$api_font.= $bodyWeight != '' ? ':' . $bodyWeight : '';
			$font_name = str_replace(" ", '-', $bodyFont);
			wp_enqueue_style('pixtheme-font-' . $font_name, "//fonts.googleapis.com/css?family=" . $api_font);
		}

		if ($titleFont != ''){
			$api_font = str_replace(" ", '+', $titleFont);
			$api_font.= $titleWeight != '' ? ':' . $titleWeight : '';
			$font_name = str_replace(" ", '-', $titleFont);
			wp_enqueue_style('pixtheme-font-' . $font_name, "//fonts.googleapis.com/css?family=" . $api_font);
		}
		
	}
	
}

add_action('wp_enqueue_scripts', 'pixhealth_fonts');
/************************************************************/
?>