<?php

/************* LOAD REQUIRED SCRIPTS AND STYLES *************/
function pixhealth_loadscripts(){
	global $post;
	$pixhealth_options = isset($_POST['options']) ? $_POST['options'] : get_option('pixhealth_general_settings');
	if ($GLOBALS['pagenow'] != 'wp-login.php' && !is_admin()){
		
		/* MAIN CSS */
		wp_enqueue_style('style', get_stylesheet_uri());
		//wp_enqueue_style('pixtheme-theme', get_template_directory_uri() . '/css/theme.css');
		wp_enqueue_style('pixtheme-woocommerce', get_template_directory_uri() . '/woocommerce/assets/css/woocommerce.css');
		wp_enqueue_style('pixtheme-woocommerce-layout', get_template_directory_uri() . '/woocommerce/assets/css/woocommerce-layout.css');
		
		
		if (pixhealth_get_option('pixhealth_responsive','1')){
			wp_enqueue_style('pixtheme-responsive', get_template_directory_uri() . '/css/responsive.css');
		}else{
			wp_enqueue_style('pixtheme-no-responsive', get_template_directory_uri() . '/css/no-responsive.css');
		}
			
			
		wp_dequeue_style('js_composer_front');
		wp_deregister_style('js_composer_front');
		wp_enqueue_style('pixhealth_composer-css', get_template_directory_uri() . '/css/js_composer.css');
		
		
	

		/* MASTER CSS */
		wp_enqueue_style('pixtheme-bxslider', get_template_directory_uri() . '/assets/bxslider/jquery.bxslider.css');
		wp_enqueue_style('pixtheme-master', get_template_directory_uri() . '/css/master.css');
		wp_enqueue_style('pixtheme-isotope', get_template_directory_uri() . '/assets/isotope/isotope.css');
	    wp_enqueue_style('pixtheme-shop', get_template_directory_uri() . '/css/shop.css');
		
		
		/* FONTS*/
		//wp_enqueue_style('pixtheme-icomoon', get_template_directory_uri() . '/fonts/icomoon/style.css');
		//wp_enqueue_style('pixtheme-simple-line-icons', get_template_directory_uri() . '/fonts/simple/simple-line-icons.css');
		//wp_enqueue_style('pixtheme-flaticon', get_template_directory_uri() . '/fonts/flaticon/flaticon.css');
		//wp_enqueue_style('pixtheme-fontawesome', get_template_directory_uri() . '/fonts/font-awesome/css/font-awesome.min.css');
		wp_enqueue_style('pixtheme-dynamic-styles', get_template_directory_uri() . '/css/dynamic-styles.php');

		
		if(isset($_GET['get_scheme'])){
			$wp_session['color-scheme'] = $_GET['get_scheme'];
			wp_enqueue_style('pixtheme-color-scheme', get_template_directory_uri() . '/assets/switcher/css/'.$wp_session['color-scheme'].'.css');			
		}
		elseif(!empty($wp_session['color-scheme'])){
			wp_enqueue_style('pixtheme-color-scheme', get_template_directory_uri() . '/assets/switcher/css/'.$wp_session['color-scheme'].'.css');
		}
		elseif (pixhealth_get_option('pix_color_switcher','0') == 0 && !isset($_GET['get_scheme']) && !isset($wp_session['color-scheme'])) {
			if(isset($post->ID) && $post->ID>0 && get_post_meta($post->ID, 'color_sheme', 1) != ''){
				wp_enqueue_style('pixtheme-color-scheme', get_template_directory_uri() . '/assets/switcher/css/'.get_post_meta($post->ID, 'color_sheme', 1).'.css');
			}else{
            	wp_enqueue_style('pixtheme-color-scheme', get_template_directory_uri() . '/assets/switcher/css/'.pixhealth_get_option('pix_color_scheme','color1').'.css');
			}
        }


		// jQuery
		wp_enqueue_script('pixtheme-migrate', get_template_directory_uri() . '/js/jquery-migrate-1.2.1.min.js', array() , '3.3', true);
		wp_enqueue_script('pixtheme-jquery-ui', get_template_directory_uri() . '/js/jquery-ui.min.js', array() , '3.3', true);


		// Bootstrap Core JavaScript
		wp_enqueue_script('pixtheme-bootstrap', get_template_directory_uri() . '/js/bootstrap.min.js', array() , '3.3', true);
		wp_enqueue_script('pixtheme-modernizr', get_template_directory_uri() . '/js/modernizr.js');


		// User agent
		wp_enqueue_script('pixtheme-cssua', get_template_directory_uri() . '/js/cssua.min.js', array() , '3.3', true);


		// Waypoint
		wp_enqueue_script('pixtheme-waypoints', get_template_directory_uri() . '/js/waypoints.min.js', array() , '3.3', true);
		
		
		// Isotope filter
		wp_enqueue_script('isotope', get_template_directory_uri() . '/assets/isotope/jquery.isotope.min.js', array() , '3.3', true);
		wp_enqueue_script('masonry', get_template_directory_uri() . '/assets/events/masonry.pkgd.min.js', array() , '3.3', true);		
		wp_enqueue_script('pixtheme-easypiechart', get_template_directory_uri() . '/js/jquery.easypiechart.js', array() , '3.3', true);
		
		
		// Ios Fix
		wp_enqueue_script('pixtheme-ios-orientationchange-fix', get_template_directory_uri() . '/js/ios-orientationchange-fix.js', array() , '3.3', true);
		
		
		// Bx Slider
		wp_enqueue_script('pixtheme-bxslider', get_template_directory_uri() . '/assets/bxslider/jquery.bxslider.min.js', array() , '3.3', true);


		// Flex Slider
		wp_enqueue_script('pixtheme-flexslider', get_template_directory_uri() . '/assets/flexslider/jquery.flexslider-min.js', array() , '3.3', true);
		

		// Pretty Photo
		wp_enqueue_script('pixtheme-prettyphoto', get_template_directory_uri() . '/assets/prettyphoto/js/jquery.prettyPhoto.js', array() , '3.6', true);
		
		// Switcher	
		wp_enqueue_script('pixtheme-bootstrap-select', get_template_directory_uri() . '/assets/switcher/js/bootstrap-select.js', array(), '3.3', true);
		wp_enqueue_script('pixtheme-colorpicker-evol', get_template_directory_uri() . '/assets/switcher/js/evol.colorpicker.min.js', array(), '3.3', true);
		wp_enqueue_script('pixtheme-dmss-js', get_template_directory_uri() . '/assets/switcher/js/dmss.js', array(), '3.3', true);
		wp_enqueue_style('pixtheme-css-switcher', get_template_directory_uri() . '/assets/switcher/css/switcher.css');		
		
		// flickrfeed	
		wp_enqueue_script('pixtheme-flickrfeed', get_template_directory_uri() . '/assets/jflickrfeed/jflickrfeed.min.js', array() , '1.1', true);		
		wp_enqueue_script('pixtheme-footer', get_template_directory_uri() . '/js/scripts.js', array() , '1.1', true);		
	}
}

add_action('wp_enqueue_scripts', 'pixhealth_loadscripts'); //Load All Scripts


function pixhealth_load_custom_wp_admin_style(){
	wp_register_script('custom_wp_admin_script', get_template_directory_uri() . '/js/custom-admin.js', false, '1.0.0');
	wp_enqueue_script('custom_wp_admin_script');
}

add_action('admin_enqueue_scripts', 'pixhealth_load_custom_wp_admin_style');


function pixhealth_add_editor_styles() {
	add_editor_style( get_template_directory_uri() . '/css/editor_styles.css' );
}

add_action( 'current_screen', 'pixhealth_add_editor_styles' );
/***********************************************************/