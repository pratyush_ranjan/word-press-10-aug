<?php
/**
* Header Template
*
* Here we setup all logic and XHTML that is required for the header section of all screens.
*
* @package WooFramework
* @subpackage Template
*/
$pix_guest = get_user_by( 'login', 'user_customizer' );
?>
<!DOCTYPE html>
<html class="noIE" <?php language_attributes(); ?>>
<head>
<meta charset="utf-8" />
<?php if(pixhealth_get_option('pixhealth_responsive','1')):?>
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<?php endif?>
<link rel="alternate" type="application/rss+xml" title="RSS2.0" href="<?php bloginfo('rss2_url'); ?>" />
<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
<script src="https://code.jquery.com/jquery-2.2.4.min.js" integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=" crossorigin="anonymous"></script>

  <script src="https://cdnjs.cloudflare.com/ajax/libs/jstree/3.2.1/jstree.min.js"></script>

  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jstree/3.2.1/themes/default/style.min.css" />
  
  <script src="https://code.jquery.com/jquery-2.2.4.min.js" integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=" crossorigin="anonymous"></script>

  <script src="https://cdnjs.cloudflare.com/ajax/libs/jstree/3.2.1/jstree.min.js"></script>

  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jstree/3.2.1/themes/default/style.min.css" />

  <style type="text/css">
      
    .arrow{

      background-image: url("rsz_down_right-512.png");
        background-repeat: no-repeat;
        display: inline-block;
        height: 20px;
        width: 20px;
      padding-left: 10px;
  
    }

    li{

      list-style-type:none; 
    }

    #hidden-file{

      display: none;
    }

    /*#fl{

      display: none;

    }
    
    #lbl_file {
    background-color: #007a00;
    border: 1px solid #007a00;
    border-radius: 25px;
    color: #fff;
    display: block;
    font-size: 14px;
    margin: 1% 8% 0;
    padding: 10px 20px;
    width: 100px;
    cursor:pointer;
}

    .sbmt{

      margin: 2% 8%;
    }*/

  </style>
<?php  $pixhealth_options = get_option('pixhealth_general_settings'); ?>
<?php if(pixhealth_get_option('pixhealth_favicon','')):?>
<link rel="shortcut icon" href="<?php echo esc_url($pixhealth_options['pixhealth_favicon']) ?>" />
<?php elseif ( ! function_exists( 'has_site_icon' ) || ! has_site_icon() ) :
  wp_site_icon ();
    endif?>
<?php if ( is_singular() ) wp_enqueue_script( 'comment-reply' ); ?>
<?php wp_head();
 ?>

<link rel="stylesheet" type="text/css" href="../wp-content/themes/health/css/timeline.css">
<script type="text/javascript" src="../wp-content/themes/health/js/1.7.2.jquery.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/waypoints/2.0.5/waypoints.min.js" type="text/javascript"></script>
<script type="text/javascript" src="../wp-content/themes/health/js/timeline.js"></script>
<script type="text/javascript" src="../wp-content/themes/health/js/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="../wp-content/themes/health/js/my-timeline.js"></script>


</head>
<body  <?php body_class(); ?>   >




<?php 
  $pixhealth_header_type = isset($post->ID) && get_post_meta($post->ID, 'header_type', 1) != '' ? get_post_meta($post->ID, 'header_type', 1) : pixhealth_get_option('pixhealth_header_type','layout-header1');
  $pixhealth_header_sticky = pixhealth_get_option('pixhealth_header_sticky','sticky');
  $pixhealth_page_layout = isset($post->ID) && get_post_meta($post->ID, 'page_layout', 1) != '' ? get_post_meta($post->ID, 'page_layout', 1) : pixhealth_get_option('pixhealth_page_layout','layout-wide');
?>
<div class="<?php echo esc_attr($pixhealth_page_layout);?>-wrap">


<div data-header-top="700" data-header="<?php echo esc_attr($pixhealth_header_sticky);?>" class="layout-theme <?php echo esc_attr($pixhealth_header_type);?> <?php echo esc_attr($pixhealth_page_layout);?> animated-css">

<?php if ( is_object($pix_guest) && $pix_guest->user_login == 'user_customizer' ) : ?>
<?php if (pixhealth_get_option('pixhealth_color_switcher','0')) : ?>
<div class="demo_changer">
  <div class="demo-icon"> <i class="fa fa-cog fa-spin fa-2x"></i> </div>
  <!-- end opener icon -->
  <div class="form_holder">
    <h3 class="title-option">
      <?php _e('Theme Customization', 'PixTheme')?>
    </h3>
    <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="predefined_styles">
          <div class="color_box"> 
            <a target="_blank" href="<?php echo esc_url(get_site_url()); ?>/wp-admin/access_customizer.php" rel="color1" class="styleswitch" > </a> 
            <a target="_blank" href="<?php echo esc_url(get_site_url()); ?>/wp-admin/access_customizer.php" rel="color2" class="styleswitch" > </a> 
            <a target="_blank" href="<?php echo esc_url(get_site_url()); ?>/wp-admin/access_customizer.php" rel="color3" class="styleswitch" > </a> 
          </div>
          <div class="color_box"> 
            <a target="_blank" href="<?php echo esc_url(get_site_url()); ?>/wp-admin/access_customizer.php" rel="color4" class="styleswitch" > </a> 
            <a target="_blank" href="<?php echo esc_url(get_site_url()); ?>/wp-admin/access_customizer.php" rel="color5" class="styleswitch" > </a> 
            <a target="_blank" href="<?php echo esc_url(get_site_url()); ?>/wp-admin/access_customizer.php" rel="color6" class="styleswitch" > </a> 
          </div>
          <div class="color_box"> 
            <a target="_blank" href="<?php echo esc_url(get_site_url()); ?>/wp-admin/access_customizer.php" rel="color7" class="styleswitch" > </a> 
            <a target="_blank" href="<?php echo esc_url(get_site_url()); ?>/wp-admin/access_customizer.php" rel="color8" class="styleswitch" > </a>
            <a target="_blank" href="<?php echo esc_url(get_site_url()); ?>/wp-admin/access_customizer.php" rel="color9" class="styleswitch" > </a> 
          </div>
        </div>        
        <!-- end predefined_styles --> 
      </div>
      <!-- end col --> 
      
      <!-- end col --> 
    </div>
    <!-- end row --> 
  </div>
  <!-- end form_holder --> 
</div>
<?php endif?>
<?php endif?>
<?php 
  if( (pixhealth_get_option('pixhealth_loader','1') == 1 && is_front_page()) || pixhealth_get_option('pixhealth_loader','1') == 2){
?>
<div id="page-preloader"><span class="spinner"></span></div>
<?php 
  }
?>

<!-- Loader end -->

<div class="top-header">
  <div class="container">
    <div class="row">
      <div class="col-sm-5 text-left">
    <ul class="social-links">
      <?php if(pixhealth_get_option('pixhealth_facebook', '')){ ?>
            <li><a href="<?php echo esc_url($pixhealth_options['pixhealth_facebook']); ?>" target="_blank"><i class="social_icons fa fa-facebook-square"></i></a></li>
            <?php } ?>
            <?php if(pixhealth_get_option('pixhealth_vk', '')){ ?>
            <li><a href="<?php echo esc_url($pixhealth_options['pixhealth_vk']); ?>" target="_blank"><i class="social_icons fa fa-vk"></i></a></li>
            <?php } ?>
            <?php if(pixhealth_get_option('pixhealth_youtube', '')){ ?>
            <li><a href="<?php echo esc_url($pixhealth_options['pixhealth_youtube']); ?>" target="_blank"><i class="social_icons fa fa-youtube-square"></i></a></li>
            <?php } ?>
            <?php if(pixhealth_get_option('pixhealth_vimeo', '')){ ?>
            <li><a href="<?php echo esc_url($pixhealth_options['pixhealth_vimeo']); ?>" target="_blank"><i class="social_icons fa fa-vimeo-square"></i></a></li>
            <?php } ?>
            <?php if(pixhealth_get_option('pixhealth_twitter', '')){ ?>
            <li><a href="<?php echo esc_url($pixhealth_options['pixhealth_twitter']); ?>" target="_blank"><i class="social_icons fa fa-twitter-square"></i></a></li>
            <?php } ?>
            <?php if(pixhealth_get_option('pixhealth_google', '')){ ?>
            <li><a href="<?php echo esc_url($pixhealth_options['pixhealth_google']); ?>" target="_blank"><i class="social_icons fa fa-google-plus-square"></i></a></li>
            <?php } ?>
            <?php if(pixhealth_get_option('pixhealth_tumblr', '')){ ?>
            <li><a href="<?php echo esc_url($pixhealth_options['pixhealth_tumblr']); ?>" target="_blank"><i class="social_icons fa fa-tumblr-square"></i></a></li>
            <?php } ?>
            <?php if(pixhealth_get_option('pixhealth_instagram', '')){ ?>
            <li><a href="<?php echo esc_url($pixhealth_options['pixhealth_instagram']); ?>" target="_blank"><i class="social_icons fa fa-instagram"></i></a></li>
            <?php } ?>
            <?php if(pixhealth_get_option('pixhealth_pinterest', '')){ ?>
            <li><a href="<?php echo esc_url($pixhealth_options['pixhealth_pinterest']); ?>" target="_blank"><i class="social_icons fa fa-pinterest-square"></i></a></li>
            <?php } ?>
            <?php if(pixhealth_get_option('pixhealth_linkedin', '')){ ?>
            <li><a href="<?php echo esc_url($pixhealth_options['pixhealth_linkedin']); ?>" target="_blank"><i class="social_icons fa fa-linkedin-square"></i></a></li>
            <?php } ?>
            <?php if(pixhealth_get_option('pixhealth_custom1_link', '')){ ?>
            <li><a href="<?php echo esc_url($pixhealth_options['pixhealth_custom1_link']); ?>" target="_blank"><i class="social_icons fa <?php echo esc_attr($pixhealth_options['pixhealth_custom1_icon']); ?>"></i></a></li>
            <?php } ?>
            <?php if(pixhealth_get_option('pixhealth_custom2_link', '')){ ?>
            <li><a href="<?php echo esc_url($pixhealth_options['pixhealth_custom2_link']); ?>" target="_blank"><i class="social_icons fa <?php echo esc_attr($pixhealth_options['pixhealth_custom2_icon']); ?>"></i></a></li>
            <?php } ?>
            <?php if(pixhealth_get_option('pixhealth_custom3_link', '')){ ?>
            <li><a href="<?php echo esc_url($pixhealth_options['pixhealth_custom3_link']); ?>" target="_blank"><i class="social_icons fa <?php echo esc_attr($pixhealth_options['pixhealth_custom3_icon']); ?>"></i></a></li>
            <?php } ?>
    </ul>
      </div>
      <div class="top-header__links col-sm-7">
         <?php
                    wp_nav_menu(array( 
                        'theme_location' => 'top_nav',
                        'menu' =>'top_nav', 
                        'container'=>'', 
                        'depth' => 1, 
                        'menu_class' => 'nav pull-right btn-group-top-desctop'
                        ));
                    ?>
                   
    </div>
    </div>
  </div>
</div>


<div class="header">


  <div class="container">
    <div class="header-inner">
      <div class="row">
        <div class="col-md-4 col-xs-12">
          <a title="<?php echo esc_attr(pixhealth_get_option('pixhealth_logotext',''))?>" href="<?php echo esc_url(home_url('/')) ?>" id="logo"  class="logo">
          <?php if(!empty($pixhealth_options['pixhealth_logo'])):?>
          <img src="<?php echo esc_url($pixhealth_options['pixhealth_logo']) ?>" alt="<?php echo esc_attr(pixhealth_get_option('pixhealth_logotext',''))?>"  />
          <?php elseif ( get_header_image() ):?>
          <img  src="<?php header_image(); ?>" alt="<?php echo esc_attr(pixhealth_get_option('pixhealth_logotext',''))?>"  class="logo__img"  />
          <?php else:?>
          <img src="<?php echo get_template_directory_uri(); ?>/images/logo.jpg" alt="<?php echo esc_attr(pixhealth_get_option('pixhealth_logotext',''))?>"  class="logo__img"  />
          <div class="logo-desc"> <?php echo pixhealth_get_option('pixhealth_logotext','') ?></div>
          <?php endif?>
          </a> </div>
        <div class="col-md-8 col-xs-12">
          <div class="header-block"> 
          <?php if(pixhealth_get_option('pixhealth_header_phone','')) : ?>
            <span class="header-label"> 
              <i class="icon-header icon-call-in color_primary"></i> 
                <span class="helper"> <?php esc_html_e('Call Us', 'PixHealth') ?><?php echo wp_kses_post($pixhealth_options['pixhealth_header_phone'])?> </span>
            </span> 
          <?php endif; ?>
          <?php if(pixhealth_get_option('pixhealth_header_email','')) : ?>
            <span class="header-label"> 
              <i class="icon-header icon-envelope-open color_primary"></i> 
                <span class="helper"> <?php esc_html_e('Email Us', 'PixHealth') ?><?php echo wp_kses_post($pixhealth_options['pixhealth_header_email'])?> </span>
            </span> 
          <?php endif; ?>
          <?php if ( class_exists( 'WooCommerce' ) && pixhealth_get_option('pixhealth_header_minicart','1') ) : ?>
            <a href="<?php echo WC()->cart->get_cart_url(); ?>" class="top-cart"> 
              <i class="icon icon-basket bg-color_primary"></i> <?php _e('Cart Items:', 'PixHealth') ?> <?php echo WC()->cart->cart_contents_count; ?> <span class="top-cart__price color_second"><?php echo WC()->cart->get_cart_total(); ?></span>
            </a>
          <?php endif; ?>
          </div>
          <form method="get" id="search-global-mobile" class="hidden-md hidden-lg text-center">
            <input type="text" name="s" id="search-mobile" value="">
            <button type="submit"><i class="icon fa fa-search"></i></button>
          </form>
        </div>
      </div>
    </div>
    <!-- end header-inner--> 
  </div>
  <!-- end container-->
  
  <div class="top-nav ">
    <div class="container">
      <div class="row">
        <div class="col-md-12  col-xs-12">
          <div class="navbar yamm ">
            <div class="navbar-header hidden-md  hidden-lg  hidden-sm ">
              <button class="navbar-toggle" data-target="#navbar-collapse-1" data-toggle="collapse" type="button"><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button>
              <a class="navbar-brand" href="#"><?php _e('Menu', 'PixHealth') ?></a> </div>
            <div class="navbar-collapse collapse" id="navbar-collapse-1">
              
              <?php echo pixhealth_get_theme_generator('pixhealth_site_menu', 'nav navbar-nav'); ?>
                          
                                    
              <?php if ( pixhealth_get_option('pixhealth_header_search','1') ) : ?> 
                <form action="<?php echo esc_url(site_url()) ?>" method="get" id="search-global-menu">    
                    <input type="text"  name="s" id="search" value="<?php esc_attr(the_search_query()); ?>" />
                    <button type="submit"><i class="icon-magnifier"></i></button>
                </form>
              <?php endif; ?> 
                
              
              <?php if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('Menu sidebar')) : ?>
              <?php endif; ?>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!--end top-nav --> 
</div>


<?php if (!is_page_template('fullwidth.php')){?>

  <?php if ( class_exists( 'WooCommerce' ) && is_product_category() ) :
      $pixhealth_cat = $wp_query->get_queried_object();
        $pixhealth_thumbnail_id = get_woocommerce_term_meta( $pixhealth_cat->term_id, 'thumbnail_id', true );
      $pixhealth_image = wp_get_attachment_url( $pixhealth_thumbnail_id );
      $pixhealth_image = $pixhealth_image == '' ? (!empty($pixhealth_options['pixhealth_header_img']) ? $pixhealth_options['pixhealth_header_img'] : get_template_directory_uri().'/images/bg-default.png') : $pixhealth_image;
    ?>
  <div style="background-image:url(<?php echo esc_url($pixhealth_image) ?>);" class="ui-title-page bg_title bg_transparent">
  <?php
    elseif ( class_exists( 'WooCommerce' ) && is_product() && !empty($post->ID)) :
    
      $pixhealth_terms = get_the_terms( $post->ID, 'product_cat' );
      $pixhealth_image = '';
      if($pixhealth_terms)
      foreach ($pixhealth_terms as $pixhealth_term) {
        $pixhealth_thumbnail_id = get_woocommerce_term_meta( $pixhealth_term->term_id, 'thumbnail_id', true );
        $pixhealth_image = wp_get_attachment_url( $pixhealth_thumbnail_id );
        if($pixhealth_image != '')
          break;
      }     
      $pixhealth_image = $pixhealth_image == '' ? (!empty($pixhealth_options['pixhealth_header_img']) ? $pixhealth_options['pixhealth_header_img'] : get_template_directory_uri().'/images/bg-default.png') : $pixhealth_image;
    ?>
  <div style="background-image:url(<?php echo esc_url($pixhealth_image) ?>);" class="ui-title-page bg_title bg_transparent">
  <?php 
    else : 
       if(has_post_thumbnail()): 
        $pixhealth_post_thumbnail_id = get_post_thumbnail_id($post->ID);
        $pixhealth_post_thumbnail_url = wp_get_attachment_url( $pixhealth_post_thumbnail_id );?>
  <div style="background-image:url(<?php echo esc_url($pixhealth_post_thumbnail_url) ?>);"  class="ui-title-page bg_title bg_transparent">
  <?php   elseif(!empty($pixhealth_options['pixhealth_header_img'])): ?>
  <div style="background-image:url(<?php echo esc_url($pixhealth_options['pixhealth_header_img']) ?>);" class="ui-title-page bg_title bg_transparent">
  <?php else:?>
  <div style="background-image:url(<?php echo get_template_directory_uri() ?>/images/bg-default.png);"  class="ui-title-page bg_title bg_transparent">
  <?php endif;
  endif;?>
    
  <?php if(class_exists( 'RW_Meta_Box' ) && rwmb_meta( 'pix_video_header' ) != '') : ?>
  
      <div class="hero-module">
          <video autoplay loop class="fillWidth">
              <source src="<?php echo esc_url(wp_get_attachment_url(rwmb_meta( 'pix_video_header' ))) ?>" type="video/mp4" />
          </video>
      </div>
  
  <?php endif;?>    
 
    <div class="container">
      <div class="row">
        <div class="col-xs-12">
          <h1>
          
           <?php 
      $pixhealth_postpage_id = get_option('page_for_posts'); 
      $pixhealth_frontpage_id = get_option('page_on_front');
      $pixhealth_page_id = isset ($wp_query) ? $wp_query->get_queried_object_id() : '';
      if( get_query_var( 'taxonomy' ) == 'services_category' ){
        $pixhealth_term = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) );
        $pixhealth_t_id = $pixhealth_term->term_id;
        $pixhealth_term_meta = get_option("services_category_$pixhealth_t_id"); 
      }
      
    ?>
          <?php if(is_single() && ! is_attachment()): ?>
          <?php echo wp_kses_post(get_the_title()); ?>
          <?php elseif( class_exists( 'WooCommerce' ) && (is_shop() || is_product_category() || is_product_tag()) ): ?>
          <?php wp_kses_post(woocommerce_page_title()); ?>
          <?php elseif( get_query_var( 'taxonomy' ) == 'services_category' ): ?>
          <?php echo wp_kses_post($pixhealth_term_meta['pix_serv_title']); ?>
          <?php elseif( is_archive() ): ?>
          <?php echo wp_kses_post(get_the_title($pixhealth_postpage_id)); ?>
          <?php elseif( is_page_template( 'blog-template.php' ) ): ?>          
          <?php echo wp_kses_post(get_the_title($pixhealth_page_id));  ?>
          <?php elseif( $pixhealth_page_id == $pixhealth_frontpage_id ): ?>          
          <?php echo wp_kses_post(_e('Health Posts', 'PixHealth'));  ?>
          <?php elseif( is_search() ): ?>
          <?php echo wp_kses_post(get_search_query()); ?>
          <?php elseif( is_category() ): ?>
          <?php echo wp_kses_post(single_cat_title()); ?>
          <?php elseif( is_tag() ): ?>
          <?php echo wp_kses_post(single_tag_title()); ?>
          <?php elseif( isset($post->ID) && $post->ID > 0 ): ?>
          <?php echo wp_kses_post(get_the_title($pixhealth_page_id)); ?>
          <?php else: ?>
          <?php echo wp_kses_post(get_the_title()); ?>
          <?php endif; ?>
          
          </h1>
          
           <div class="ui-subtitle-page">
       <?php 
          if( get_query_var( 'taxonomy' ) == 'services_category' ){
          echo wp_kses_post($pixhealth_term_meta['pix_serv_add_title']);          
        }elseif(class_exists( 'RW_Meta_Box' )){
          echo wp_kses_post(rwmb_meta( 'add_title' )) ;
        }
      ?>
           </div>
           
        </div>
      </div>
    </div>
  </div>
  
  
  
<div class="border_btm">
    <div class="container">
      <div class="row">
        <div class="col-xs-12">
           <?php if ( class_exists( 'WooCommerce' ) && !is_page_template( 'fullwidth.php' )) woocommerce_breadcrumb(); ?>
        </div>
      </div>
    </div>
  </div>
  
  




<?php } ?>

<!--#content-->
<div class="content" id="content">

<!-- HEADER -->
<?php if (!is_page_template('under-construction.php')):?>
<?php endif; ?>
