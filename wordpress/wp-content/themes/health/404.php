<?php /*** The template for displaying 404 pages (not found) ***/ ?>

<?php get_header();?>



  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <section role="main" class="main-content page-404">
          <h1 class="notfound_title">
            <?php _e('Page not found', 'PixHealth')?>
          </h1>
          <p class="notfound_description">
            <?php _e('The page you are looking for seems to be missing.Go back, or return to yourcompany.com to choose a new direction.Please report any broken links to our team.', 'PixHealth')?>
          </p>
          <a class="button notfound_button" href="javascript: history.go(-1)">
          <?php _e('Return to previous page', 'PixHealth')?>
          </a> </section>
      </div>
    </div>
  </div>

<?php get_footer(); ?>
