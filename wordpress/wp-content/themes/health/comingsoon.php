<!DOCTYPE html>
<html >
  <head>
    <meta charset="UTF-8">
    <title>Coming soon</title>
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-2.2.4.js"></script>
    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    <link rel="stylesheet" type="text/css" href="css/style1.css">
  </head>
  <body>
    <header>
      <nav class="navbar">
        <div class="container">
          <!-- Brand and toggle get grouped for better mobile display -->
          <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand logo" href="#"><img src="img/logo.png" class="img-responsive"></a>
          </div>
          <!-- Collect the nav links, forms, and other content for toggling -->
          <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav navbar-right">
              
              <li><a href="#">Blog</a></li>
              
            </ul>
            </div><!-- /.navbar-collapse -->
            </div><!-- /.container-fluid -->
          </nav>
        </header>
        <div class="parallax-body">
          <div id="background" class="layer-0"></div>
          <!-- <div id="x-axis" class="axis"></div>
          <div id="y-axis" class="axis"></div> -->
          <div id="chat" class="chat-planet chat"></div>
          <div id="earth" class="planet earth"></div>
          <!--  <div id="chat" class="chat-planet layer-1"></div> -->
          <div id="coming-soon" class="coming-soon-planet order"></div>
          <div  id="hero-fly" class="hero-fly-planet hero-fly"></div>
          
        </div>
        <!--********************************************************************-->
        <!-- <div class="container">
          <div class="row">
            <div class="col-xs-hidden col-sm-hidden col-md-12 col-lg-12">
              <div class="banner_image">
                <img src="home_img.jpg" class="img-responsive">
              </div>end banner_image
            </div>end col
          </div>end row
        </div> -->
        <div class="container">
          <div class="row">
            
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
              <div class="banner_coming_soon">
                <div class="banner_coming_soon_content">
                  <p>We are in a development phase and will be up and running very soon. You can checkout our Blog, meanwhile signup for our newsletter (no spam guaranteed) to get notified as soon as we go live. </p>
                </div>
                <form>
                  <div class="form-group">
                    <input type="email" class="form-control input_email" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email">
                    
                  </div>
                  <button type="submit" class="btn btnmod">Subscribe</button>
                </form>
                
              </div>
              </div><!-- end col -->
              
              
            </div><!-- end row banner -- >
          </div>
          
          <!-- ############################################## -->
          <script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
          <script src='http://cdnjs.cloudflare.com/ajax/libs/gsap/1.11.7/TweenMax.min.js'></script>
          <script src="js/index.js"></script>
          
          
        </body>
      </html>