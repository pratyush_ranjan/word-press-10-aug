<?php
/**
 * The Template for displaying all single posts.
 */
$pixhealth_custom =  get_post_custom($post->ID);
$pixhealth_layout = isset ($pixhealth_custom['pixhealth_page_layout']) ? $pixhealth_custom['pixhealth_page_layout'][0] : '2';
$pixhealth_sidebar = isset ($pixhealth_custom['pixhealth_selected_sidebar'][0]) ? $pixhealth_custom['pixhealth_selected_sidebar'][0] : 'global-sidebar-1';
$pixhealth_options = get_option('pixhealth_general_settings');
?>
<?php get_header();?>

<main class="section" id="main">
  <div class="container">
    <div class="row">
      <?php if ($pixhealth_layout == '3'): require_once(get_template_directory() .'/template-parts/sidebar.php'); endif?>
      <div class="col-xs-12 <?php if ($pixhealth_layout == '1'):?>  col-sm-12 col-md-12 <?php else: ?> col-sm-12 col-md-9 <?php endif?>">
        <section role="main" class="main-content contentsingle-post ">
          <?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
          <article id="post-<?php esc_attr(the_ID());?>"<?php post_class('post format-image clearfix'); ?>>
            <?php			
                    $pixhealth_format  = get_post_format();
                    $pixhealth_format = !in_array($pixhealth_format, array("quote", "gallery", "video")) ? 'standared' : $pixhealth_format;
					$pixhealth_icon = array("standared" => "icon-picture", "quote" => "fa fa-pencil", "gallery" => "icon-camera", "video" => "fa fa-video-camera");
                    get_template_part('template-parts/post-format-single/blog', $pixhealth_format);
                ?>
                
           	<div class="entry-main"> 
            
             <?php if($pixhealth_options['pixhealth_blog_show_date']): ?>
             
             <div class="box-date-post"> <span class="date-1"><?php echo get_the_time('j'); ?></span> <span class="date-2">  <?php echo get_the_time('M'); ?></span> </div>
             
  		                     
  <?php endif?>  
              
                <div class="entry-meta clearfix">
                  
                  <div class="meta">
                   <span class="meta-i"> <?php _e("By", "PixHealth") ?> <?php the_author_posts_link(); ?> </span>  <span class="divider-bog">/</span>
				  <?php if ( 'post' == get_post_type() ) : // Hide category and tag text for pages on Search ?>                  
                  <?php 
				  	if(!empty($pixhealth_options['pixhealth_blog_show_category']) && $pixhealth_options['pixhealth_blog_show_category'] == 1){
						$pixhealth_categories = get_the_category($post->ID);
                        if($pixhealth_categories){
                            $pixhealth_output = '<span class="meta-i">'.__('In: ', 'PixHealth');						
                            foreach($pixhealth_categories as $pixhealth_category) {
                                $pixhealth_output .= '<a href="'.esc_url(get_category_link( $pixhealth_category->term_id )).'" >'.wp_kses_post($pixhealth_category->cat_name).'</a> ';
                            }
                            $pixhealth_output .= '</span>';
                            echo wp_kses_post($pixhealth_output);						
                        }
					}
					if(!empty($pixhealth_options['pixhealth_blog_show_tag']) && $pixhealth_options['pixhealth_blog_show_tag'] == 1){	
						$pixhealth_posttags = get_the_tags($post->ID);
                        if ($pixhealth_posttags) {
                            $pixhealth_output = '<span class="meta-i">'.__('Tags: ', 'PixHealth');
                            foreach($pixhealth_posttags as $pixhealth_tag) {
                                $pixhealth_output .= '<a href="'.esc_url(get_tag_link( $pixhealth_tag->term_id )).'" >'.wp_kses_post($pixhealth_tag->name).'</a> ';
                            }
                            $pixhealth_output .= '</span>';
                            echo wp_kses_post($pixhealth_output);
                        }
					}
                  ?>
                  
                  <?php endif; // End if 'post' == get_post_type()?>
                    <?php if( 'open' == $post->comment_status && $pixhealth_options['pixhealth_blog_show_comments']) : ?>      	
                   <span class="divider-bog">/</span> <span class="meta-i">              
                        <?php _e( 'Comments: ', 'PixHealth' ); ?><?php comments_popup_link( __( '0', 'PixHealth' ), __( '1', 'PixHealth' ), __( '%', 'PixHealth' )); ?>              
                    </span>
                    <?php endif?>
                  </div>
                  
                  <?php wp_link_pages();?>
                </div>
              
                <h3 class="entry-title">
                  <?php the_title(); ?>
                </h3>
                
                <div class="entry-content">
                  <?php the_content(); ?>
                </div>
                
          <?php if(!empty($pixhealth_options['pixhealth_blog_share']) && $pixhealth_options['pixhealth_blog_share'] == 1) echo do_shortcode('[share]'); ?>
            
              </div>
            
            
          </article>
        </section>
    <?php if(!empty($pixhealth_options['pixhealth_blog_show_author']) && $pixhealth_options['pixhealth_blog_show_author'] == 1) : ?>
        <?php 
			$pixhealth_get_avatar = get_avatar(get_the_author_meta('user_email'), 85);
			preg_match("/src='(.*?)'/i", $pixhealth_get_avatar, $pixhealth_matches);
			$pixhealth_src = !empty($pixhealth_matches[1]) ? $pixhealth_matches[1] : '';
		?>
        <section class="about-autor ">
        
        <h3 class="widget-title"><span> <?php _e('About Author', 'PixHealth')?></span></h3>

          <article class="comment img">
            <div class="avatar-placeholder ">
            <img src="<?php echo esc_url($pixhealth_src) ?>)"  alt="avatar"/> </div>
            <header class="comment-header"> <cite class="comment-author">
              <?php the_author_posts_link(); ?>
              </cite>
              <time class="comment-datetime" datetime="2012-10-27"><span class="icon-clock" aria-hidden="true"></span> <?php echo date_i18n( get_option('date_format'), strtotime( get_the_author_meta( 'user_registered') ) ); ?> </time>
            </header>
            <div class="comment-body">
              <?php the_author_meta( 'description'); ?>
            </div>
          </article>
        </section>
    <?php endif; ?>
    <?php if(!empty($pixhealth_options['pixhealth_blog_show_author_posts']) && $pixhealth_options['pixhealth_blog_show_author_posts'] == 1) :?>
        <?php
  	$args = array(
		'author'        =>  get_the_author_meta( 'ID'), 
		'orderby'       =>  'post_date',
		'order'         =>  'ASC',
		'posts_per_page'=> 4 
    );
	$pixhealth_author_posts = get_posts( $args );

	if(!empty($pixhealth_author_posts) && count($pixhealth_author_posts) > 2) :
  ?>
        <section class="about-autor ">
        
           <h3 class="widget-title"><span> <?php _e('author posts  ', 'PixHealth') ?></span></h3>
           

          <div class="padding25">
            <div class="row">
              <?php foreach($pixhealth_author_posts as $pixhealth_apost){ ?>
              <?php $pixhealth_tumbnail = get_the_post_thumbnail( $pixhealth_apost->ID ) != '' ? get_the_post_thumbnail( $pixhealth_apost->ID ) : '<img src="'.esc_url(get_template_directory_uri()).'/images/noimage.jpg">'; ?>
              <div class="  col-lg-3 col-md-3  col-sm-6 col-xs-6   ">
                <div class="box-simple-image">                  
                 <a href="<?php echo esc_url(get_permalink( $pixhealth_apost->ID )); ?>"> <?php echo wp_kses_post($pixhealth_tumbnail); ?>  </a></div>
              </div>
              <?php } ?>
            </div>
          </div>
        </section>
        <?php endif; ?>
    <?php endif; ?>
    <?php if(!empty($pixhealth_options['pixhealth_blog_show_comments']) && $pixhealth_options['pixhealth_blog_show_comments'] == 1) : ?>
        <div class="section-comment ">
          <?php comments_template(); ?>
          <?php $test = false; if ($test) {comment_form(); } ?>
        </div>
    <?php endif; ?>
        <?php endwhile; ?>
      </div>
      <?php if ($pixhealth_layout == '2'): require_once(get_template_directory() .'/template-parts/sidebar.php'); endif?>
    </div>
  </div>
</main>
<?php get_footer();?>
