<?php /* Template Name: Blog Template*/ 

//$custom =  get_post_custom(get_queried_object()->ID);

$pixhealth_custom = isset ($wp_query) ? get_post_custom($wp_query->get_queried_object_id()) : '';
$pixhealth_layout = isset ($pixhealth_custom['pixhealth_page_layout']) ? $pixhealth_custom['pixhealth_page_layout'][0] : '2';
$pixhealth_sidebar = isset ($pixhealth_custom['pixhealth_selected_sidebar'][0]) ? $pixhealth_custom['pixhealth_selected_sidebar'][0] : 'global-sidebar-1';
$pixhealth_options = get_option('pixhealth_general_settings');
?>

<?php get_header();?>

<section id="pageContentBox" class="pageRow blogPage">
    <div class="wrapper">
        <div class="row">					
    
		  	<?php if ($pixhealth_layout == '3'): require_once(get_template_directory() .'/template-parts/sidebar.php'); endif?>
                
         	<div class="col-xs-12 <?php if ($pixhealth_layout == '1'):?>  col-sm-12 col-md-12 <?php else: ?> col-sm-12 col-md-9 <?php endif?>">            
				<?php 
                    $wp_query = new WP_Query();
                    $pixhealth_pp = get_option('posts_per_page');
                    $wp_query->query('posts_per_page='.$pixhealth_pp.'&paged='.$paged);			
                    get_template_part( 'loop', 'index' );
                ?> 
        	</div>
          
            <?php if ($pixhealth_layout == '2'): require_once(get_template_directory() .'/template-parts/sidebar.php'); endif?>
      
		</div>
	</div>
</section>
    
<?php get_footer(); ?>