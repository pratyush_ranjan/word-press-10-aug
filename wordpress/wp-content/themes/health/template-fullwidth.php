<?php 
/**
 * The template for displaying page blocks.
 *
 * @package PixHealth
 * @since 1.0
 *
 * Template Name: Section (Parent Home)
 */
get_header(); ?>
<main id="main" class="section">
	<div class="container">
		<div class="row">
			<?php
         		if ( have_posts() ) : the_post();
            	the_content();
				echo "";
               wp_link_pages(array('before' => 'Pages: ', 'next_or_number' => 'number'));
            	$pixhealth_comment = get_option('pixhealth_general');
            	if ($pixhealth_comment['comment_page'] == 'on' ) { comments_template( '', true ); } // page comment
         		endif;
      		?>
		</div>
	</div>
</main>
<?php get_footer(); ?>