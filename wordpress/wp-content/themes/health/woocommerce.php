<?php 
/* Woocommerce template. */ 
if( is_shop() || is_product_category() || is_product_tag() ) {
    $pixhealth_id = get_option( 'woocommerce_shop_page_id' );
}elseif( is_product() || !empty($post->ID) )
	$pixhealth_id = $post->ID;
else
	$pixhealth_id = 0;

$pixhealth_custom = $pixhealth_id > 0 ? get_post_custom($pixhealth_id) : '';
if(isset($pixhealth_custom) && $pixhealth_custom != ''){
	$pixhealth_layout = isset ($pixhealth_custom['pixhealth_page_layout']) ? $pixhealth_custom['pixhealth_page_layout'][0] : '2';
	$pixhealth_sidebar = isset ($pixhealth_custom['pixhealth_selected_sidebar'][0]) ? $pixhealth_custom['pixhealth_selected_sidebar'][0] : 'shop-sidebar-1';
}
$pixhealth_options = get_option('pixhealth_general_settings');

?>


<?php get_header();?>

<div class="container">
    <div class="row">
		
		<?php if (isset($pixhealth_layout) && $pixhealth_layout == '3'): require_once(get_template_directory() .'/template-parts/sidebar.php'); endif?>
		
        <div class="col-xs-12 <?php if (!isset($pixhealth_layout) || $pixhealth_layout == '1'):?>  col-sm-12 col-md-12 <?php else: ?> col-sm-12 col-md-9 <?php endif?>">
			<main class="main-content">       
 			<?php  woocommerce_content(); ?>
 			</main>
 		</div>
        
        <?php if (isset($pixhealth_layout) && $pixhealth_layout == '2'): require_once(get_template_directory() .'/template-parts/sidebar.php'); endif?>
        
    </div>
</div>
            
<?php get_footer();?>
