<?php
/*** The template for displaying search results pages. ***/
if( get_post() )
	$pixhealth_custom =  get_post_custom($post->ID);
$pixhealth_layout = isset ($pixhealth_custom['pixhealth_page_layout']) ? $pixhealth_custom['pixhealth_page_layout'][0] : '2';
$pixhealth_sidebar = isset ($pixhealth_custom['pixhealth_selected_sidebar'][0]) ? $pixhealth_custom['pixhealth_selected_sidebar'][0] : 'global-sidebar-1';
$pixhealth_options = get_option('pixhealth_general_settings');
?>

<?php get_header();?>

<main class="section" id="main">
  <div class="container">
    <div class="row"> 
    
     <?php if ($pixhealth_layout == '3'): require_once(get_template_directory() .'/template-parts/sidebar.php'); endif?>
      
       <div class="col-xs-12 col-sm-7 col-md-9  <?php if ($pixhealth_layout == '3'):?>  col2-left  <?php endif?>   <?php if ($pixhealth_layout == '2'):?>  col2-right  <?php endif?>">  
        <section role="main" class="main-content">
        
           <?php if ( have_posts() ) : ?>
       
                <?php get_template_part( 'loop', 'search' );?>
        
            <?php else : ?>
                <div id="post-0" class="post no-results not-found">
                    <h1><?php _e( 'Nothing Found', 'PixHealth' ); ?></h1>
                    <div class="entry-content">
                        <p><?php _e( 'Sorry, but nothing matched your search criteria. Please try again with some different keywords.', 'PixHealth' ); ?></p>
                     </div><!-- .entry-content -->
                </div><!-- #post-0 -->
            <?php endif; ?>
        
        </section></div>
        
     <?php if ($pixhealth_layout == '2'): require_once(get_template_directory() .'/template-parts/sidebar.php'); endif?>
    
    </div>
    </div>
    </main>
    

<?php get_footer(); ?>
			
            
            