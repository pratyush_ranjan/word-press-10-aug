 <?php /* Template Name: Single Service */ 

$pixhealth_custom =  get_post_custom($post->ID);
$pixhealth_layout = isset ($pixhealth_custom['pixhealth_page_layout']) ? $pixhealth_custom['pixhealth_page_layout'][0] : '2';
$pixhealth_sidebar = isset ($pixhealth_custom['pixhealth_selected_sidebar'][0]) ? $pixhealth_custom['pixhealth_selected_sidebar'][0] : 'global-sidebar-1';
$pixhealth_options = get_option('pixhealth_general_settings');
?>
<?php get_header();?>

<main class="main-content">

      <div class="container">
      
		<?php if ($pixhealth_layout == '3'): require_once(get_template_directory() .'/template-parts/sidebar.php'); endif?>
        
   	<div class="col-xs-12 <?php if ($pixhealth_layout == '1'):?>  col-sm-12 col-md-12 <?php else: ?> col-sm-12 col-md-9 <?php endif?>">    
        
        
        
        
        	<?php if ( have_posts() ) while ( have_posts() ) : the_post(); 
					
					$pixhealth_thumbnail = get_the_post_thumbnail($post->ID);
			
			?>
            	

                    <div class="services-details__foto text-center">
                    	<div class="hover__figure"> <?php echo wp_kses_post($pixhealth_thumbnail) ?> </div>
                    <?php if ( get_post_meta( get_the_ID(), 'service_icon', true ) !== '' ) : ?>
                        <span class="icon-round helper">
                            <i class="icon <?php echo esc_attr(get_post_meta( get_the_ID(), 'service_icon', true )) ?>"></i>
                        </span> 
                    <?php endif; ?>
                    </div>
                    <div class="service-content">
                    	<?php the_content(); ?>
                    </div>
          
                
        <?php endwhile; ?>
      </div>
      <?php if ($pixhealth_layout == '2'): require_once(get_template_directory() .'/template-parts/sidebar.php'); endif?>

  </div>
</section>
<?php get_footer();?>
