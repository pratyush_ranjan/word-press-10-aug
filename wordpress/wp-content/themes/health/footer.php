  </div>
  <!-- end content--> 
  
  
  <?php 
/*** The template for displaying the footer ***/

	$pixhealth_options = isset($_POST['options']) ? $_POST['options'] : get_option('pixhealth_general_settings');
	$pixhealth_footerBlockId = pixhealth_get_option('pixhealth_footer_staticblock');
	if(function_exists('icl_object_id')) {
		$pixhealth_footerBlockId = icl_object_id ($pixhealth_footerBlockId,'staticblocks',true);
	}
	
?>

<footer class="footer">

<?php if ($pixhealth_footerBlockId):?>


<div class="footer__inner">

  <div class="container">
    <div class="row">
      <?php
			$pixhealth_post = get_post($pixhealth_footerBlockId);
			$pixhealth_item_output = do_shortcode($pixhealth_post->post_content);
			echo $pixhealth_item_output;
		?>
    </div>
  </div>
  </div>

<?php endif; ?>


  <?php
    wp_footer();
?>




<div class="footer__menu clearfix">
  <div class="container"> 
  <?php if(pixhealth_get_option('pixhealth_footer_logo', '')):?>
  <a  href="<?php echo esc_url(home_url('/')) ?>"> 
	<img class="logo__img logo pull-left" width="270" height="44" alt="Logo" src="<?php echo esc_url($pixhealth_options['pixhealth_footer_logo']) ?>" >
  </a>
  <?php endif; ?>
 <?php
	wp_nav_menu(array( 
		'theme_location' => 'footer_nav',
		'menu' =>'footer_nav', 
		'container'=>'', 
		'depth' => 1, 
		'menu_class' => 'pull-right'
		));
  ?>

</div></div>  </div>


<div class="footer__bottom"> 
  <span class="copyright">
  <?php if (!empty($pixhealth_options['pixhealth_footer_copy'])) { echo wp_kses_post($pixhealth_options['pixhealth_footer_copy']); }?>
  </span>
  <ul class="social-links">
	<?php if(pixhealth_get_option('pixhealth_facebook', '')){ ?>
    <li><a href="<?php echo esc_url($pixhealth_options['pixhealth_facebook']); ?>" target="_blank"><i class="social_icons fa fa-facebook-square"></i></a></li>
    <?php } ?>
    <?php if(pixhealth_get_option('pixhealth_vk', '')){ ?>
    <li><a href="<?php echo esc_url($pixhealth_options['pixhealth_vk']); ?>" target="_blank"><i class="social_icons fa fa-vk"></i></a></li>
    <?php } ?>
    <?php if(pixhealth_get_option('pixhealth_youtube', '')){ ?>
    <li><a href="<?php echo esc_url($pixhealth_options['pixhealth_youtube']); ?>" target="_blank"><i class="social_icons fa fa-youtube-square"></i></a></li>
    <?php } ?>
    <?php if(pixhealth_get_option('pixhealth_vimeo', '')){ ?>
    <li><a href="<?php echo esc_url($pixhealth_options['pixhealth_vimeo']); ?>" target="_blank"><i class="social_icons fa fa-vimeo-square"></i></a></li>
    <?php } ?>
    <?php if(pixhealth_get_option('pixhealth_twitter', '')){ ?>
    <li><a href="<?php echo esc_url($pixhealth_options['pixhealth_twitter']); ?>" target="_blank"><i class="social_icons fa fa-twitter-square"></i></a></li>
    <?php } ?>
    <?php if(pixhealth_get_option('pixhealth_google', '')){ ?>
    <li><a href="<?php echo esc_url($pixhealth_options['pixhealth_google']); ?>" target="_blank"><i class="social_icons fa fa-google-plus-square"></i></a></li>
    <?php } ?>
    <?php if(pixhealth_get_option('pixhealth_tumblr', '')){ ?>
    <li><a href="<?php echo esc_url($pixhealth_options['pixhealth_tumblr']); ?>" target="_blank"><i class="social_icons fa fa-tumblr-square"></i></a></li>
    <?php } ?>
    <?php if(pixhealth_get_option('pixhealth_instagram', '')){ ?>
    <li><a href="<?php echo esc_url($pixhealth_options['pixhealth_instagram']); ?>" target="_blank"><i class="social_icons fa fa-instagram"></i></a></li>
    <?php } ?>
    <?php if(pixhealth_get_option('pixhealth_pinterest', '')){ ?>
    <li><a href="<?php echo esc_url($pixhealth_options['pixhealth_pinterest']); ?>" target="_blank"><i class="social_icons fa fa-pinterest-square"></i></a></li>
    <?php } ?>
    <?php if(pixhealth_get_option('pixhealth_linkedin', '')){ ?>
    <li><a href="<?php echo esc_url($pixhealth_options['pixhealth_linkedin']); ?>" target="_blank"><i class="social_icons fa fa-linkedin-square"></i></a></li>
    <?php } ?>
    <?php if(pixhealth_get_option('pixhealth_custom1_link', '')){ ?>
    <li><a href="<?php echo esc_url($pixhealth_options['pixhealth_custom1_link']); ?>" target="_blank"><i class="social_icons fa <?php echo esc_attr($pixhealth_options['pixhealth_custom1_icon']); ?>"></i></a></li>
    <?php } ?>
    <?php if(pixhealth_get_option('pixhealth_custom2_link', '')){ ?>
    <li><a href="<?php echo esc_url($pixhealth_options['pixhealth_custom2_link']); ?>" target="_blank"><i class="social_icons fa <?php echo esc_attr($pixhealth_options['pixhealth_custom2_icon']); ?>"></i></a></li>
    <?php } ?>
    <?php if(pixhealth_get_option('pixhealth_custom3_link', '')){ ?>
    <li><a href="<?php echo esc_url($pixhealth_options['pixhealth_custom3_link']); ?>" target="_blank"><i class="social_icons fa <?php echo esc_attr($pixhealth_options['pixhealth_custom3_icon']); ?>"></i></a></li>
    <?php } ?>
  </ul>
</div>
</footer>


  </div>
  <!-- end container --> 


</body></html>
