<?php header("Content-type: text/css; charset: UTF-8"); 
global $woocommerce;
$parse_uri = explode( 'wp-content', $_SERVER['SCRIPT_FILENAME'] );
require_once( $parse_uri[0] . 'wp-load.php' );
$pixhealth_options = get_option('pixhealth_general_settings');
$pixhealth_customize = get_option( 'pixhealth_customize_options' );

$pixhealth_customize_live_preview = array();
if(class_exists('WP_Session')) {
    $pixhealth_wp_session = WP_Session::get_instance();
    $pixhealth_customize_live_preview = array();
    foreach($pixhealth_customize as $key => $val){
        if(isset($pixhealth_wp_session['customize_live_preview'][$key]))
            $pixhealth_customize_live_preview[$key] = $pixhealth_wp_session['customize_live_preview'][$key];
    }
}else {
    $pixhealth_customize_live_preview = get_option( 'pixhealth_customize_live_preview' );
    update_option('pixhealth_customize_live_preview', '');
}
$pixhealth_customize = !is_array($pixhealth_customize_live_preview) ? $pixhealth_customize : array_merge($pixhealth_customize, $pixhealth_customize_live_preview);

?>



  <?php if($pixhealth_customize['first_color'] != ''):?>
  
html   a {
    color: <?php echo esc_attr($pixhealth_customize['first_color'])?>;
    
    }
  
html  .nav-tabs > li.active > a, html   .nav-tabs > li.active > a:hover, html   .nav-tabs > li.active > a:focus {
    background-color: <?php echo esc_attr($pixhealth_customize['first_color'])?>;
    }
    
    html .nav > li > a:hover, .nav > li > a:focus {
    background-color: #fff;
   color: <?php echo esc_attr($pixhealth_customize['first_color'])?>;
}





.color_second {
	color: <?php echo esc_attr($pixhealth_customize['first_color'])?>;
}

html .product-bottom .added_to_cart{

   background-color:  <?php echo esc_attr($pixhealth_customize['first_color'])?> !important;
}



.bg-color_second {
    background-color:  <?php echo esc_attr($pixhealth_customize['first_color'])?>;
}


html .ui-accordion-header-active, .ui-tabs-active {
   background-color:  <?php echo esc_attr($pixhealth_customize['first_color'])?> !important;
}


.slider-reviews .quote::before {
color:  <?php echo esc_attr($pixhealth_customize['first_color'])?> !important;
    }
    
    
    html .dropdown-menu > li > a:hover,html  .dropdown-menu > li > a:focus {
    background-color:<?php echo esc_attr($pixhealth_customize['first_color'])?> ;
    }
    
    
    html .yamm .nav > li:hover > a {
    border-top: 3px solid ?php echo esc_attr($pixhealth_customize['first_color'])?> !important;
}
    
    
    html .slider_team .slide:hover .btn {
 background-color:  <?php echo esc_attr($pixhealth_customize['first_color'])?> ;
}


html .product-grid .product-bottom .btn-group .btn {
    background-color: <?php echo esc_attr($pixhealth_customize['first_color'])?> ;
    }
    
    
    html .icon-round {
    background-color: <?php echo esc_attr($pixhealth_customize['first_color'])?> ;
    }
    
    
    html .post .social-links a:hover {
    border-color: <?php echo esc_attr($pixhealth_customize['first_color'])?> ;
}

    <!--END SECOND COLOR  ORANGE-->
    

  <?php endif?> 
  
  
    <?php if($pixhealth_customize['second_color'] != ''):?>
    
    html .woocommerce div.product p.price, .woocommerce div.product span.price {
    color: <?php echo esc_attr($pixhealth_customize['first_color'])?> ;
    }
    
    
    html .list-contacts .icon {
    background-color:<?php echo esc_attr($pixhealth_customize['first_color'])?> ;
    }
    
    html .widget-title {
    color: <?php echo esc_attr($pixhealth_customize['first_color'])?> ;;
    }
    
    html .services__item:hover .icon-round {
    background-color: <?php echo esc_attr($pixhealth_customize['first_color'])?> ;
}
    
    
   html  .services__item:hover .ui-title-inner {
   color: <?php echo esc_attr($pixhealth_customize['first_color'])?> ;
}


html .services__item:hover .btn {
    background-color: <?php echo esc_attr($pixhealth_customize['first_color'])?> ;
}
    
    .woocommerce nav.woocommerce-pagination ul li a:focus, .woocommerce nav.woocommerce-pagination ul li a:hover, .woocommerce nav.woocommerce-pagination ul li span.current {
    background-color: <?php echo esc_attr($pixhealth_customize['first_color'])?> !important ;
}
    
    
    html .btn-primary {
    background-color: <?php echo esc_attr($pixhealth_customize['first_color'])?> ;
    border-color: <?php echo esc_attr($pixhealth_customize['first_color'])?> ;
    }
    
    
    html .woocommerce #respond input#submit,htmk  .woocommerce a.button, html .woocommerce button.button,html  .woocommerce input.button {
    background-color: <?php echo esc_attr($pixhealth_customize['first_color'])?> ;
    border: 0 none <?php echo esc_attr($pixhealth_customize['first_color'])?> ;
    }
	
html #filter li a.current::before, html  #filter li a:hover::before {
    border-top: 10px solid  <?php echo esc_attr($pixhealth_customize['second_color'])?>;
}
html .yamm .nav > li:hover > a {
    border-top: 3px solid <?php echo esc_attr($pixhealth_customize['second_color'])?>;
    }
    

html #filter li a:hover, #filter li a.current {
    box-shadow: 0 3px 0 0 <?php echo esc_attr($pixhealth_customize['second_color'])?>;
    }
    
    
    html .nav-tabs > li.active > a::before {
    border-top: 10px solid <?php echo esc_attr($pixhealth_customize['second_color'])?>;
}


html .nav-tabs > li.active > a, .nav-tabs > li.active > a:hover, .nav-tabs > li.active > a:focus {
    box-shadow: 0 3px 0 0 <?php echo esc_attr($pixhealth_customize['second_color'])?>;
    }
    
    
    html .list-services .list-services__item:hover .icon-round_grey {
    background-color: <?php echo esc_attr($pixhealth_customize['second_color'])?>;
}


    html   .list-services_vert .list-services__item:hover .list-services__title {
 color: <?php echo esc_attr($pixhealth_customize['second_color'])?>;
}
    
html  #filter li a.current::before, html  #filter li a:hover::before {
    border-top: 10px solid <?php echo esc_attr($pixhealth_customize['second_color'])?>;
}

html .btn-link,html  .view-post-btn {
  color: <?php echo esc_attr($pixhealth_customize['second_color'])?> !important;
}


html .bg-color_primary{
    background-color: <?php echo esc_attr($pixhealth_customize['second_color'])?> ;
}


html .slider_team .category {
color: <?php echo esc_attr($pixhealth_customize['second_color'])?> ;
    }


html .color_primary{
color: <?php echo esc_attr($pixhealth_customize['second_color'])?> ;
}
.bx-wrapper .bx-pager.bx-default-pager a:hover, .bx-wrapper .bx-pager.bx-default-pager a.active {
 background-color:  <?php echo esc_attr($pixhealth_customize['first_color'])?> !important;
}

.icon_box_wrap .fa::before {
color: <?php echo esc_attr($pixhealth_customize['second_color'])?>  !important;
    }
    
    
html  .btn_small:hover {
    background-color: <?php echo esc_attr($pixhealth_customize['second_color'])?> ;
}
    

html  .advantages__inner:hover .icon {
color: <?php echo esc_attr($pixhealth_customize['second_color'])?> ;
}


html .advantages__inner:hover .btn {
   background-color: <?php echo esc_attr($pixhealth_customize['second_color'])?> ;
}

html   .list-icons .icon-round:hover {
   background-color: <?php echo esc_attr($pixhealth_customize['second_color'])?> ;
}



html   .list-mark li::before {
 color: <?php echo esc_attr($pixhealth_customize['second_color'])?> ;
    }
    
 html    input[type="submit"] {
    background-color: <?php echo esc_attr($pixhealth_customize['second_color'])?> ;
    }
    
    
    html  .departments-item:hover .icon-round {
    background-color: <?php echo esc_attr($pixhealth_customize['second_color'])?> ;

}

html .departments-item:hover .btn {
    background-color: <?php echo esc_attr($pixhealth_customize['second_color'])?> ;

}


html .departments-item:hover .ui-title-inner {
 color: <?php echo esc_attr($pixhealth_customize['second_color'])?> ;
}


html .price-box span {
color: <?php echo esc_attr($pixhealth_customize['second_color'])?> ;
    }
    
    
    html .about-autor {
    border-bottom: 4px solid  <?php echo esc_attr($pixhealth_customize['second_color'])?> ;
    }
    
    
    
    html  .input-group .icon {
    color: <?php echo esc_attr($pixhealth_customize['second_color'])?> ;
    }
    
    
    html  .post .social-links a:hover .icon {
  color: <?php echo esc_attr($pixhealth_customize['second_color'])?> ;
}


  <?php endif?> 
  
  
  
  
    <?php if($pixhealth_customize['third_color'] != ''):?>
    
        html .info-post .comments a {
    background-color:  <?php echo esc_attr($pixhealth_customize['third_color'])?>;
    }

    
    
    html .entry-meta {
    background-color:  <?php echo esc_attr($pixhealth_customize['third_color'])?>;
}
    
    
    html .page-numbers a.prev, .page-numbers a.next {
    background-color: <?php echo esc_attr($pixhealth_customize['third_color'])?>;
}
    
    
 html    .toolbar-wrap {
    background: <?php echo esc_attr($pixhealth_customize['third_color'])?>;
    
    }
    
    
   html  .slider_team .slide:hover {
   background-color: <?php echo esc_attr($pixhealth_customize['third_color'])?> ;
    border-bottom-color: <?php echo esc_attr($pixhealth_customize['third_color'])?>;
}
    
     
html  .wpb_content_element .wpb_accordion_wrapper .wpb_accordion_header, html  .wpb_content_element.wpb_tabs .wpb_tour_tabs_wrapper .wpb_tab {
    background-color: <?php echo esc_attr($pixhealth_customize['third_color'])?> ;
}

html  .top-header{
    background-color: <?php echo esc_attr($pixhealth_customize['third_color'])?> ;
     border-top: 5px solid <?php echo esc_attr($pixhealth_customize['third_color'])?> ;
}


html .departments-item:hover {
background-color: <?php echo esc_attr($pixhealth_customize['third_color'])?> ;
}
  
   <?php endif?>
   
  
  <?php if($pixhealth_customize['font_family'] != ''):?>
  
html .ui-text {
    font-family: '<?php echo esc_attr($pixhealth_customize['font_family'])?>';
    font-weight: <?php echo esc_attr($pixhealth_customize['font_weight'])?>;    
}
    <?php endif?>
    
    
    
    <?php if($pixhealth_customize['font_title_family'] != ''):?>
html h1, html  h2, html  h3,  html  h4, html  h5, html  h6 , html .footer__title , html .ui-title-inner{
    font-family: '<?php echo esc_attr($pixhealth_customize['font_title_family'])?>';
    font-weight:<?php echo esc_attr($pixhealth_customize['font_title_weight'])?>;
}
html .ui-title-block , html .ui-title-block *{

 font-family: '<?php echo esc_attr($pixhealth_customize['font_title_family'])?>';
    font-weight:<?php echo esc_attr($pixhealth_customize['font_title_weight'])?>;
}

    <?php endif?>

<?php if($pixhealth_options['pixhealth_portfolio_width']):?>
#pix-portfolio .isotope-frame .x-item  {
<?php /*?>    width: <?php echo esc_attr($pixhealth_options['pixhealth_portfolio_width'])?>px;<?php */?>
<?php /*?>   height: <?php echo esc_attr($pixhealth_options['pixhealth_portfolio_height'])?>px;<?php */?>
}
<?php endif?>
<?php if($pixhealth_options['pixhealth_portfolio_height']):?>
#pix-portfolio  .portfolio-image , #pix-portfolio .isotope-frame .x-item {
<?php /*?>  height: <?php echo esc_attr($pixhealth_options['pixhealth_portfolio_height'])?>px ;<?php */?>
}
<?php endif?>
<?php if($pixhealth_options['pixhealth_custom_css']):?>
<?php echo $pixhealth_options['pixhealth_custom_css'] ?>
<?php endif?>