<!DOCTYPE html>
<html >
  <head>
    <meta charset="UTF-8">
    <title>Simple parallax effect on mouse hover</title>

    <link rel="stylesheet" href="<?php echo get_template_directory_uri();?>/assets/css/style.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
  <!-- Optional theme -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
  <script src="https://code.jquery.com/jquery-2.2.4.js"></script>   
  <!-- Latest compiled and minified JavaScript -->
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
  <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri();?>/assets/css/style1.css">

  </head>
  <body>
  <header>
  <nav class="navbar">
  <div class="container">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">    
        <span class="sr-only">Toggle navigation</span>    
        <span class="icon-bar"></span>   
        <span class="icon-bar"></span>   
        <span class="icon-bar"></span>   
      </button>   
      <a class="navbar-brand logo" href="#"><img src="<?php echo get_template_directory_uri();?>/assets/images/logo.png" class="img-responsive"></a>   
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">    
      <ul class="nav navbar-nav navbar-right">   
        <li><a href="#">Home</a></li>    
        <li><a href="#">Shop</a></li>   
        <li><a href="#">Services</a></li>   
        <li><a href="#">Blog</a></li>    
        <li><a href="#">Contact</a></li>
        <li><a href="#"><span class="glyphicon glyphicon-search"></span></a></li>  
      </ul>    
    </div><!-- /.navbar-collapse --> 
  </div><!-- /.container-fluid -->
</nav>
</header>
   <div class="parallax-body">
    <div id="background" class="layer-0"></div>
    <!-- <div id="x-axis" class="axis"></div>
    <div id="y-axis" class="axis"></div> -->
    <div id="chat" class="chat-planet chat"></div>
    <div id="earth" class="planet earth"></div>
   <!--  <div id="chat" class="chat-planet layer-1"></div> -->
    <div id="order" class="order-planet order"></div>
    <div  id="hero-fly" class="hero-fly-planet hero-fly"></div>
    
    </div>
<!--********************************************************************-->

<!-- <div class="container"> 
  <div class="row"> 
  <div class="col-xs-hidden col-sm-hidden col-md-12 col-lg-12">   
    <div class="banner_image">      
      <img src="home_img.jpg" class="img-responsive">
    </div>end banner_image
  </div>end col  
  </div>end row
</div> -->
  <div class="container">
  <div class="row">
  <div class="banner_content_wrapper">  
  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
  <div class="banner_content">
    <div class="banner_header">   
      <p>what can i do for you?</p>   
    </div><!-- end banner_header -->
  </div><!-- end banner_content -->
  </div><!-- end col -->
  </div><!-- end row -->
    <div class="banner_description">
      <div class="row">     
      <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
      <div class="banner_service1">         
        <div class="service1_header">         
          <h3>service 1</h3>          
        </div><!-- end service1_header -->
        <div class="service1_content">          
          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
          tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
          quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
          consequat.</p>
        </div><!-- end service1_content -->
      </div><!-- end banner_service1 -->
      </div><!-- end col -->
      <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
      <div class="banner_service2">
        <div class="service2_header">
          <h3>service 2</h3>
        </div><!-- end service2_header -->
        <div class="service2_content">
          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
          tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
          quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
          consequat.</p>
        </div><!-- end service2_content -->
      </div><!-- end banner_service2 -->
      </div><!-- end col -->      
      <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
      <div class="banner_service3">       
        <div class="service3_header">         
          <h3>service 3</h3>
        </div><!-- end service3_header -->
        <div class="service3_content">          
          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
          tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
          quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
          consequat.</p>
        </div><!-- end service3_contetn -->
      </div><!-- end banner_service3 -->  
      </div><!-- end col-->
      </div><!-- end row -->
      <div class="row">
      <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
      <div class="banner_service4">       
        <div class="service4_header">         
          <h3>service 4</h3>
        </div><!-- end service4_header -->
        <div class="service4_content">            
          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
          tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
          quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
          consequat.</p>          
        </div><!-- end service4_content --> 
      </div><!-- end banner_service4 -->
      </div><!-- end col -->      
      <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
      <div class="banner_service5">       
        <div class="service5_header">         
          <h3>service 5</h3>
        </div><!-- end service5_header -->
        <div class="service5_content">          
          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
          tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
          quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
          consequat.</p>
        </div><!-- end service5_content -->
      </div><!-- end banner_service5 -->
      </div><!-- end col -->
      <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
      <div class="banner_service6">       
        <div class="service6_header">         
          <h3>service 6</h3>
        </div><!-- end service6_header -->
        <div class="service6_content">          
          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
          tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
          quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
          consequat.</p>
        </div><!-- end service6_content -->
      </div><!-- end banner_service6 -->    
    </div><!-- end col -->  
  </div><!-- end row -->
  </div>  
  <div class="clients_description">
  <div class="row"> 
  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="client_head">     
      <div class="client_head_main">
        <h3>our happy friends</h3>      
      </div>
      <div class="client_head_sub">       
        <h5>lorem ipsum dolor sit amet lorem ipsum dolor sit amet lorem ipsum dolor sit amet.</h5>        
      </div>
    </div>
  </div>
  </div>
  <div class="row">   
    <div class="client_details">      
      <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
        <div class="client1_detail">
        <div class="row">         
          <div class="col-md-4 col-lg-4">
          <div class="client1_image">           
            <img src="<?php echo get_template_directory_uri();?>/assets/images/client1.jpg">           
            <p>client1</p>
            <p>businessman</p>
          </div>
          </div>
          <div class="col-md-8 col-lg-8">
          <div class="client1_content">           
            <p>lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
            tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
            quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
            consequat.</p>
          </div>  
          </div>        
        </div>
        </div>
      </div>
      <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
        <div class="client2_detail">          
          <div class="row">         
          <div class="col-md-4 col-lg-4">
          <div class="client2_image">           
            <img src="<?php echo get_template_directory_uri();?>/assets/images/client2.jpg">
            <p>client2</p>
            <p>entrepreneur</p>
          </div>          
          </div>
          <div class="col-md-8 col-lg-8">
          <div class="client2_content">           
            <p>lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
            tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
            quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
            consequat.</p>
          </div>          
          </div>
        </div>
        </div>
      </div>
    </div>
  </div>  
  </div>  
  </div><!-- end row banner -- >    
  </div>
  
  <!-- ############################################## -->


    <script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
    <script src='http://cdnjs.cloudflare.com/ajax/libs/gsap/1.11.7/TweenMax.min.js'></script>
    <script src="<?php echo get_template_directory_uri();?>/assets/js/index.js"></script>
    
    
  </body>
</html>