<?php
/**
 * Storefront engine room
 *
 * @package storefront
 */

/**
 * Assign the Storefront version to a var
 */
$theme              = wp_get_theme( 'storefront' );
$storefront_version = $theme['Version'];

/**
 * Set the content width based on the theme's design and stylesheet.
 */
if ( ! isset( $content_width ) ) {
	$content_width = 980; /* pixels */
}

/**
 * Initialize all the things.
 */
require 'inc/class-storefront.php';
require 'inc/jetpack/class-storefront-jetpack.php';
require 'inc/customizer/class-storefront-customizer.php';

require 'inc/storefront-functions.php';
require 'inc/storefront-template-hooks.php';
require 'inc/storefront-template-functions.php';
add_filter('acf/settings/show_admin', '__return_true');
if ( is_woocommerce_activated() ) {
	require 'inc/woocommerce/class-storefront-woocommerce.php';
	require 'inc/woocommerce/storefront-woocommerce-template-hooks.php';
	require 'inc/woocommerce/storefront-woocommerce-template-functions.php';
}

if ( is_admin() ) {
	require 'inc/admin/class-storefront-admin.php';
}

//##################### Add custome  admin column ##############################
add_filter( 'manage_edit-shop_order_columns', 'shop_order_columns' );
function shop_order_columns($columns){
	add_thickbox();
    $new_columns = (is_array($columns)) ? $columns : array();

    //edit this for you column(s)
    $new_columns['prescription'] = 'prescription';
    //stop editing


    return $new_columns;
}

add_action( 'manage_shop_order_posts_custom_column', 'shop_order_posts_custom_column', 2 );
function shop_order_posts_custom_column($column){
    global $post, $the_order , $wpdb;
    $upload_dir = wp_upload_dir();
    if ( empty( $the_order ) || $the_order->id != $post->ID ) {
        $the_order = wc_get_order( $post->ID );
       
    }
     $order_id=$the_order->id;
     $key='prescription';
     $prescription_id=woocommerce_get_order_item_meta( $order_id, $key);
     
     
    if ( $column == 'prescription' ) {  
    	$post_id = $wpdb->get_results("SELECT  option_value FROM $wpdb->options WHERE option_id = '$prescription_id'");
      ?>
        <a class="thickbox" href="<?php echo $upload_dir['baseurl'];?>/<?php echo $post_id[0]->option_value; ?>"><?php echo (isset($prescription_id) ? $prescription_id : ''); ?></a>

      <?php
    }
    //stop editing
}
/**
 * Note: Do not add any custom code here. Please use a custom plugin so that your customizations aren't lost during updates.
 * https://github.com/woothemes/theme-customisations
 */
/*

*/