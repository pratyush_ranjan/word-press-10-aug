<?php
/**
 * Proceed to checkout button
 *
 * Contains the markup for the proceed to checkout button on the cart.
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/cart/proceed-to-checkout-button.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 2.4.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
?>

<?php global $woocommerce; 
//############ check salt is in the product or not ######################
  $items = $woocommerce->cart->get_cart();
  $flag=true;
  foreach($items as $item => $values) { 
         
        $_product = $values['data']->post;
        $saltid = get_post_meta($_product->ID , 'Salt_id' );
        // if selected product in cart has salt_id
         if($saltid){
         	$flag=false;
         	break;
         }


 }
 //#################### check prescription is uploaded by user or not ##############
        @  $prescription =$_SESSION['prescription_id'];
          if($prescription == true || $flag == true){
          	$flag=true;
          }else{

          	$flag=false;
           }
          

  //#################### show checkout button  or not ##############################

      if($flag == true){ ?>
      	<a href="<?php echo esc_url( wc_get_checkout_url() ) ;?>" class="checkout-button button alt wc-forward">
	       <?php echo __( 'Proceed to Checkout', 'woocommerce' ); ?>
           </a> 
     <?php }else{ ?>
          <h3>please upload prescriptions</h3>
    <?php }

  ?>


         

