<?php
/**
 * The template for displaying product content in the single-product.php template
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-single-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

?>

<?php
	/**
	 * woocommerce_before_single_product hook.
	 *
	 * @hooked wc_print_notices - 10
	 */
	 do_action( 'woocommerce_before_single_product' );

	 if ( post_password_required() ) {
	 	echo get_the_password_form();
	 	return;
	 }
?>

<div itemscope itemtype="<?php echo woocommerce_get_product_schema(); ?>" id="product-<?php the_ID(); ?>" <?php post_class(); ?>>

	<?php
		/**
		 * woocommerce_before_single_product_summary hook.
		 *
		 * @hooked woocommerce_show_product_sale_flash - 10
		 * @hooked woocommerce_show_product_images - 20
		 */
		do_action( 'woocommerce_before_single_product_summary' );
	?>

	<div class="summary entry-summary">


		<?php

			/**
			 * woocommerce_single_product_summary hook.
			 *
			 * @hooked woocommerce_template_single_title - 5
			 * @hooked woocommerce_template_single_rating - 10
			 * @hooked woocommerce_template_single_price - 10
			 * @hooked woocommerce_template_single_excerpt - 20
			 * @hooked woocommerce_template_single_add_to_cart - 30
			 * @hooked woocommerce_template_single_meta - 40
			 * @hooked woocommerce_template_single_sharing - 50
			 */
			do_action( 'woocommerce_single_product_summary' );
		?>

	</div><!-- .summary -->
	<!-- display post meta data packet size -->
<div style="background-color:red;">

	<h4>packet size : <?php echo get_post_meta( get_the_ID(), 'packet_size', true ); ?> <?php echo get_post_meta( get_the_ID(), 'Unit', true ); ?></h4>
	<h4>DrugForm : <?php echo get_post_meta( get_the_ID(), 'Drug_form', true ); ?></h4>
	<h4>PackForm : <?php echo get_post_meta( get_the_ID(), 'Pack_form', true ); ?></h4>
	<h4>Ingredients : <?php echo get_post_meta( get_the_ID(), 'Ingredients', true ); ?></h4>
	<h4>Users : <?php echo get_post_meta( get_the_ID(), 'Users', true ); ?></h4>
	<h4>Form : <?php echo get_post_meta( get_the_ID(), 'Form', true ); ?></h4>
	<h4>Rx/Otc : <?php echo get_post_meta( get_the_ID(), 'Rx/Otc', true ); ?></h4>
	
	<?php if( isset( get_post_meta(get_the_ID() , 'Salt_id' )[0])  ){ if( get_post_meta(get_the_ID() , 'Salt_id' )[0] != ''){ ?>
	<h4>Salts data :
		
		<?php 
		$saltid = get_post_meta(get_the_ID() , 'Salt_id' )[0];
		$salts  = $wpdb->get_results("SELECT `option_value` FROM $wpdb->options WHERE option_id = $saltid"); 
		$salts = json_decode( $salts[0]->option_value );
		 ?>

		 <table>
		 	<thead>
		 		<tr>
		 			<td>
		 				Name
		 			</td>

		 			<td>
		 				Strength
		 			</td>


		 		</tr>
		 	</thead>

		 	<tbody>
		 		
		 		<?php foreach($salts as $salt){ ?>
					<tr>
						<td>
							<?php  echo $salt->name; ?>
						</td>

						<td>
							<?php echo $salt->strength; ?>
						</td>

					</tr>

		 		<?php } ?>

		 	</tbody>
		 </table>

	 <?php } } ?>
	 </h4>
</div>
<!-- end display post meta data packet size -->
	<?php
		/**
		 * woocommerce_after_single_product_summary hook.
		 *
		 * @hooked woocommerce_output_product_data_tabs - 10
		 * @hooked woocommerce_upsell_display - 15
		 * @hooked woocommerce_output_related_products - 20
		 */
		do_action( 'woocommerce_after_single_product_summary' );
	?>

	<meta itemprop="url" content="<?php the_permalink(); ?>" />

</div><!-- #product-<?php the_ID(); ?> -->

<?php do_action( 'woocommerce_after_single_product' ); ?>
