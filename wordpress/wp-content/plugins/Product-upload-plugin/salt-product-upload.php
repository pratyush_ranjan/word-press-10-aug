<?php
/*
Plugin Name: Salt Excel Product Upload
Plugin URI: http://example.com
Description: Salt Excel Product Upload
Version: 1.0
Author: Raju Collins 
Author URI: http://w3guy.com

*/

use Box\Spout\Reader\ReaderFactory;
use Box\Spout\Common\Type;
 
// Include Spout library

require_once 'spout-2.4.3/src/Spout/Autoloader/autoload.php';

add_action('admin_menu','Salt_admin_actions');


function Salt_admin_actions()
{
	add_options_page('Product Excel File Upload','Salt Product Upload','manage_options', __FILE__ ,'salt_product_upload');
}

function salt_product_upload(){
 
          global $wpdb;
          $post_id = $wpdb->get_results("SELECT option_name , option_value FROM $wpdb->options WHERE option_name LIKE '%salt%'");
                echo " <table> ";
                echo "  <tr> ";
                echo "    <th>option name</th> ";
               
                echo "    <th>option_value</th> ";
               // echo "    <th>autoload</th> ";
                echo "  </tr> ";
        foreach ($post_id as $key => $variable) {
                 echo " <tr> ";
             foreach ($variable as $key => $value) {
                  echo "<td>". $value ."</td>";
                 }
                echo "  </tr> ";
              }
             echo " </table> ";

  echo '<form action="' . esc_url( $_SERVER['REQUEST_URI'] ) . '" method="post" enctype="multipart/form-data">';
  echo '<p>';
  echo 'Your salt product file (required) Excel (.xlsx)<br/>';
  echo '<input type="file" name="file">';
  echo '</p>';
  echo '<p><input type="submit" name="cf-submitted" value="upload"></p>';
  echo '</form>';

	// if the submit button is clicked, send the email
	if ( isset( $_POST['cf-submitted'] ) ) {
/*******************************************************************/
 /* echo "string";
  die();*/
// check file name is not empty
if (!empty($_FILES['file']['name'])) {   
    // Get File extension eg. 'xlsx' to check file is excel sheet
    $pathinfo = pathinfo($_FILES["file"]["name"]);
  
    // check file has extension xlsx, xls and also check
    // file is not empty
   if (($pathinfo['extension'] == 'xlsx' || $pathinfo['extension'] == 'xls')
           && $_FILES['file']['size'] > 0 ) {
        // Temporary file name
        $inputFileName = $_FILES['file']['tmp_name'];

        // Read excel file by using ReadFactory object.
        $reader = ReaderFactory::create(Type::XLSX);
        // Open file
        $reader->open($inputFileName);
        $count = 1;
        $DataArray=array();
        $storeData=array();
        // Number of sheet in excel file
        foreach ($reader->getSheetIterator() as $sheet) {
            // Number of Rows in Excel sheet
            foreach ($sheet->getRowIterator() as $row) {

                // It reads data after header. In the my excel sheet,
                // header is in the first row.
                if ($count > 1) {
                     //##################################################
                   $option_name = 'salt_id_'.$row[0] ;
                      $new_value = $row[1] ;

                      if ( get_option( $option_name ) !== false ) {

                          // The option already exists, so we just update it.
                          update_option( $option_name, $new_value );

                      } else {

                          // The option hasn't been added yet. We'll add it with $autoload set to 'no'.
                          $deprecated = null;
                          $autoload = 'no';
                          add_option( $option_name, $new_value, $deprecated, $autoload );
                      }

                  //##################################################
                  $arrayName = array("Id"=>$row[0],"salts"=>$row[1]);
                  array_push($storeData, $arrayName);

                  
                
                }
                $count++;
            }
          
        }
          
    // Close excel file
        $reader->close();

        
        
                echo " <table> ";
                echo "  <tr> ";
                echo "    <th>Id</th> ";
                echo "    <th>salts</th> ";  
                echo "  </tr> ";
         foreach ($storeData as $key => $value) {
                echo "  <tr> ";
                  foreach ($value as $_key => $_value) {
                     echo "<td>".$_value."</td>";
                     }
                echo "  </tr> ";
            }
            echo " </table> ";

      unset($storeData);
    } else {
 
        echo "Please Select Valid Excel File";
    }
 
}
}
}
/********************************************************************/


?>


