<?php
/*
Plugin Name: Ayurveda Excel Product Upload
Plugin URI: http://example.com
Description: Ayurveda Excel Product Upload
Version: 1.0
Author: Raju Collins 
Author URI: http://w3guy.com

*/

use Box\Spout\Reader\ReaderFactory;
use Box\Spout\Common\Type;
 
// Include Spout library

require_once 'spout-2.4.3/src/Spout/Autoloader/autoload.php';

add_action('admin_menu','ayurveda_admin_actions');


function ayurveda_admin_actions()
{
  add_options_page('Product Excel File Upload','Ayurveda Product Upload','manage_options', __FILE__ ,'ayurveda_admin');
}

function ayurveda_admin(){
 
// echo '<form action="' . esc_url( $_SERVER['REQUEST_URI'] ) . '" method="post" enctype="multipart/form-data">';
  echo '<form method="post" enctype="multipart/form-data">';
  echo '<p>';
  echo 'Your ayurveda product file (required) Excel (.xlsx)<br/>';
  echo '<input type="file" name="file">';
  echo '</p>';
  echo '<p><input type="submit" name="cf-submitted" value="upload"></p>';
  echo '</form>';






  // if the submit button is clicked, send the email
  if ( isset( $_POST['cf-submitted'] ) ) {
/*******************************************************************/
 /* echo "string";
  die();*/
// check file name is not empty
if (!empty($_FILES['file']['name'])) {   
    // Get File extension eg. 'xlsx' to check file is excel sheet
    $pathinfo = pathinfo($_FILES["file"]["name"]);
  
    // check file has extension xlsx, xls and also check
    // file is not empty
   if (($pathinfo['extension'] == 'xlsx' || $pathinfo['extension'] == 'xls')
           && $_FILES['file']['size'] > 0 ) {
        // Temporary file name
        $inputFileName = $_FILES['file']['tmp_name'];

        // Read excel file by using ReadFactory object.
        $reader = ReaderFactory::create(Type::XLSX);
        // Open file
        $reader->open($inputFileName);
        $count = 1;
        $DataArray=array();
        $storeData=array();
        // Number of sheet in excel file
        foreach ($reader->getSheetIterator() as $sheet) {
            // Number of Rows in Excel sheet
            foreach ($sheet->getRowIterator() as $row) {

                // It reads data after header. In the my excel sheet,
                // header is in the first row.
                if ($count > 1) {
                     $flag=false;
                    // Data of excel sheet  
                    
                   $data=array('post_type' => 'product',
                    'post_title' => $row[0],//Brand Name
                    'post_content' => $row[2],
                    //'post_category'=>array($row[4]),//category id
                    'post_status' => 'publish');
                  
                  // post data im wp
                  $new_post_id=wp_insert_post($data);
/*############################# category  #####################################*/

                 // check category exists or not 
                  $prod_cat_args = array(
                      'taxonomy'     => 'product_cat', //woocommerce
                      'orderby'      => 'name',
                      'hide_empty'        => 0
                    );


                    $woo_categories = get_categories( $prod_cat_args );
                    foreach ( $woo_categories as $woo_cat ) {
                            $woo_cat_id = $woo_cat->term_id; //category ID
                            $woo_cat_name = $woo_cat->name; //category name
                            $woo_cat_slug = $woo_cat->slug; //category slug
                              if($woo_cat_name == $row[10]){
         /* ################### find subCategory #####################*/

                              $term_id=$woo_cat_id;
                             $Sub_caterory_term_id=check_SubCategory($term_id,$row[11]);

                             if($Sub_caterory_term_id == true){

                              $flag=true;
                              $term_id=$Sub_caterory_term_id;
                              break;
                             }  
         /* ###################  End find subCategory #####################*/                        
                              }
           
                        }// end foreach


                        if($flag == false ){

                                $Category_data=wp_insert_term(
                                   $row[10], // the term  name ( category name in capital)
                                   'product_cat', // the taxonomy
                                   array(
                                     'description'=> 'Category description',
                                     //'slug' => strtolower($row[4]), // category name which show
                                   )
                                );
                               
                              
                               $term_id=$Category_data['term_id'];



                               // insert sub category
                               $term_id=ins_Subcategory($row[11],$term_id);
                               
                              }// end if

                     // insert category 
                    wp_set_object_terms( $new_post_id, $term_id, 'product_cat' );
  // ######################## end category  ##########################3



                   //########################## Done adding attributes to product #################
                  update_post_meta( $new_post_id, '_sku', $row[1]);
                  add_post_meta( $new_post_id, 'packet_size', $row[3] );//add packet size
                  update_post_meta( $new_post_id, '_stock_status', 'instock');
                  update_post_meta($new_post_id, '_price',$row[7]);
                  update_post_meta($new_post_id , '_regular_price',  $row[7]);
                  update_post_meta( $new_post_id, '_visibility', 'visible' );

                  //####################### post custom post meta ########################
                  update_post_meta($new_post_id , 'Unit',  $row[4]);
                  update_post_meta($new_post_id , 'Drug_form',  $row[5]);
                  update_post_meta( $new_post_id, 'Pack_form', $row[6] );
                  update_post_meta( $new_post_id, 'Ingredients', $row[8] );
                  update_post_meta( $new_post_id, 'Users', $row[9] );
                  // ################ post product image ####################
                  post_product_image_ayurveda($row[12],$new_post_id);
                 
                  $arrayName = array("BrandName"=>$row[0],"SkuName"=>$row[1],"Manufacturer"=>$row[2],"PackSize"=>$row[3],"Unit"=>$row[4],"DrugForm"=>$row[5],"PackForm"=>$row[6],"Price"=>$row[7],"Ingredients"=>$row[8],"Users"=>$row[9],"Category ID"=>$row[10],"Sub Category ID"=>$row[11],"Image"=>$row[12]);
                  array_push($storeData, $arrayName);

                  
                }
                $count++;
            }
          
        }
          
        // Close excel file
        $reader->close();

        
        
             echo " <table> ";
                echo "  <tr> ";
                echo "    <th>BrandName</th> ";
                echo "    <th>SkuName</th> ";
                echo "    <th>Manufacturer</th> ";
                echo "    <th>PackSize</th> ";
                echo "    <th>Unit</th> ";
                echo "    <th>DrugForm</th> ";
                echo "    <th>PackForm</th> ";
                echo "    <th>MRP(INR)</th> ";
                echo "    <th>Ingredients</th> ";
                echo "    <th>Users</th> ";
                echo "    <th>Category Name</th> ";
                echo "    <th>Sub Category Name</th> "; 
                echo "    <th>Image</th> ";    
                echo "  </tr> ";
         foreach ($storeData as $key => $value) {
                echo "  <tr> ";
                  foreach ($value as $_key => $_value) {
                     echo "<td>".$_value."</td>";
                     }
                echo "  </tr> ";
            }
            echo " </table> ";

      unset($storeData);
 
    } else {
 
        echo "Please Select Valid Excel File";
    }
 
}
}
}
/********************************************************************/

//#################### find list of subCategory ####################
    function check_SubCategory($parent_term_a_id,$Sub_category_name){

         $args = array('child_of' =>$parent_term_a_id,
                       'hide_empty'   => 0);
         
              $woo_categories =get_terms('product_cat',$args);
                    if($woo_categories == false){
                     $flag=true;
                    }else{

                      foreach ( $woo_categories as $woo_cat ) {
                            $woo_cat_id = $woo_cat->term_id; //category ID
                            $woo_cat_name = $woo_cat->name; //category name
                            $woo_cat_slug = $woo_cat->slug; //category slug

                              if($woo_cat_name == $Sub_category_name){
                              $id=$woo_cat_id;
                              $flag=false;
                              break;
                            } 
                            $flag=true;
                       }
                     

                    }
                    if($flag == true){
                    $id=ins_Subcategory($Sub_category_name,$parent_term_a_id);         
                    }

            return $id;                  
  }

//###################### insert  sub category ###########################################

   function ins_Subcategory($Sub_category_name,$child_term_a_id){
    $Category_data=wp_insert_term(
            $Sub_category_name, // the term 
            'product_cat', // the taxonomy
            array(
                // 'description'=> 'Some description.',
                'slug' => $Sub_category_name,
                'parent'=> $child_term_a_id
            )
        );
   $id=$Category_data['term_id'];
    return $id;
   }
 
//########################## set image of product ############################

 function post_product_image_ayurveda($image_name,$post_id){
 $upload_dir = wp_upload_dir();

$file = $upload_dir['baseurl'].'/ayurveda/'.$image_name;
$filename = basename($file);
$upload_file = wp_upload_bits($filename, null, file_get_contents($file));
if (!$upload_file['error']) {
  $wp_filetype = wp_check_filetype($filename, null );
  $attachment = array(
    'post_mime_type' => $wp_filetype['type'],
    'post_parent' => $post_id,
    'post_title' => preg_replace('/\.[^.]+$/', '', $filename),
    'post_content' => '',
    'post_status' => 'inherit'
  );
  $attachment_id = wp_insert_attachment( $attachment, $upload_file['file'], $post_id );
  if (!is_wp_error($attachment_id)) {
    require_once(ABSPATH . "wp-admin" . '/includes/image.php');
    $attachment_data = wp_generate_attachment_metadata( $attachment_id, $upload_file['file'] );
    wp_update_attachment_metadata( $attachment_id,  $attachment_data );
    set_post_thumbnail( $post_id, $attachment_id );
  }
}

   
   return true;
  }
// ######################### end image of product set ########################


?>


